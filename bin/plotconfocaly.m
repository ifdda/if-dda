function plotconfocaly(hinci,event,x,y,z,imageconf,imageconfx,imageconfy,imageconfz,nprint)

% Gets the value of the parameter from the slider.
Param = get(hinci,'Value');

% Tranform in integer
j = int8(Param);

% Puts the value of the parameter on the GUI.
uicontrol('Style', 'text', 'String', num2str(y(j),'%+1.2e\n'),...
'Position', [560 10 80 20]);

% Plots the Graph.

figure(500)
set(500,'DefaultAxesFontName','Times')
set(500,'DefaultAxesFontSize',12)
set(500,'DefaultAxesFontWeight','Bold')
set(500,'DefaultTextfontName','Times')
set(500,'DefaultTextfontSize',12)
set(500,'DefaultTextfontWeight','Bold')
set(500,'Position',[0 0 1000 600])

  subplot('position',[0.075 0.63 0.33 0.33])
  imagesc(x,z,permute(imageconf(:,j,:),[1 3 2])');
axis xy

shading interp
xlabel('xx')
ylabel('z')
axis image
title('modulus')  
colorbar('vert')

  
  subplot('position',[0.65 0.63 0.33 0.33])
  imagesc(x,z,permute(abs(imageconfx(:,j,:)),[1 3 2])');
axis xy

shading interp
xlabel('x')
ylabel('z')
axis image
title(' x')  
colorbar('vert')
  
 subplot('position',[0.075 0.15 0.33 0.33])
  imagesc(x,z,permute(abs(imageconfy(:,j,:)),[1 3 2])');
axis xy

shading interp
xlabel('x')
ylabel('z')
axis image
title(' y')  
colorbar('vert')

%text(-0.4,0.8,'Inci field','units','Normalized','rotation',90)


  subplot('position',[0.65 0.15 0.33 0.33])
  imagesc(x,z,permute(abs(imageconfz(:,j,:)),[1 3 2])');
axis xy

shading interp
xlabel('x')
ylabel('z')
axis image
title(' z')  
colorbar('vert')
printfig(nprint,'confocal')

  
