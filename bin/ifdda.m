close all
clear all
nexist=exist('inputmatlab.mat','file');

if (nexist == 0)
    namefileh5=input('Please give the name of an H5 file : ','s');
    nexisth5 = exist(namefileh5,'file');

    if (nexisth5 == 0)
        disp('Data files do not exist!')
        disp('Please use Advanced interface')
        disp('and uncheck the option "Do not write file" if necessary')
        return;

    end
end

if (nexist == 0)

    nproche=h5read(namefileh5,'/Option/nproche');
    nlocal=h5read(namefileh5,'/Option/nlocal');
    nmacro=h5read(namefileh5,'/Option/nmacro');
    nsection=h5read(namefileh5,'/Option/nsection');
    nsectionsca=h5read(namefileh5,'/Option/nsectionsca');
    nquickdiffracte=h5read(namefileh5,'/Option/nquickdiffracte');
    nforce=h5read(namefileh5,'/Option/nforce');
    nforced=h5read(namefileh5,'/Option/nforced');
    ntorque=h5read(namefileh5,'/Option/ntorque');
    ntorqued=h5read(namefileh5,'/Option/ntorqued');
    nlentille=h5read(namefileh5,'/Option/nlentille');
    nquicklens=h5read(namefileh5,'/Option/nquicklens');
    nphi=h5read(namefileh5,'/Option/nphi');
    ntheta=h5read(namefileh5,'/Option/ntheta');
    niso=h5read(namefileh5,'/Option/iso');
    nfft=h5read(namefileh5,'/Option/nfft2d');
    k0=h5read(namefileh5,'/Option/k0');
    numaper=h5read(namefileh5,'/Option/numaper');
    nprochefft=h5read(namefileh5,'/Option/nprochefft');
    nobjet=h5read(namefileh5,'/Option/nobjet');
    ntypemic=h5read(namefileh5,'/Option/ntypemic');
    nside=h5read(namefileh5,'/Option/nside');
    ntypefile=h5read(namefileh5,'/Option/nmat');
    numaperinc=h5read(namefileh5,'/Option/numaperinc');
    numaperinc2=h5read(namefileh5,'/Option/numaperinc2');
    kcnax=h5read(namefileh5,'/Option/kcx');
    kcnay=h5read(namefileh5,'/Option/kcy');
    testgreen=h5read(namefileh5,'/Option/testgreen');
    numaperil=h5read(namefileh5,'/Option/numaperil');

else

    load inputmatlab.mat -ascii
    nproche=inputmatlab(1);         % Defined box of computation for the near field
    nlocal=inputmatlab(2);          % Compute the local field
    nmacro=inputmatlab(3);          % Compute the macroscopic field
    nsection=inputmatlab(4);        % Compute the cross section
    nsectionsca=inputmatlab(5);     % Compute  C_sca, Poynting and g
    nquickdiffracte=inputmatlab(6); % Compute  C_sca, Poynting and g with FFT
    nforce=inputmatlab(7);          % Compute the optical force
    nforced=inputmatlab(8);         % Compute the optical force
    ntorque=inputmatlab(9);         % Compute the optical force
    ntorqued=inputmatlab(10);       % Compute the optical force
    nlentille=inputmatlab(11);      % Compute the object through the microscope
    nquicklens=inputmatlab(12);     % Compte the lens with FFT
    nphi=inputmatlab(13);           % nphi
    ntheta=inputmatlab(14);         % ntheta
    niso=inputmatlab(15);           % 0 isotrope, 1 anisotrope
    nfft=inputmatlab(16);           % size of the FFT
    k0=inputmatlab(17);             % Wavenumber
    numaper=inputmatlab(18);        % Numerical aperture
    nprochefft=inputmatlab(19);     % =1 if wide field
    nobjet=inputmatlab(20);         % =1 do only the objet
    ntypemic=inputmatlab(21);       % type de microscope
    nside=inputmatlab(22);          % reflexion or transmission
    ntypefile=inputmatlab(23);      % mat file or hdf5 file
    numaperinc=inputmatlab(24);     % Numerical aperture for condenser
    numaperinc2=inputmatlab(25);     % Numerical aperture for condenser
    kcnax=inputmatlab(26);     % Numerical aperture for condenser
    kcnay=inputmatlab(27);     % Numerical aperture for condenser
    testgreen=inputmatlab(28);      % Test if Green tensor computed
    numaperil=inputmatlab(29);      % NA for the confocal and speckle

    if (ntypefile == 0)
        disp('Use mat file')
    end

    if (ntypefile == 1)
        disp('Data files do not computed')
        disp('Please use advanced interface')
        disp('and uncheck the option "Do not write file" if necessary')
        return;
    end

    if (ntypefile == 2)


        nexisth5a = exist('filenameh5','file');
        if (nexisth5a == 0)
            disp('H5 files do not exist!')
            return;
        end

        fid=fopen('filenameh5');    % name file of hdf5
        namefileh5a=fgetl(fid);
        k=0; for i=1:101; if namefileh5a(i) ~= ' '; k = k+1; namefileh5(k)=namefileh5a(i);end;end
        disp('Use HDF5 file')
        %namefileh5=input('Please give the name of an H5 file : ','s')
    end
end

icomp=complex(0,1);

%%%%%%%%%%%%%%%%%%%%%%%% option to print %%%%%%%%%%%%%%%%%%%
prompt = "Print figures [y/n] : ";
txt = input(prompt,"s");
if isempty(txt)
    nprint(1,1) = 'y';
else
    nprint(1,1) =txt;
end

if nprint(1,1) == 'y'
    prompt = "Format [eps/jpg/tiff/pdf/png] : ";
    txt = input(prompt,"s");
    if isempty(txt)
        nprint(2,1:3) = 'eps';
    else
        L = strlength(txt);
        nprint(2,1:L) =txt;
    end
    prompt = "Print ui command [n/y]: ";
    txt = input(prompt,"s");
     if isempty(txt)
        nprint(3,1) = 'n';
     else
       nprint(3,1) = txt;
    end
end

%%%%%%%%%%%%%%%% Begin plot dipole %%%%%%%%%%%%%%%%%%%%%%%%%%

if (ntypefile == 0)

    load x.mat -ascii
    load y.mat -ascii
    load z.mat -ascii

    nx=max(size(x));
    ny=max(size(y));
    nz=max(size(z));

    load xc.mat -ascii
    load yc.mat -ascii
    load zc.mat -ascii
    load epsilon.mat -ascii

elseif(ntypefile == 2)

    nx=double(h5read(namefileh5,'/Object/nx'));
    ny=double(h5read(namefileh5,'/Object/ny'));
    nz=double(h5read(namefileh5,'/Object/nz'));
    xc=h5read(namefileh5,'/Object/Dipole position x');
    yc=h5read(namefileh5,'/Object/Dipole position y');
    zc=h5read(namefileh5,'/Object/Dipole position z');
    epsilon(:,1)=h5read(namefileh5,'/Object/Epsilon real part');
    epsilon(:,2)=h5read(namefileh5,'/Object/Epsilon imaginary part');

    xmin=min(xc);
    xmax=max(xc);
    pasx=(xmax-xmin)/(nx-1);
    ymin=min(yc);
    ymax=max(yc);
    pasy=(ymax-ymin)/(ny-1);
    zmin=min(zc);
    zmax=max(zc);
    pasz=(zmax-zmin)/(nz-1);
    x=xmin:pasx:xmax;
    y=ymin:pasy:ymax;
    z=zmin:pasz:zmax;

end

if (niso == 0)
    if (nproche == -1 )

        figure(1)
        set(1,'DefaultAxesFontName','Times')
        set(1,'DefaultAxesFontSize',12)
        set(1,'DefaultAxesFontWeight','Bold')
        set(1,'DefaultTextfontName','Times')
        set(1,'DefaultTextfontSize',12)
        set(1,'DefaultTextfontWeight','Bold')
        set(1,'Position',[0 600 500 500])

        m=0;n=max(size( epsilon));
        for i=1:n
            if (epsilon(i,1)~= 1 || epsilon(i,2)~= 0)
                m=m+1;epsilonbg(m)=epsilon(i,1);
            end
        end
        scatter3(xc,yc,zc,10,epsilonbg)
        axis image
        colorbar
        xlabel('x')
        ylabel('y')
        zlabel('z')
        title('Position of the dipoles')
        printfig(nprint,'dipolepos')


    else

        figure(1)
        set(1,'DefaultAxesFontName','Times')
        set(1,'DefaultAxesFontSize',12)
        set(1,'DefaultAxesFontWeight','Bold')
        set(1,'DefaultTextfontName','Times')
        set(1,'DefaultTextfontSize',12)
        set(1,'DefaultTextfontWeight','Bold')
        set(1,'Position',[0 600 1200 500])

        subplot(1,2,1)

        scatter3(xc,yc,zc,10,epsilon(:,1))
        axis image
        colorbar
        xlabel('x')
        ylabel('y')
        zlabel('z')
        title('Position of the dipoles')
        subplot(1,2,2)

        m=0;n=max(size( epsilon));
        for i=1:n
            if (epsilon(i,1)~= 1 || epsilon(i,2)~= 0)
                m=m+1;
                xcc(m)=xc(i);ycc(m)=yc(i);zcc(m)=zc(i);epsilonbg(m)=epsilon(i,1);
            end
        end

        scatter3(xcc,ycc,zcc,10,epsilonbg)
        axis image
        colorbar
        xlabel('x')
        ylabel('y')
        zlabel('z')
        title({'Position of the dipoles' 'without background'})
    end
    printfig(nprint,'dipolepos')


else

    figure(1)
    set(1,'DefaultAxesFontName','Times')
    set(1,'DefaultAxesFontSize',12)
    set(1,'DefaultAxesFontWeight','Bold')
    set(1,'DefaultTextfontName','Times')
    set(1,'DefaultTextfontSize',12)
    set(1,'DefaultTextfontWeight','Bold')
    set(1,'Position',[0 600 1200 500])

    scatter3(xc,yc,zc,10)
    axis image
    colorbar
    xlabel('x')
    ylabel('y')
    zlabel('z')
    title('Position of the dipoles (anisotropic object)')
    printfig(nprint,'dipolepos')

end
%%%%%%%%%%%%%%%% End plot dipole %%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%% Begin plot epsilon %%%%%%%%%%%%%%%%%%%%%%%%%%
if niso == 0

    matxyepsilonr=reshape(epsilon(:,1),nx,ny,nz);
    matxyepsiloni=reshape(epsilon(:,2),nx,ny,nz);

    clear epsilon

    figure(2)
    set(2,'DefaultAxesFontName','Times')
    set(2,'DefaultAxesFontSize',12)
    set(2,'DefaultAxesFontWeight','Bold')
    set(2,'DefaultTextfontName','Times')
    set(2,'DefaultTextfontSize',12)
    set(2,'DefaultTextfontWeight','Bold')
    set(2,'Position',[0 600 1000 500])

    uicontrol('Style','text','Fontsize',16,'Fontweight','bold',...
        'units','normalized','Position', [0.25 0.9 0.5 0.1],'String','Plot relative permittivity');

    uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
        {'2D-view cutting planes','(x,y)-plane','(x,z)-plane','(y,z)-plane'},...
        'units','normalized','Position', [0.4 0.85 0.2 0.05],...
        'Callback',{@plotepsilon,nx,ny,nz,x,y,z,matxyepsilonr,matxyepsiloni,nprint});


else
    matepsilonrxx=zeros(nz,ny,nx);
    matepsilonixx=zeros(nz,ny,nx);
    matepsilonrxy=zeros(nz,ny,nx);
    matepsilonixy=zeros(nz,ny,nx);
    matepsilonrxz=zeros(nz,ny,nx);
    matepsilonixz=zeros(nz,ny,nx);
    matepsilonryx=zeros(nz,ny,nx);
    matepsiloniyx=zeros(nz,ny,nx);
    matepsilonryy=zeros(nz,ny,nx);
    matepsiloniyy=zeros(nz,ny,nx);
    matepsilonryz=zeros(nz,ny,nx);
    matepsiloniyz=zeros(nz,ny,nx);
    matepsilonrzx=zeros(nz,ny,nx);
    matepsilonizx=zeros(nz,ny,nx);
    matepsilonrzy=zeros(nz,ny,nx);
    matepsilonizy=zeros(nz,ny,nx);
    matepsilonrzz=zeros(nz,ny,nx);
    matepsilonizz=zeros(nz,ny,nx);



    for i=1:nz
        for j=1:ny
            for k=1:nx;kk=9*(k+nx*(j-1)+nx*ny*(i-1)-1);
                matepsilonrxx(i,j,k)=epsilon(kk+1,1);
                matepsilonixx(i,j,k)=epsilon(kk+1,2);
                matepsilonrxy(i,j,k)=epsilon(kk+2,1);
                matepsilonixy(i,j,k)=epsilon(kk+2,2);
                matepsilonrxz(i,j,k)=epsilon(kk+3,1);
                matepsilonixz(i,j,k)=epsilon(kk+3,2);
                matepsilonryx(i,j,k)=epsilon(kk+4,1);
                matepsiloniyx(i,j,k)=epsilon(kk+4,2);
                matepsilonryy(i,j,k)=epsilon(kk+5,1);
                matepsiloniyy(i,j,k)=epsilon(kk+5,2);
                matepsilonryz(i,j,k)=epsilon(kk+6,1);
                matepsiloniyz(i,j,k)=epsilon(kk+6,2);
                matepsilonrzx(i,j,k)=epsilon(kk+7,1);
                matepsilonizx(i,j,k)=epsilon(kk+7,2);
                matepsilonrzy(i,j,k)=epsilon(kk+8,1);
                matepsilonizy(i,j,k)=epsilon(kk+8,2);
                matepsilonrzz(i,j,k)=epsilon(kk+9,1);
                matepsilonizz(i,j,k)=epsilon(kk+9,2);
            end
        end
    end
    clear epsilon

    figure(2)
    set(2,'DefaultAxesFontName','Times')
    set(2,'DefaultAxesFontSize',12)
    set(2,'DefaultAxesFontWeight','Bold')
    set(2,'DefaultTextfontName','Times')
    set(2,'DefaultTextfontSize',12)
    set(2,'DefaultTextfontWeight','Bold')
    set(2,'Position',[0 600 1000 500])

    uicontrol('Style','text','Fontsize',16,'Fontweight','bold','Position',[300 440 380 50],'String','Plot tensor of permittivity in (x,y)-plane for        component');

    uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
        {'xx','xy','xz','yx','yy','yz','zx','zy','zz'},...
        'Position',[490 425 40 40],...
        'Callback',{@plotepsilonani,nx,ny,nz,x,y,z,matepsilonrxx,matepsilonixx,matepsilonrxy,matepsilonixy,matepsilonrxz,matepsilonixz,matepsilonryx,matepsiloniyx,matepsilonryy,matepsiloniyy,matepsilonryz,matepsiloniyz,matepsilonrzx,matepsilonizx,matepsilonrzy,matepsilonizy,matepsilonrzz,matepsilonizz,nprint});

    printfig(nprint,'epsilon')
    

end

if nobjet == 1; return ;end


%%%%%%%%%%%%%%%% End plot epsilon %%%%%%%%%%%%%%%%%%%%%%%%%%


if (testgreen == 1)

    if (ntypefile == 0)

        load greenxx.mat -ascii
        load greenxy.mat -ascii
        load greenxz.mat -ascii
        load greenyx.mat -ascii
        load greenyy.mat -ascii
        load greenyz.mat -ascii
        load greenzx.mat -ascii
        load greenzy.mat -ascii
        load greenzz.mat -ascii

        greenxxr=reshape(greenxx(:,1),nx,ny,nz);
        greenxxi=reshape(greenxx(:,2),nx,ny,nz);
        greenxyr=reshape(greenxy(:,1),nx,ny,nz);
        greenxyi=reshape(greenxy(:,2),nx,ny,nz);
        greenxzr=reshape(greenxz(:,1),nx,ny,nz);
        greenxzi=reshape(greenxz(:,2),nx,ny,nz);

        greenyxr=reshape(greenyx(:,1),nx,ny,nz);
        greenyxi=reshape(greenyx(:,2),nx,ny,nz);
        greenyyr=reshape(greenyy(:,1),nx,ny,nz);
        greenyyi=reshape(greenyy(:,2),nx,ny,nz);
        greenyzr=reshape(greenyz(:,1),nx,ny,nz);
        greenyzi=reshape(greenyz(:,2),nx,ny,nz);

        greenzxr=reshape(greenzx(:,1),nx,ny,nz);
        greenzxi=reshape(greenzx(:,2),nx,ny,nz);
        greenzyr=reshape(greenzy(:,1),nx,ny,nz);
        greenzyi=reshape(greenzy(:,2),nx,ny,nz);
        greenzzr=reshape(greenzz(:,1),nx,ny,nz);
        greenzzi=reshape(greenzz(:,2),nx,ny,nz);

        clear grenxx;clear greenxy;clear greenxz;
        clear grenyx;clear greenyy;clear greenyz;
        clear grenzx;clear greenzy;clear greenzz;

    elseif(ntypefile == 2)

        greenxxr=reshape(h5read(namefileh5,'/Near Field/Green tensor xx component real part'),nx,ny,nz);
        greenxxi=reshape(h5read(namefileh5,'/Near Field/Green tensor xx component imaginary part'),nx,ny,nz);
        greenxyr=reshape(h5read(namefileh5,'/Near Field/Green tensor xy component real part'),nx,ny,nz);
        greenxyi=reshape(h5read(namefileh5,'/Near Field/Green tensor xy component imaginary part'),nx,ny,nz);
        greenxzr=reshape(h5read(namefileh5,'/Near Field/Green tensor xz component real part'),nx,ny,nz);
        greenxzi=reshape(h5read(namefileh5,'/Near Field/Green tensor xz component imaginary part'),nx,ny,nz);

        greenyxr=reshape(h5read(namefileh5,'/Near Field/Green tensor yx component real part'),nx,ny,nz);
        greenyxi=reshape(h5read(namefileh5,'/Near Field/Green tensor yx component imaginary part'),nx,ny,nz);
        greenyyr=reshape(h5read(namefileh5,'/Near Field/Green tensor yy component real part'),nx,ny,nz);
        greenyyi=reshape(h5read(namefileh5,'/Near Field/Green tensor yy component imaginary part'),nx,ny,nz);
        greenyzr=reshape(h5read(namefileh5,'/Near Field/Green tensor yz component real part'),nx,ny,nz);
        greenyzi=reshape(h5read(namefileh5,'/Near Field/Green tensor yz component imaginary part'),nx,ny,nz);

        greenzxr=reshape(h5read(namefileh5,'/Near Field/Green tensor zx component real part'),nx,ny,nz);
        greenzxi=reshape(h5read(namefileh5,'/Near Field/Green tensor zx component imaginary part'),nx,ny,nz);
        greenzyr=reshape(h5read(namefileh5,'/Near Field/Green tensor zy component real part'),nx,ny,nz);
        greenzyi=reshape(h5read(namefileh5,'/Near Field/Green tensor zy component imaginary part'),nx,ny,nz);
        greenzzr=reshape(h5read(namefileh5,'/Near Field/Green tensor zz component real part'),nx,ny,nz);
        greenzzi=reshape(h5read(namefileh5,'/Near Field/Green tensor zz component imaginary part'),nx,ny,nz);

    end


    figure(3)
    set(3,'DefaultAxesFontName','Times')
    set(3,'DefaultAxesFontSize',12)
    set(3,'DefaultAxesFontWeight','Bold')
    set(3,'DefaultTextfontName','Times')
    set(3,'DefaultTextfontSize',12)
    set(3,'DefaultTextfontWeight','Bold')
    set(3,'Position',[0 600 1000 500])

    uicontrol('Style','text','Fontsize',16,'Fontweight','bold','Position',[300 440 380 50],'String','Plot tensor in (x,y)-plane for        component');

    uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
        {'xx','xy','xz','yx','yy','yz','zx','zy','zz'},...
        'Position',[490 425 40 40],...
        'Callback',{@plotgreen,nx,ny,nz,x,y,z,greenxxr,greenxxi,greenxyr,greenxyi,greenxzr,greenxzi,greenyxr,greenyxi,greenyyr,greenyyi,greenyzr,greenyzi,greenzxr,greenzxi,greenzyr,greenzyi,greenzzr,greenzzi,nprint});

    printfig(nprint,'epsilon')


else

    %%%%%%%%%%%%%%%% Begin incident field %%%%%%%%%%%%%%%%%%%%%%%%%%

    if ( nprochefft == 0)

        if (ntypefile == 0)
            load incidentfield.mat -ascii
            load incidentfieldx.mat -ascii
            load incidentfieldy.mat -ascii
            load incidentfieldz.mat -ascii
        elseif (ntypefile ==2)
            incidentfield=h5read(namefileh5,'/Near Field/Incident field modulus');
            incidentfieldx(:,1)=h5read(namefileh5,'/Near Field/Incident field x component real part');
            incidentfieldx(:,2)=h5read(namefileh5,'/Near Field/Incident field x component imaginary part');
            incidentfieldy(:,1)=h5read(namefileh5,'/Near Field/Incident field y component real part');
            incidentfieldy(:,2)=h5read(namefileh5,'/Near Field/Incident field y component imaginary part');
            incidentfieldz(:,1)=h5read(namefileh5,'/Near Field/Incident field z component real part');
            incidentfieldz(:,2)=h5read(namefileh5,'/Near Field/Incident field z component imaginary part');
        end

        matxyincifield=reshape(incidentfield,nx,ny,nz);
        matxyincifieldx=reshape(incidentfieldx(:,1)+icomp*incidentfieldx(:,2),nx,ny,nz);
        matxyincifieldy=reshape(incidentfieldy(:,1)+icomp*incidentfieldy(:,2),nx,ny,nz);
        matxyincifieldz=reshape(incidentfieldz(:,1)+icomp*incidentfieldz(:,2),nx,ny,nz);

        clear incidentfieldx
        clear incidentfieldy
        clear incidentfieldz


        figure(10)
        set(10,'DefaultAxesFontName','Times')
        set(10,'DefaultAxesFontSize',12)
        set(10,'DefaultAxesFontWeight','Bold')
        set(10,'DefaultTextfontName','Times')
        set(10,'DefaultTextfontSize',12)
        set(10,'DefaultTextfontWeight','Bold')
        set(10,'Position',[0 0 1000 600])




        uicontrol('Style','text','Fontsize',16,'Fontweight','bold',...
            'units','normalized','Position', [0.4 0.7 0.2 0.3],'String','Plot incident field');

        uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
            {'2D-view cutting planes','(x,y)-plane','(x,z)-plane','(y,z)-plane'},...
            'units','normalized','Position', [0.4 0.65 0.2 0.05],...
            'Callback',{@plotincifield,nx,ny,nz,x,y,z,matxyincifield,matxyincifieldx,matxyincifieldy,matxyincifieldz,nprint});


    else

        if (ntypefile == 0)

            load xwf.mat -ascii
            load ywf.mat -ascii
            load zwf.mat -ascii

            nxm=max(size(xwf));
            nym=max(size(ywf));
            nzm=max(size(zwf));

            load incidentfieldwf.mat -ascii
            load incidentfieldxwf.mat -ascii
            load incidentfieldywf.mat -ascii
            load incidentfieldzwf.mat -ascii

        elseif (ntypefile ==2)

            xwf=h5read(namefileh5,'/Near Field/xwf');
            ywf=h5read(namefileh5,'/Near Field/ywf');
            zwf=h5read(namefileh5,'/Near Field/zwf');

            nxm=max(size(xwf));
            nym=max(size(ywf));
            nzm=max(size(zwf));

            incidentfieldwf=h5read(namefileh5,'/Near Field/Incident field modulus wf');
            incidentfieldxwf(:,1)=h5read(namefileh5,'/Near Field/Incident field x component real part wf');
            incidentfieldxwf(:,2)=h5read(namefileh5,'/Near Field/Incident field x component imaginary part wf');
            incidentfieldywf(:,1)=h5read(namefileh5,'/Near Field/Incident field y component real part wf');
            incidentfieldywf(:,2)=h5read(namefileh5,'/Near Field/Incident field y component imaginary part wf');
            incidentfieldzwf(:,1)=h5read(namefileh5,'/Near Field/Incident field z component real part wf');
            incidentfieldzwf(:,2)=h5read(namefileh5,'/Near Field/Incident field z component imaginary part wf');
        end


        matxyincifield=reshape(incidentfieldwf,nxm,nym,nzm);
        matxyincifieldx=reshape(incidentfieldxwf(:,1)+icomp*incidentfieldxwf(:,2),nxm,nym,nzm);
        matxyincifieldy=reshape(incidentfieldywf(:,1)+icomp*incidentfieldywf(:,2),nxm,nym,nzm);
        matxyincifieldz=reshape(incidentfieldzwf(:,1)+icomp*incidentfieldzwf(:,2),nxm,nym,nzm);

        clear incidentfieldxwf
        clear incidentfieldywf
        clear incidentfieldzwf

        figure(10)
        set(10,'DefaultAxesFontName','Times')
        set(10,'DefaultAxesFontSize',12)
        set(10,'DefaultAxesFontWeight','Bold')
        set(10,'DefaultTextfontName','Times')
        set(10,'DefaultTextfontSize',12)
        set(10,'DefaultTextfontWeight','Bold')
        set(10,'Position',[0 0 1000 600])


        uicontrol('Style','text','Fontsize',16,'Fontweight','bold',...
            'units','normalized','Position', [0.4 0.7 0.2 0.3],'String','Plot incident field');

        uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
            {'2D-view cutting planes','(x,y)-plane','(x,z)-plane','(y,z)-plane'},...
            'units','normalized','Position', [0.4 0.65 0.2 0.05],...
            'Callback',{@plotincifield,nx,ny,nz,x,y,z,matxyincifield,matxyincifieldx,matxyincifieldy,matxyincifieldz,nprint});

        printfig(nprint,'incident')

    end
    %%%%%%%%%%%%%%%% End incident field %%%%%%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%%%% Plot local field %%%%%%%%%%%%%%%%%%%%%%%%%%
    if nlocal == 1

        if (nprochefft ==0)

            if (ntypefile == 0)
                load localfield.mat -ascii
                load localfieldx.mat -ascii
                load localfieldy.mat -ascii
                load localfieldz.mat -ascii
            elseif (ntypefile ==2)
                localfield=h5read(namefileh5,'/Near Field/Local field modulus');
                localfieldx(:,1)=h5read(namefileh5,'/Near Field/Local field x component real part');
                localfieldx(:,2)=h5read(namefileh5,'/Near Field/Local field x component imaginary part');
                localfieldy(:,1)=h5read(namefileh5,'/Near Field/Local field y component real part');
                localfieldy(:,2)=h5read(namefileh5,'/Near Field/Local field y component imaginary part');
                localfieldz(:,1)=h5read(namefileh5,'/Near Field/Local field z component real part');
                localfieldz(:,2)=h5read(namefileh5,'/Near Field/Local field z component imaginary part');
            end

            matxylocalfield=reshape(localfield,nx,ny,nz);
            matxylocalfieldx=reshape(localfieldx(:,1)+icomp*localfieldx(:,2),nx,ny,nz);
            matxylocalfieldy=reshape(localfieldy(:,1)+icomp*localfieldy(:,2),nx,ny,nz);
            matxylocalfieldz=reshape(localfieldz(:,1)+icomp*localfieldz(:,2),nx,ny,nz);

            clear localfieldx
            clear localfieldy
            clear localfieldz

            figure(20)
            set(20,'DefaultAxesFontName','Times')
            set(20,'DefaultAxesFontSize',12)
            set(20,'DefaultAxesFontWeight','Bold')
            set(20,'DefaultTextfontName','Times')
            set(20,'DefaultTextfontSize',12)
            set(20,'DefaultTextfontWeight','Bold')
            set(20,'Position',[0 0 1000 600])

            uicontrol('Style','text','Fontsize',16,'Fontweight','bold',...
                'units','normalized','Position', [0.4 0.7 0.2 0.3],'String','Plot local field');

            uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
                {'2D-view cutting planes','(x,y)-plane','(x,z)-plane','(y,z)-plane'},...
                'units','normalized','Position', [0.4 0.65 0.2 0.05],...
                'Callback',{@plotlocalfield,nx,ny,nz,x,y,z,matxylocalfield,matxylocalfieldx,matxylocalfieldy,matxylocalfieldz,nprint});


        else

            if (ntypefile ==0)

                load xwf.mat -ascii
                load ywf.mat -ascii
                load zwf.mat -ascii

                nxm=max(size(xwf));
                nym=max(size(ywf));
                nzm=max(size(zwf));

                load localfieldwf.mat -ascii
                load localfieldxwf.mat -ascii
                load localfieldywf.mat -ascii
                load localfieldzwf.mat -ascii

            elseif (ntypefile ==2)

                xwf=h5read(namefileh5,'/Near Field/xwf');
                ywf=h5read(namefileh5,'/Near Field/ywf');
                zwf=h5read(namefileh5,'/Near Field/zwf');

                nxm=max(size(xwf));
                nym=max(size(ywf));
                nzm=max(size(zwf));

                localfieldwf=h5read(namefileh5,'/Near Field/Local field modulus wf');
                localfieldxwf(:,1)=h5read(namefileh5,'/Near Field/Local field x component real part wf');
                localfieldxwf(:,2)=h5read(namefileh5,'/Near Field/Local field x component imaginary part wf');
                localfieldywf(:,1)=h5read(namefileh5,'/Near Field/Local field y component real part wf');
                localfieldywf(:,2)=h5read(namefileh5,'/Near Field/Local field y component imaginary part wf');
                localfieldzwf(:,1)=h5read(namefileh5,'/Near Field/Local field z component real part wf');
                localfieldzwf(:,2)=h5read(namefileh5,'/Near Field/Local field z component imaginary part wf');

            end

            matxylocalfield=reshape(localfieldwf,nxm,nym,nzm);
            matxylocalfieldx=reshape(localfieldxwf(:,1)+icomp*localfieldxwf(:,2),nxm,nym,nzm);
            matxylocalfieldy=reshape(localfieldywf(:,1)+icomp*localfieldywf(:,2),nxm,nym,nzm);
            matxylocalfieldz=reshape(localfieldzwf(:,1)+icomp*localfieldzwf(:,2),nxm,nym,nzm);

            clear localfieldxwf
            clear localfieldywf
            clear localfieldzwf

            figure(20)
            set(20,'DefaultAxesFontName','Times')
            set(20,'DefaultAxesFontSize',12)
            set(20,'DefaultAxesFontWeight','Bold')
            set(20,'DefaultTextfontName','Times')
            set(20,'DefaultTextfontSize',12)
            set(20,'DefaultTextfontWeight','Bold')
            set(20,'Position',[0 0 1000 600])


            uicontrol('Style','text','Fontsize',16,'Fontweight','bold',...
                'units','normalized','Position', [0.4 0.7 0.2 0.3],'String','Plot local field');

            uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
                {'2D-view cutting planes','(x,y)-plane','(x,z)-plane','(y,z)-plane'},...
                'units','normalized','Position', [0.4 0.65 0.2 0.05],...
                'Callback',{@plotlocalfield,nxm,nym,nzm,xwf,ywf,zwf,matxylocalfield,matxylocalfieldx,matxylocalfieldy,matxylocalfieldz,nprint});

        end
    end

    %%%%%%%%%%%%%%%% End local field %%%%%%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%%%% Begin macrocopic field %%%%%%%%%%%%%%%%%%%%%%%%%%
    if nmacro == 1

        if (nprochefft ==0)
            if (ntypefile == 0)
                load macroscopicfield.mat -ascii
                load macroscopicfieldx.mat -ascii
                load macroscopicfieldy.mat -ascii
                load macroscopicfieldz.mat -ascii
            elseif (ntypefile ==2)
                macroscopicfield=h5read(namefileh5,'/Near Field/Macroscopic field modulus');
                macroscopicfieldx(:,1)=h5read(namefileh5,'/Near Field/Macroscopic field x component real part');
                macroscopicfieldx(:,2)=h5read(namefileh5,'/Near Field/Macroscopic field x component imaginary part');
                macroscopicfieldy(:,1)=h5read(namefileh5,'/Near Field/Macroscopic field y component real part');
                macroscopicfieldy(:,2)=h5read(namefileh5,'/Near Field/Macroscopic field y component imaginary part');
                macroscopicfieldz(:,1)=h5read(namefileh5,'/Near Field/Macroscopic field z component real part');
                macroscopicfieldz(:,2)=h5read(namefileh5,'/Near Field/Macroscopic field z component imaginary part');
            end

            matxymacrofield=reshape(macroscopicfield,nx,ny,nz);
            matxymacrofieldx=reshape(macroscopicfieldx(:,1)+icomp*macroscopicfieldx(:,2),nx,ny,nz);
            matxymacrofieldy=reshape(macroscopicfieldy(:,1)+icomp*macroscopicfieldy(:,2),nx,ny,nz);
            matxymacrofieldz=reshape(macroscopicfieldz(:,1)+icomp*macroscopicfieldz(:,2),nx,ny,nz);

            clear macroscopicfieldx
            clear macroscopicfieldy
            clear macroscopicfieldz

            figure(30)
            set(30,'DefaultAxesFontName','Times')
            set(30,'DefaultAxesFontSize',12)
            set(30,'DefaultAxesFontWeight','Bold')
            set(30,'DefaultTextfontName','Times')
            set(30,'DefaultTextfontSize',12)
            set(30,'DefaultTextfontWeight','Bold')
            set(30,'Position',[0 0 1000 600])



            uicontrol('Style','text','Fontsize',16,'Fontweight','bold',...
                'units','normalized','Position', [0.4 0.7 0.2 0.3],'String','Plot macroscopic field')

            uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
                {'2D-view cutting planes','(x,y)-plane','(x,z)-plane','(y,z)-plane'},...
                'units','normalized','Position', [0.4 0.65 0.2 0.05],...
                'Callback',{@plotmacrofield,nx,ny,nz,x,y,z,matxymacrofield,matxymacrofieldx,matxymacrofieldy,matxymacrofieldz,nprint});

        else

            if (ntypefile == 0)

                load xwf.mat -ascii
                load ywf.mat -ascii
                load zwf.mat -ascii

                nxm=max(size(xwf));
                nym=max(size(ywf));
                nzm=max(size(zwf));

                load macroscopicfieldwf.mat -ascii
                load macroscopicfieldxwf.mat -ascii
                load macroscopicfieldywf.mat -ascii
                load macroscopicfieldzwf.mat -ascii

            elseif (ntypefile ==2)

                xwf=h5read(namefileh5,'/Near Field/xwf');
                ywf=h5read(namefileh5,'/Near Field/ywf');
                zwf=h5read(namefileh5,'/Near Field/zwf');

                nxm=max(size(xwf));
                nym=max(size(ywf));
                nzm=max(size(zwf));

                macroscopicfieldwf=h5read(namefileh5,'/Near Field/Macroscopic field modulus wf');
                macroscopicfieldxwf(:,1)=h5read(namefileh5,'/Near Field/Macroscopic field x component real part wf');
                macroscopicfieldxwf(:,2)=h5read(namefileh5,'/Near Field/Macroscopic field x component imaginary part wf');
                macroscopicfieldywf(:,1)=h5read(namefileh5,'/Near Field/Macroscopic field y component real part wf');
                macroscopicfieldywf(:,2)=h5read(namefileh5,'/Near Field/Macroscopic field y component imaginary part wf');
                macroscopicfieldzwf(:,1)=h5read(namefileh5,'/Near Field/Macroscopic field z component real part wf');
                macroscopicfieldzwf(:,2)=h5read(namefileh5,'/Near Field/Macroscopic field z component imaginary part wf');
            end


            matxymacrofield=reshape(macroscopicfieldwf,nxm,nym,nzm);
            matxymacrofieldx=reshape(macroscopicfieldxwf(:,1)+icomp*macroscopicfieldxwf(:,2),nxm,nym,nzm);
            matxymacrofieldy=reshape(macroscopicfieldywf(:,1)+icomp*macroscopicfieldywf(:,2),nxm,nym,nzm);
            matxymacrofieldz=reshape(macroscopicfieldzwf(:,1)+icomp*macroscopicfieldzwf(:,2),nxm,nym,nzm);

            clear macrofieldxwf
            clear macrofieldywf
            clear macrofieldzwf

            figure(30)
            set(30,'DefaultAxesFontName','Times')
            set(30,'DefaultAxesFontSize',12)
            set(30,'DefaultAxesFontWeight','Bold')
            set(30,'DefaultTextfontName','Times')
            set(30,'DefaultTextfontSize',12)
            set(30,'DefaultTextfontWeight','Bold')
            set(30,'Position',[0 0 1000 600])



            uicontrol('Style','text','Fontsize',16,'Fontweight','bold',...
                'units','normalized','Position', [0.4 0.7 0.2 0.3],'String','Plot macroscopic field')

            uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
                {'2D-view cutting planes','(x,y)-plane','(x,z)-plane','(y,z)-plane'},...
                'units','normalized','Position', [0.4 0.65 0.2 0.05],...
                'Callback',{@plotmacrofield,nxm,nym,nzm,xwf,ywf,zwf,matxymacrofield,matxymacrofieldx,matxymacrofieldy,matxymacrofieldz,nprint});


        end
    end
    %%%%%%%%%%%%%%%% End macroscopic field %%%%%%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%%%% Begin Poynting vector %%%%%%%%%%%%%%%%%%%%%%%%%%
    if nsectionsca == 1
        if nquickdiffracte == 0

            if (ntypefile ==0)

                load poynting.mat -ascii
                load poyntingpos.mat -ascii
                load poyntingneg.mat -ascii
                load kx.mat -ascii
                load ky.mat -ascii


            elseif (ntypefile ==2)

                poynting=h5read(namefileh5,'/Far Field/Poynting');
                poyntingpos=h5read(namefileh5,'/Far Field/Poynting positive');
                poyntingneg=h5read(namefileh5,'/Far Field/Poynting negative');
                kx=h5read(namefileh5,'/Far Field/kx Poynting');
                ky=h5read(namefileh5,'/Far Field/ky Poynting');

            end

            ray=reshape(poynting,nphi,ntheta);
            ray(nphi+1,:)=ray(1,:);
            xpo=zeros(nphi+1,ntheta);
            ypo=zeros(nphi+1,ntheta);
            zpo=zeros(nphi+1,ntheta);

            for i=1:ntheta
                for j=1:nphi+1
                    theta=pi*(i-1)/(ntheta-1);
                    phi=2*pi*(j-1)/(nphi);
                    xpo(j,i)=ray(j,i)*sin(theta)*cos(phi);
                    ypo(j,i)=ray(j,i)*sin(theta)*sin(phi);
                    zpo(j,i)=ray(j,i)*cos(theta);
                end
            end


            figure(100)
            set(100,'DefaultAxesFontName','Times')
            set(100,'DefaultAxesFontSize',12)
            set(100,'DefaultAxesFontWeight','Bold')
            set(100,'DefaultTextfontName','Times')
            set(100,'DefaultTextfontSize',12)
            set(100,'DefaultTextfontWeight','Bold')
            set(100,'Position',[1000 0 600 600])
            surf(xpo,ypo,zpo,ray)
            shading interp

            title('Poynting vector')

            xlabel('$k_x$','Interpreter','latex','Fontsize',18)
            ylabel('$k_y$','Interpreter','latex','Fontsize',18)
            zlabel('$k_z$','Interpreter','latex','Fontsize',18)

            printfig(nprint,'poynting3d')

            nxp=max(size(kx));
            nyp=max(size(ky));

            figure(101)
            set(101,'DefaultAxesFontName','Times')
            set(101,'DefaultAxesFontSize',12)
            set(101,'DefaultAxesFontWeight','Bold')
            set(101,'DefaultTextfontName','Times')
            set(101,'DefaultTextfontSize',12)
            set(101,'DefaultTextfontWeight','Bold')
            set(101,'Position',[1000 0 1000 600])

            suptitle('Poynting Modulus in $k_x$ and $k_y$ plane','Interpreter','latex','Fontsize',18)

            subplot(1,2,1)

            imagesc(kx/k0,ky/k0,reshape(poyntingpos,nxp,nyp))
            axis xy

            hold on
            rectangle('Position',[-1 -1 2 2],'Curvature',[1 1],'linewidth',2,'edgecolor','red')

            axis image
            axis equal
            title('$k_z>0$','Interpreter','latex','Fontsize',18)

            xlabel('$k_x/k_0$','Interpreter','latex','Fontsize',18)
            ylabel('$k_y/k_0$','Interpreter','latex','Fontsize',18)
            colorbar

            subplot(1,2,2)

            imagesc(kx/k0,ky/k0,reshape(poyntingneg,nxp,nyp))
            axis xy

            hold on
            rectangle('Position',[-1 -1 2 2],'Curvature',[1 1],'linewidth',2,'edgecolor','red')

            axis image
            axis equal
            title('$k_z<0$','Interpreter','latex','Fontsize',18)

            xlabel('$k_x/k_0$','Interpreter','latex','Fontsize',18)
            ylabel('$k_y/k_0$','Interpreter','latex','Fontsize',18)

            colorbar

            printfig(nprint,'poynting2d')

        else


            if (ntypefile ==0)
                load poynting.mat -ascii
            elseif (ntypefile ==2)
                poynting=h5read(namefileh5,'/Far Field/Poynting');
            end

            ray=reshape(poynting,nphi,ntheta);
            ray(nphi+1,:)=ray(1,:);
            xpo=zeros(nphi+1,ntheta);
            ypo=zeros(nphi+1,ntheta);
            zpo=zeros(nphi+1,ntheta);

            for i=1:ntheta
                for j=1:nphi+1
                    theta=pi*(i-1)/(ntheta-1);
                    phi=2*pi*(j-1)/(nphi);
                    xpo(j,i)=ray(j,i)*sin(theta)*cos(phi);
                    ypo(j,i)=ray(j,i)*sin(theta)*sin(phi);
                    zpo(j,i)=ray(j,i)*cos(theta);
                end
            end


            figure(100)
            set(100,'DefaultAxesFontName','Times')
            set(100,'DefaultAxesFontSize',12)
            set(100,'DefaultAxesFontWeight','Bold')
            set(100,'DefaultTextfontName','Times')
            set(100,'DefaultTextfontSize',12)
            set(100,'DefaultTextfontWeight','Bold')
            set(100,'Position',[1000 0 600 600])

            surf(xpo,ypo,zpo,ray)
            shading interp

            title('Poynting vector interpolated in 3D')

            xlabel('$k_x$','Interpreter','latex','Fontsize',18)
            ylabel('$k_y$','Interpreter','latex','Fontsize',18)
            zlabel('$k_z$','Interpreter','latex','Fontsize',18)
            printfig(nprint,'poynting3d')

            if (ntypefile ==0)
                load poyntingpos.mat -ascii
                load poyntingneg.mat -ascii
                load kx.mat -ascii
                load ky.mat -ascii
            elseif (ntypefile ==2)
                poyntingpos=h5read(namefileh5,'/Far Field/Poynting positive');
                poyntingneg=h5read(namefileh5,'/Far Field/Poynting negative');
                kx=h5read(namefileh5,'/Far Field/kx Poynting');
                ky=h5read(namefileh5,'/Far Field/ky Poynting');
            end

            nxp=max(size(kx));
            nyp=max(size(ky));



            figure(101)
            set(101,'DefaultAxesFontName','Times')
            set(101,'DefaultAxesFontSize',12)
            set(101,'DefaultAxesFontWeight','Bold')
            set(101,'DefaultTextfontName','Times')
            set(101,'DefaultTextfontSize',12)
            set(101,'DefaultTextfontWeight','Bold')
            set(101,'Position',[1000 0 1000 600])

            suptitle('Poynting Modulus in $k_x$ and $k_y$ plane','Interpreter','latex','Fontsize',18)

            subplot(1,2,1)

            imagesc(kx/k0,ky/k0,reshape(poyntingpos,nxp,nyp))
            axis xy

            hold on
            rectangle('Position',[-1 -1 2 2],'Curvature',[1 1],'linewidth',2,'edgecolor','red')

            axis image
            axis equal
            title('$k_z>0$','Interpreter','latex','Fontsize',18)

            xlabel('$k_x/k_0$','Interpreter','latex','Fontsize',18)
            ylabel('$k_y/k_0$','Interpreter','latex','Fontsize',18)
            colorbar

            subplot(1,2,2)

            imagesc(kx/k0,ky/k0,reshape(poyntingneg,nxp,nyp))
            axis xy

            hold on
            rectangle('Position',[-1 -1 2 2],'Curvature',[1 1],'linewidth',2,'edgecolor','red')

            axis image
            axis equal
            title('$k_z<0$','Interpreter','latex','Fontsize',18)

            xlabel('$k_x/k_0$','Interpreter','latex','Fontsize',18)
            ylabel('$k_y/k_0$','Interpreter','latex','Fontsize',18)

            colorbar

            printfig(nprint,'poynting2d')
        end

    end
    %%%%%%%%%%%%%%%% End Poynting vector %%%%%%%%%%%%%%%%%%%%%%%%%%




    %%%%%%%%%%%%%%%% Begin optical force %%%%%%%%%%%%%%%%%%%%%%%%%%
    if nforced==1

        if (ntypefile ==0)
            load forcex.mat -ascii
            load forcey.mat -ascii
            load forcez.mat -ascii
        elseif (ntypefile ==2)
            forcex=h5read(namefileh5,'/Optical Force/Optical force x component');
            forcey=h5read(namefileh5,'/Optical Force/Optical force y component');
            forcez=h5read(namefileh5,'/Optical Force/Optical force z component');
        end


        matxyforcex=reshape(forcex,nx,ny,nz);
        matxyforcey=reshape(forcey,nx,ny,nz);
        matxyforcez=reshape(forcez,nx,ny,nz);

        clear forcex
        clear forcey
        clear forcez
        xx=zeros(nx,ny,nz);
        yy=zeros(nx,ny,nz);
        zz=zeros(nx,ny,nz);
        for i=1:nz
            for j=1:ny
                for k=1:nx
                    xx(k,j,i)=x(k);yy(k,j,i)=y(j);zz(k,j,i)=z(i);
                end
            end
        end

        figure(200)
        set(200,'DefaultAxesFontName','Times')
        set(200,'DefaultAxesFontSize',12)
        set(200,'DefaultAxesFontWeight','Bold')
        set(200,'DefaultTextfontName','Times')
        set(200,'DefaultTextfontSize',12)
        set(200,'DefaultTextfontWeight','Bold')
        set(200,'Position',[0 0 1000 600])

        uicontrol('Style','text','Fontsize',16,'Fontweight','bold',...
            'units','normalized','Position', [0.25 0.9 0.5 0.1],'String','Plot Optical force');

        uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
            {'2D-view cutting planes','(x,y)-plane','(x,z)-plane','(y,z)-plane'},...
            'units','normalized','Position', [0.4 0.85 0.2 0.05],...
            'Callback',{@plotforce,nx,ny,nz,x,y,z,xx,yy,zz,matxyforcex,matxyforcey,matxyforcez,nprint});


        figure(201)
        set(201,'DefaultAxesFontName','Times')
        set(201,'DefaultAxesFontSize',12)
        set(201,'DefaultAxesFontWeight','Bold')
        set(201,'DefaultTextfontName','Times')
        set(201,'DefaultTextfontSize',12)
        set(201,'DefaultTextfontWeight','Bold')
        set(201,'Position',[0 0 1000 600])

        quiver3(xx,yy,zz,matxyforcex,matxyforcey,matxyforcez)


        xlabel('x')
        ylabel('y')
        zlabel('z')
        title('Density of optical force')

        printfig(nprint,'force3d')

    end
    %%%%%%%%%%%%%%%% End  optical force %%%%%%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%%%% Begin optical torque %%%%%%%%%%%%%%%%%%%%%%%%%%
    if ntorqued==1

        if (ntypefile ==0)
            load torquex.mat -ascii
            load torquey.mat -ascii
            load torquez.mat -ascii
        elseif (ntypefile ==2)
            torquex=h5read(namefileh5,'/Optical Force/Optical torque x component');
            torquey=h5read(namefileh5,'/Optical Force/Optical torque y component');
            torquez=h5read(namefileh5,'/Optical Force/Optical torque z component');
        end

        matxytorquex=reshape(torquex,nx,ny,nz);
        matxytorquey=reshape(torquey,nx,ny,nz);
        matxytorquez=reshape(torquez,nx,ny,nz);

        clear torquex
        clear torquey
        clear torquez

        xx=zeros(nx,ny,nz);
        yy=zeros(nx,ny,nz);
        zz=zeros(nx,ny,nz);

        for i=1:nz
            for j=1:ny
                for k=1:nx
                    xx(k,j,i)=x(k);yy(k,j,i)=y(j);zz(k,j,i)=z(i);
                end
            end
        end

        figure(300)
        set(300,'DefaultAxesFontName','Times')
        set(300,'DefaultAxesFontSize',12)
        set(300,'DefaultAxesFontWeight','Bold')
        set(300,'DefaultTextfontName','Times')
        set(300,'DefaultTextfontSize',12)
        set(300,'DefaultTextfontWeight','Bold')
        set(300,'Position',[0 0 1000 600])


        uicontrol('Style','text','Fontsize',16,'Fontweight','bold',...
            'units','normalized','Position', [0.25 0.9 0.5 0.1],'String','Plot Optical torque');

        uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
            {'2D-view cutting planes','(x,y)-plane','(x,z)-plane','(y,z)-plane'},...
            'units','normalized','Position', [0.4 0.85 0.2 0.05],...
            'Callback',{@plottorque,nx,ny,nz,x,y,z,xx,yy,zz,matxytorquex,matxytorquey,matxytorquez,nprint});


        figure(301)
        set(301,'DefaultAxesFontName','Times')
        set(301,'DefaultAxesFontSize',12)
        set(301,'DefaultAxesFontWeight','Bold')
        set(301,'DefaultTextfontName','Times')
        set(301,'DefaultTextfontSize',12)
        set(301,'DefaultTextfontWeight','Bold')
        set(301,'Position',[0 0 1000 600])

        quiver3(xx,yy,zz,matxytorquex,matxytorquey,matxytorquez)


        xlabel('x')
        ylabel('y')
        zlabel('z')
        title('Density of optical torque')

        printfig(nprint,'torque3d')

    end
    %%%%%%%%%%%%%%%%%%%%% End  optical torque %%%%%%%%%%%%%%%%%%%%%%%%%%



    %%%%%%%%%%%%%%%%%%%%%%%%%% Microscopy %%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if nlentille == 1

        if (ntypemic ==0)
            %%%%%%%%%%%%%%%%%%%%%%%%%% Holography %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if (ntypefile ==0 )
                load fourier.mat -ascii
                load fourierx.mat -ascii
                load fouriery.mat -ascii
                load fourierz.mat -ascii
                load kxfourier.mat -ascii
            elseif (ntypefile ==2 )
                kxfourier=h5read(namefileh5,'/Microscopy/kx Fourier');
                fourier=h5read(namefileh5,'/Microscopy/Fourier field modulus');
                fourierx(:,1)=h5read(namefileh5,'/Microscopy/Fourier field x component real part');
                fourierx(:,2)=h5read(namefileh5,'/Microscopy/Fourier field x component imaginary part');
                fouriery(:,1)=h5read(namefileh5,'/Microscopy/Fourier field y component real part');
                fouriery(:,2)=h5read(namefileh5,'/Microscopy/Fourier field y component imaginary part');
                fourierz(:,1)=h5read(namefileh5,'/Microscopy/Fourier field z component real part');
                fourierz(:,2)=h5read(namefileh5,'/Microscopy/Fourier field z component imaginary part');
            end

            nnfft=max(size(kxfourier));

            fourierm=reshape(fourier,nnfft,nnfft);
            fourierxc=reshape(fourierx(:,1)+icomp*fourierx(:,2),nnfft,nnfft);
            fourieryc=reshape(fouriery(:,1)+icomp*fouriery(:,2),nnfft,nnfft);
            fourierzc=reshape(fourierz(:,1)+icomp*fourierz(:,2),nnfft,nnfft);

            clear fourier
            clear fourierx
            clear fouriery
            clear fourierz

            if (nside==1 || numaperil ~= 0)

                if (ntypefile ==0 )
                    load fourierinc.mat -ascii
                    load fourierincx.mat -ascii
                    load fourierincy.mat -ascii
                    load fourierincz.mat -ascii
                elseif (ntypefile ==2 )
                    fourierinc=h5read(namefileh5,'/Microscopy/Fourier+incident field modulus');
                    fourierincx(:,1)=h5read(namefileh5,'/Microscopy/Fourier+incident field x component real part');
                    fourierincx(:,2)=h5read(namefileh5,'/Microscopy/Fourier+incident field x component imaginary part');
                    fourierincy(:,1)=h5read(namefileh5,'/Microscopy/Fourier+incident field y component real part');
                    fourierincy(:,2)=h5read(namefileh5,'/Microscopy/Fourier+incident field y component imaginary part');
                    fourierincz(:,1)=h5read(namefileh5,'/Microscopy/Fourier+incident field z component real part');
                    fourierincz(:,2)=h5read(namefileh5,'/Microscopy/Fourier+incident field z component imaginary part');
                end


                fourierincm=reshape(fourierinc,nnfft,nnfft);
                fourierincxc=reshape(fourierincx(:,1)+icomp*fourierincx(:,2),nnfft,nnfft);
                fourierincyc=reshape(fourierincy(:,1)+icomp*fourierincy(:,2),nnfft,nnfft);
                fourierinczc=reshape(fourierincz(:,1)+icomp*fourierincz(:,2),nnfft,nnfft);

                clear fourierinc
                clear fourierincx
                clear fourierincy
                clear fourierincz

            end

            if (ntypefile ==0)
                load image.mat -ascii
                load imagex.mat -ascii
                load imagey.mat -ascii
                load imagez.mat -ascii
            elseif (ntypefile == 2)
                image=h5read(namefileh5,'/Microscopy/Image field modulus');
                imagex(:,1)=h5read(namefileh5,'/Microscopy/Image field x component real part');
                imagex(:,2)=h5read(namefileh5,'/Microscopy/Image field x component imaginary part');
                imagey(:,1)=h5read(namefileh5,'/Microscopy/Image field y component real part');
                imagey(:,2)=h5read(namefileh5,'/Microscopy/Image field y component imaginary part');
                imagez(:,1)=h5read(namefileh5,'/Microscopy/Image field z component real part');
                imagez(:,2)=h5read(namefileh5,'/Microscopy/Image field z component imaginary part');
            end



            imagem=reshape(image,nfft,nfft);
            imagexc=reshape(imagex(:,1)+icomp*imagex(:,2),nfft,nfft);
            imageyc=reshape(imagey(:,1)+icomp*imagey(:,2),nfft,nfft);
            imagezc=reshape(imagez(:,1)+icomp*imagez(:,2),nfft,nfft);

            clear image
            clear imagex
            clear imagey
            clear imagez

            if (nside==1 || numaperil ~= 0)

                if (ntypefile == 0)
                    load imageinc.mat -ascii
                    load imageincx.mat -ascii
                    load imageincy.mat -ascii
                    load imageincz.mat -ascii
                elseif (ntypefile == 2)
                    imageinc=h5read(namefileh5,'/Microscopy/Image+incident field modulus');
                    imageincx(:,1)=h5read(namefileh5,'/Microscopy/Image+incident field x component real part');
                    imageincx(:,2)=h5read(namefileh5,'/Microscopy/Image+incident field x component imaginary part');
                    imageincy(:,1)=h5read(namefileh5,'/Microscopy/Image+incident field y component real part');
                    imageincy(:,2)=h5read(namefileh5,'/Microscopy/Image+incident field y component imaginary part');
                    imageincz(:,1)=h5read(namefileh5,'/Microscopy/Image+incident field z component real part');
                    imageincz(:,2)=h5read(namefileh5,'/Microscopy/Image+incident field z component imaginary part');
                end

                imageincm=reshape(imageinc,nfft,nfft);
                imageincxc=reshape(imageincx(:,1)+icomp*imageincx(:,2),nfft,nfft);
                imageincyc=reshape(imageincy(:,1)+icomp*imageincy(:,2),nfft,nfft);
                imageinczc=reshape(imageincz(:,1)+icomp*imageincz(:,2),nfft,nfft);

                clear imageinc
                clear imageincx
                clear imageincy
                clear imageincz
            end

            if (ntypefile == 0)
                load ximage.mat -ascii
            elseif (ntypefile ==2)
                ximage=h5read(namefileh5,'/Microscopy/x Image');
            end

            %[x,y,z,unitmes] = chgtunit3(x,y,z)
            %xlabel(strcat('x',unitmes))



            %%%%%%%%%%%%%%%%%%%%%%%%%%% Fourier %%%%%%%%%%%%%%%%%%%%%%%%%

            figure(400)

            set(400,'DefaultAxesFontName','Times')
            set(400,'DefaultAxesFontSize',12)
            set(400,'DefaultAxesFontWeight','Bold')
            set(400,'DefaultTextfontName','Times')
            set(400,'DefaultTextfontSize',12)
            set(400,'DefaultTextfontWeight','Bold')
            set(400,'Position',[0 0 1000 600])

            subplot('position',[0.1 0.1 0.8 0.8])

            imagesc(kxfourier,kxfourier,fourierm.^2')
            axis xy
            axis equal
            axis image

            hold on
            rectangle('Position',[-numaper -numaper 2*numaper 2*numaper],'Curvature',[1 1],'linewidth',2,'edgecolor','red')

            clim([ 0 max(max(fourierm.^2))])
            shading interp
            axis equal
            axis image
            colorbar

            xlabel('$k_x/k_0$','Interpreter','latex','Fontsize',18)
            ylabel('$k_y/k_0$','Interpreter','latex','Fontsize',18)


            uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.15 0.91 0.32 0.09],'String','Fourier plane scattered field:')

            if (nside == 1)
                uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.65 0.94 0.15 0.05],'String','kz>0')
            elseif  (nside == -1)
                uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.65 0.94 0.15 0.05],'String','kz<0')
            end

            uicontrol('Style','text','Fontsize',12,'Fontweight','bold','units','normalized','Position',[0.50 0.91 0.09 0.033],'String','NA=')
            uicontrol('Style', 'text','Fontsize',12,'Fontweight','bold', 'String', num2str(numaper),'units','normalized','Position',[0.60 0.91 0.08 0.033]);

            uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
                {'Intensity','Modulus','x-component','y-component','z-component'},...
                'units','normalized','Position',[0.48 0.94 0.15 0.05],...
                'Callback',{@plotfourier,numaper,kxfourier,fourierm,fourierxc,fourieryc,fourierzc,nprint});

            printfig(nprint,'fourier')

            %%%%%%%%%%%%%%%%%%%%%%%%%%% Fourier +incident %%%%%%%%%%%%%%%%%%%%%%%%%

            if (nside==1 || numaperil ~= 0)

                figure(450)

                set(450,'DefaultAxesFontName','Times')
                set(450,'DefaultAxesFontSize',12)
                set(450,'DefaultAxesFontWeight','Bold')
                set(450,'DefaultTextfontName','Times')
                set(450,'DefaultTextfontSize',12)
                set(450,'DefaultTextfontWeight','Bold')
                set(450,'Position',[0 0 1000 600])

                subplot('position',[0.1 0.1 0.8 0.8])

                imagesc(kxfourier,kxfourier,fourierincm.^2')
                axis xy
                axis equal
                axis image

                hold on
                rectangle('Position',[-numaper -numaper 2*numaper 2*numaper],'Curvature',[1 1],'linewidth',2,'edgecolor','red')

                clim([ 0 max(max(fourierincm^2))])
                shading interp
                axis equal
                axis image
                colorbar

                xlabel('$k_x/k_0$','Interpreter','latex','Fontsize',18)
                ylabel('$k_y/k_0$','Interpreter','latex','Fontsize',18)


                uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.15 0.91 0.32 0.09],'String','Fourier plane total field:')
                if  (nside == 1)
                    uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.65 0.94 0.15 0.05],'String','kz>0')
                elseif  (nside == -1)
                    uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.65 0.94 0.15 0.05],'String','kz<0')
                end

                uicontrol('Style','text','Fontsize',12,'Fontweight','bold','units','normalized','Position',[0.50 0.91 0.09 0.033],'String','NA=')
                uicontrol('Style', 'text','Fontsize',12,'Fontweight','bold', 'String', num2str(numaper),'units','normalized','Position',[0.60 0.91 0.08 0.033]);

                uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
                    {'Intensity','Modulus','x-component','y-component','z-component'},...
                    'units','normalized','Position',[0.48 0.94 0.15 0.05],...
                    'Callback',{@plotfourierinc,numaper,kxfourier,fourierincm,fourierincxc,fourierincyc,fourierinczc,nprint});

                printfig(nprint,'fourierinc')

            end

            %%%%%%%%%%%%%%%%% Image  %%%%%%%%%%%%%%%%%%%%%%


            figure(500)

            set(500,'DefaultAxesFontName','Times')
            set(500,'DefaultAxesFontSize',12)
            set(500,'DefaultAxesFontWeight','Bold')
            set(500,'DefaultTextfontName','Times')
            set(500,'DefaultTextfontSize',12)
            set(500,'DefaultTextfontWeight','Bold')
            set(500,'Position',[0 0 1000 600])

            subplot('position',[0.1 0.1 0.8 0.8])

            imagesc(ximage,ximage,imagem.^2')
            axis xy
            clim([ 0 max(max(imagem.^2))])
            shading interp
            axis equal
            axis image
            colorbar

            xlabel('$x$','Interpreter','latex','Fontsize',18)
            ylabel('$y$','Interpreter','latex','Fontsize',18)

            uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.05 0.94 0.40 0.06],'String','Image plane scattered field:')
            if (nside == 1)
                uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.65 0.94 0.10 0.05],'String','z>0')
            elseif  (nside == -1)
                uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.65 0.94 0.10 0.05],'String','z<0')
            end
            uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
                {'Intensity','Modulus','x-component','y-component','z-component'},...
                'units','normalized','Position',[0.48 0.94 0.15 0.05],...
                'Callback',{@plotimage,ximage,imagem,imagexc,imageyc,imagezc,nprint});

            printfig(nprint,'image')


            %%%%%%%%%%%%%%%%% Image + incident %%%%%%%%%%%%%%%%%%%%%%

            if (nside==1 || numaperil ~= 0)

                figure(550)

                set(550,'DefaultAxesFontName','Times')
                set(550,'DefaultAxesFontSize',12)
                set(550,'DefaultAxesFontWeight','Bold')
                set(550,'DefaultTextfontName','Times')
                set(550,'DefaultTextfontSize',12)
                set(550,'DefaultTextfontWeight','Bold')
                set(550,'Position',[0 0 1000 600])

                subplot('position',[0.1 0.1 0.8 0.8])

                imagesc(ximage,ximage,imageincm.^2')
                axis xy
                clim([min(min(imageincm.^2)) max(max(imageincm.^2))])
                shading interp
                axis equal
                axis image
                colorbar

                xlabel('$x$','Interpreter','latex','Fontsize',18)
                ylabel('$y$','Interpreter','latex','Fontsize',18)

                uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.05 0.94 0.40 0.06],'String','Image plane total field:')
                if (nside == 1)
                    uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.65 0.94 0.10 0.05],'String','z>0')
                elseif  (nside == -1)
                    uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.65 0.94 0.10 0.05],'String','z<0')
                end
                uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
                    {'Intensity','Modulus','x-component','y-component','z-component'},...
                    'units','normalized','Position',[0.48 0.94 0.15 0.05],...
                    'Callback',{@plotimageinc,ximage,imageincm,imageincxc,imageincyc,imageinczc,nprint});

                printfig(nprint,'imageinc')

            end
            %%%%%%%%%%%%%%%%%%%%%%%%%% bf, df, phase %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        elseif (ntypemic == 1 || ntypemic == 2 || ntypemic == 3 ||  ntypemic == 5)
            if (ntypefile == 0)
                load imagebf.mat -ascii
                load imagebfx.mat -ascii
                load imagebfy.mat -ascii
                load imagebfz.mat -ascii

            elseif(ntypefile == 2)

                if (nside==1)
                    imagebf=h5read(namefileh5,'/Microscopy/Image bright field kz>0 field modulus');
                    imagebfx(:,1)=h5read(namefileh5,'/Microscopy/Image bright field kz>0 field x component real part');
                    imagebfx(:,2)=h5read(namefileh5,'/Microscopy/Image bright field kz>0 field x component imaginary part');
                    imagebfy(:,1)=h5read(namefileh5,'/Microscopy/Image bright field kz>0 field y component real part');
                    imagebfy(:,2)=h5read(namefileh5,'/Microscopy/Image bright field kz>0 field y component imaginary part');
                    imagebfz(:,1)=h5read(namefileh5,'/Microscopy/Image bright field kz>0 field z component real part');
                    imagebfz(:,2)=h5read(namefileh5,'/Microscopy/Image bright field kz>0 field z component imaginary part');
                else
                    imagebf=h5read(namefileh5,'/Microscopy/Image bright field kz<0 field modulus');
                    imagebfx(:,1)=h5read(namefileh5,'/Microscopy/Image bright field kz<0 field x component real part');
                    imagebfx(:,2)=h5read(namefileh5,'/Microscopy/Image bright field kz<0 field x component imaginary part');
                    imagebfy(:,1)=h5read(namefileh5,'/Microscopy/Image bright field kz<0 field y component real part');
                    imagebfy(:,2)=h5read(namefileh5,'/Microscopy/Image bright field kz<0 field y component imaginary part');
                    imagebfz(:,1)=h5read(namefileh5,'/Microscopy/Image bright field kz<0 field z component real part');
                    imagebfz(:,2)=h5read(namefileh5,'/Microscopy/Image bright field kz<0 field z component imaginary part');
                end

            end


            imagem=reshape(imagebf,nfft,nfft);
            imagexc=reshape(imagebfx(:,1),nfft,nfft);
            imageyc=reshape(imagebfy(:,1),nfft,nfft);
            imagezc=reshape(imagebfz(:,1),nfft,nfft);

            clear imagebf
            clear imagebfx
            clear imagebfy
            clear imagebfz

            if (nside==1)
                if (ntypefile == 0)
                    load imageincbf.mat -ascii
                    load imageincbfx.mat -ascii
                    load imageincbfy.mat -ascii
                    load imageincbfz.mat -ascii
                elseif (ntypefile == 2)
                    imageincbf=h5read(namefileh5,'/Microscopy/Image+incident bright field kz>0 field modulus');
                    imageincbfx(:,1)=h5read(namefileh5,'/Microscopy/Image+incident bright field kz>0 field x component real part');
                    imageincbfx(:,2)=h5read(namefileh5,'/Microscopy/Image+incident bright field kz>0 field x component imaginary part');
                    imageincbfy(:,1)=h5read(namefileh5,'/Microscopy/Image+incident bright field kz>0 field y component real part');
                    imageincbfy(:,2)=h5read(namefileh5,'/Microscopy/Image+incident bright field kz>0 field y component imaginary part');
                    imageincbfz(:,1)=h5read(namefileh5,'/Microscopy/Image+incident bright field kz>0 field z component real part');
                    imageincbfz(:,2)=h5read(namefileh5,'/Microscopy/Image+incident bright field kz>0 field z component imaginary part');
                end

                imageincm=reshape(imageincbf,nfft,nfft);
                imageincxc=reshape(imageincbfx(:,1),nfft,nfft);
                imageincyc=reshape(imageincbfy(:,1),nfft,nfft);
                imageinczc=reshape(imageincbfz(:,1),nfft,nfft);

                clear imageincbf
                clear imageincbfx
                clear imageincbfy
                clear imageincbfz

            end

            if (ntypefile == 0)
                load ximage.mat -ascii
            elseif (ntypefile ==2)
                ximage=h5read(namefileh5,'/Microscopy/x Image');
            end

            %%%%%%%%%%%%%%%%% Image  %%%%%%%%%%%%%%%%%%%%%%


            figure(500)

            set(500,'DefaultAxesFontName','Times')
            set(500,'DefaultAxesFontSize',12)
            set(500,'DefaultAxesFontWeight','Bold')
            set(500,'DefaultTextfontName','Times')
            set(500,'DefaultTextfontSize',12)
            set(500,'DefaultTextfontWeight','Bold')
            set(500,'Position',[0 0 1000 600])

            subplot('position',[0.1 0.1 0.75 0.75])

            imagesc(ximage,ximage,imagem.^2')
            axis xy
            clim([ 0 max(max(imagem.^2))])
            shading interp
            axis equal
            axis image
            colorbar

            xlabel('$x$','Interpreter','latex','Fontsize',18)
            ylabel('$y$','Interpreter','latex','Fontsize',18)

            uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.05 0.94 0.40 0.06],'String','Image plane scattered field:')
            if (nside == 1)
                uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.65 0.94 0.10 0.05],'String','z>0')
            elseif  (nside == -1)
                uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.65 0.94 0.10 0.05],'String','z<0')
            end
            uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
                {'Intensity','Modulus','x-component','y-component','z-component'},...
                'Position', [460 561 150 30],...
                'Callback',{@plotimagereal,ximage,imagem,imagexc,imageyc,imagezc,nprint});

            printfig(nprint,'imagemic')


            %%%%%%%%%%%%%%%%% Image + incident %%%%%%%%%%%%%%%%%%%%%%

            if (nside==1)

                figure(550)

                set(550,'DefaultAxesFontName','Times')
                set(550,'DefaultAxesFontSize',12)
                set(550,'DefaultAxesFontWeight','Bold')
                set(550,'DefaultTextfontName','Times')
                set(550,'DefaultTextfontSize',12)
                set(550,'DefaultTextfontWeight','Bold')
                set(550,'Position',[0 0 1000 600])

                subplot('position',[0.1 0.1 0.75 0.75])

                imagesc(ximage,ximage,imageincm.^2')
                axis xy
                clim([min(min(imageincm.^2)) max(max(imageincm.^2))])
                shading interp
                axis equal
                axis image
                colorbar

                xlabel('$x$','Interpreter','latex','Fontsize',18)
                ylabel('$y$','Interpreter','latex','Fontsize',18)

                uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.05 0.94 0.40 0.06],'String','Image plane total field:')
                if (nside == 1)
                    uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.65 0.94 0.10 0.05],'String','z>0')
                elseif (nside == -1)
                    uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.65 0.94 0.10 0.05],'String','z<0')
                end
                uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
                    {'Intensity','Modulus','x-component','y-component','z-component'},...
                    'units','normalized','Position',[0.48 0.94 0.15 0.05],...
                    'Callback',{@plotimageincreal,ximage,imageincm,imageincxc,imageincyc,imageinczc,nprint});

                printfig(nprint,'imageincmic')

            end


            figure(560)
            set(560,'DefaultAxesFontName','Times')
            set(560,'DefaultAxesFontSize',12)
            set(560,'DefaultAxesFontWeight','Bold')
            set(560,'DefaultTextfontName','Times')
            set(560,'DefaultTextfontSize',12)
            set(560,'DefaultTextfontWeight','Bold')
            set(560,'Position',[0 0 1000 600])

            if (ntypefile == 0)
                load kxmic.mat -ascii
                load kymic.mat -ascii
                load masquemic.mat -ascii
            elseif (ntypefile == 2)
                kxmic=h5read(namefileh5,'/Microscopy/kxmic');
                kymic=h5read(namefileh5,'/Microscopy/kymic');
                masquemic=h5read(namefileh5,'/Microscopy/masquemic');
            end

            ii=0;
            nmasque=max(size(kxmic));
            for i=1:nmasque
                for j=1:nmasque
                    if (masquemic(i+(j-1)*nmasque) < 2)
                        ii=ii+1;kxmas(ii)=kxmic(i);kymas(ii)=kymic(j);
                    end
                end
            end
            plot(kxmas,kymas,'b+','MarkerSize',8,'LineWidth',3)
            title('Position in Fourier space of all the incident field to compute bf')
            xlabel('$k_x/k_0$','Interpreter','latex','Fontsize',18)
            ylabel('$k_y/k_0$','Interpreter','latex','Fontsize',18)

            hold on
            if (ntypemic == 1)
                rectangle('Position',[-numaperinc -numaperinc 2*numaperinc 2*numaperinc],'Curvature',[1 1],'linewidth',2,'edgecolor','red')
            elseif (ntypemic == 2)
                rectangle('Position',[-numaperinc -numaperinc 2*numaperinc 2*numaperinc],'Curvature',[1 1],'linewidth',2,'edgecolor','red')
                rectangle('Position',[-numaperinc2 -numaperinc2 2*numaperinc2 2*numaperinc2],'Curvature',[1 1],'linewidth',2,'edgecolor','red')
            elseif (ntypemic == 3)
                rectangle('Position',[-numaperinc+kcnax -numaperinc+kcnay 2*numaperinc 2*numaperinc],'Curvature',[1 1],'linewidth',2,'edgecolor','red')
            elseif (ntypemic == 5)
                rectangle('Position',[-numaperinc -numaperinc 2*numaperinc 2*numaperinc],'Curvature',[1 1],'linewidth',2,'edgecolor','red')
                rectangle('Position',[-numaperinc2 -numaperinc2 2*numaperinc2 2*numaperinc2],'Curvature',[1 1],'linewidth',2,'edgecolor','red')
            end
            axis([-1 1 -1 1]);
            axis square
    	    printfig(nprint,'angleincmic')


            %%%%%%%%%%%%%%%%%%%%%%%%%%%% df cone %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        elseif (ntypemic == 4)

            if (ntypefile == 0)
                load imagedf.mat -ascii
                load imagedfx.mat -ascii
                load imagedfy.mat -ascii
                load imagedfz.mat -ascii

            elseif(ntypefile == 2)

                if (nside==1)
                    imagedf=h5read(namefileh5,'/Microscopy/Image dark field kz>0 field modulus');
                    imagedfx(:,1)=h5read(namefileh5,'/Microscopy/Image dark field kz>0 field x component real part');
                    imagedfx(:,2)=h5read(namefileh5,'/Microscopy/Image dark field kz>0 field x component imaginary part');
                    imagedfy(:,1)=h5read(namefileh5,'/Microscopy/Image dark field kz>0 field y component real part');
                    imagedfy(:,2)=h5read(namefileh5,'/Microscopy/Image dark field kz>0 field y component imaginary part');
                    imagedfz(:,1)=h5read(namefileh5,'/Microscopy/Image dark field kz>0 field z component real part');
                    imagedfz(:,2)=h5read(namefileh5,'/Microscopy/Image dark field kz>0 field z component imaginary part');
                else
                    imagedf=h5read(namefileh5,'/Microscopy/Image dark field kz<0 field modulus');
                    imagedfx(:,1)=h5read(namefileh5,'/Microscopy/Image dark field kz<0 field x component real part');
                    imagedfx(:,2)=h5read(namefileh5,'/Microscopy/Image dark field kz<0 field x component imaginary part');
                    imagedfy(:,1)=h5read(namefileh5,'/Microscopy/Image dark field kz<0 field y component real part');
                    imagedfy(:,2)=h5read(namefileh5,'/Microscopy/Image dark field kz<0 field y component imaginary part');
                    imagedfz(:,1)=h5read(namefileh5,'/Microscopy/Image dark field kz<0 field z component real part');
                    imagedfz(:,2)=h5read(namefileh5,'/Microscopy/Image dark field kz<0 field z component imaginary part');
                end

            end





            imagem=reshape(imagedf,nfft,nfft);
            imagexc=reshape(imagedfx(:,1),nfft,nfft);
            imageyc=reshape(imagedfy(:,1),nfft,nfft);
            imagezc=reshape(imagedfz(:,1),nfft,nfft);

            clear imagedf
            clear imagedfx
            clear imagedfy
            clear imagedfz

            if (nside==1)
                if (ntypefile == 0)
                    load imageincdf.mat -ascii
                    load imageincdfx.mat -ascii
                    load imageincdfy.mat -ascii
                    load imageincdfz.mat -ascii
                elseif (ntypefile == 2)
                    imageincdf=h5read(namefileh5,'/Microscopy/Image+incident dark field kz>0 field modulus');
                    imageincdfx(:,1)=h5read(namefileh5,'/Microscopy/Image+incident dark field kz>0 field x component real part');
                    imageincdfx(:,2)=h5read(namefileh5,'/Microscopy/Image+incident dark field kz>0 field x component imaginary part');
                    imageincdfy(:,1)=h5read(namefileh5,'/Microscopy/Image+incident dark field kz>0 field y component real part');
                    imageincdfy(:,2)=h5read(namefileh5,'/Microscopy/Image+incident dark field kz>0 field y component imaginary part');
                    imageincdfz(:,1)=h5read(namefileh5,'/Microscopy/Image+incident dark field kz>0 field z component real part');
                    imageincdfz(:,2)=h5read(namefileh5,'/Microscopy/Image+incident dark field kz>0 field z component imaginary part');
                end

                imageincm=reshape(imageincdf,nfft,nfft);
                imageincxc=reshape(imageincdfx(:,1),nfft,nfft);
                imageincyc=reshape(imageincdfy(:,1),nfft,nfft);
                imageinczc=reshape(imageincdfz(:,1),nfft,nfft);

            end

            clear imageincdf
            clear imageincdfx
            clear imageincdfy
            clear imageincdfz

            if (ntypefile == 0)
                load ximage.mat -ascii
            elseif (ntypefile ==2)
                ximage=h5read(namefileh5,'/Microscopy/x Image');
            end

            %%%%%%%%%%%%%%%%% Image  %%%%%%%%%%%%%%%%%%%%%%


            figure(500)

            set(500,'DefaultAxesFontName','Times')
            set(500,'DefaultAxesFontSize',12)
            set(500,'DefaultAxesFontWeight','Bold')
            set(500,'DefaultTextfontName','Times')
            set(500,'DefaultTextfontSize',12)
            set(500,'DefaultTextfontWeight','Bold')
            set(500,'Position',[0 0 1000 600])

            subplot('position',[0.1 0.1 0.75 0.75])

            imagesc(ximage,ximage,imagem.^2')
            axis xy
            clim([ 0 max(max(imagem.^2))])
            shading interp
            axis equal
            axis image
            colorbar

            xlabel('$x$','Interpreter','latex','Fontsize',18)
            ylabel('$y$','Interpreter','latex','Fontsize',18)

            uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.05 0.94 0.40 0.06],'String','Image plane scattered field:')
            if (nside == 1)
                uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.65 0.94 0.10 0.05],'String','z>0')
            elseif  (nside == -1)
                uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.65 0.94 0.10 0.05],'String','z<0')
            end
            uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
                {'Intensity','Modulus','x-component','y-component','z-component'},...
                'units','normalized','Position',[0.48 0.94 0.15 0.05],...
                'Callback',{@plotimagereal,ximage,imagem,imagexc,imageyc,imagezc,nprint});

            printfig(nprint,'imagemic')

            %%%%%%%%%%%%%%%%% Image + incident %%%%%%%%%%%%%%%%%%%%%%

            if (nside==1)

                figure(550)

                set(550,'DefaultAxesFontName','Times')
                set(550,'DefaultAxesFontSize',12)
                set(550,'DefaultAxesFontWeight','Bold')
                set(550,'DefaultTextfontName','Times')
                set(550,'DefaultTextfontSize',12)
                set(550,'DefaultTextfontWeight','Bold')
                set(550,'Position',[0 0 1000 600])

                subplot('position',[0.1 0.1 0.75 0.75])

                imagesc(ximage,ximage,imageincm.^2')
                axis xy
                clim([min(min(imageincm.^2)) max(max(imageincm.^2))])
                shading interp
                axis equal
                axis image
                colorbar

                xlabel('$x$','Interpreter','latex','Fontsize',18)
                ylabel('$y$','Interpreter','latex','Fontsize',18)

                uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.05 0.94 0.40 0.06],'String','Image plane total field:')
                uicontrol('Style','text','Fontsize',16,'Fontweight','bold','units','normalized','Position',[0.65 0.94 0.10 0.05],'String','z>0')
                uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
                    {'Intensity','Modulus','x-component','y-component','z-component'},...
                    'units','normalized','Position',[0.48 0.94 0.15 0.05],...
                    'Callback',{@plotimageincreal,ximage,imageincm,imageincxc,imageincyc,imageinczc,nprint});

                printfig(nprint,'imageincmic')


            end



            figure(560)
            set(560,'DefaultAxesFontName','Times')
            set(560,'DefaultAxesFontSize',12)
            set(560,'DefaultAxesFontWeight','Bold')
            set(560,'DefaultTextfontName','Times')
            set(560,'DefaultTextfontSize',12)
            set(560,'DefaultTextfontWeight','Bold')
            set(560,'Position',[0 0 1000 600])

            if (ntypefile == 0)
                load kxincidentdf.mat -ascii
                load kyincidentdf.mat -ascii
            elseif (ntypefile ==2)
                kxincidentdf=h5read(namefileh5,'/Microscopy/kx incident df');
                kyincidentdf=h5read(namefileh5,'/Microscopy/ky incident df');
            end

            plot(kxincidentdf,kyincidentdf,'o','MarkerSize',5,...
                'MarkerEdgeColor','blue','MarkerFaceColor',[0 0 1])
            title('Position in Fourier space of all the incident field to compute df')
            xlabel('$k_x/k_0$','Interpreter','latex','Fontsize',18)
            ylabel('$k_y/k_0$','Interpreter','latex','Fontsize',18)
            axis xy
            axis equal
            axis image

            hold on
            rectangle('Position',[-numaperinc -numaperinc 2*numaperinc 2*numaperinc],'Curvature',[1 1],'linewidth',1,'edgecolor','red')

    	    printfig(nprint,'angleincmic')
            %%%%%%%%%%%%%%%%%%%%%%%%%%%% confocal %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        elseif (ntypemic == 6)

            if (ntypefile == 0)

                load ximage.mat -ascii
                load zimage.mat -ascii

                load fieldconfx.mat -ascii
                load fieldconfy.mat -ascii
                load fieldconfz.mat -ascii
                load fieldconfm.mat -ascii


            elseif(ntypefile == 2)
                ximage=h5read(namefileh5,'/Microscopy/x Image');
                zimage=h5read(namefileh5,'/Microscopy/z Image');
                fieldconfx(:,1)=h5read(namefileh5,'/Microscopy/Confocal field x component real part');
                fieldconfx(:,2)=h5read(namefileh5,'/Microscopy/Confocal field x component imaginary part');
                fieldconfy(:,1)=h5read(namefileh5,'/Microscopy/Confocal field y component real part');
                fieldconfy(:,2)=h5read(namefileh5,'/Microscopy/Confocal field y component imaginary part');
                fieldconfz(:,1)=h5read(namefileh5,'/Microscopy/Confocal field z component real part');
                fieldconfz(:,2)=h5read(namefileh5,'/Microscopy/Confocal field z component imaginary part');
                fieldconfm(:)=h5read(namefileh5,'/Microscopy/Confocal field modulus');
            end
            nx=max(size(ximage));
            ny=max(size(ximage));
            nz=max(size(zimage));
            imageconfocalxtmp=reshape(fieldconfx(:,1)+icomp*fieldconfx(:,2),nx,ny,nz);
            imageconfocalytmp=reshape(fieldconfy(:,1)+icomp*fieldconfy(:,2),nx,ny,nz);
            imageconfocalztmp=reshape(fieldconfz(:,1)+icomp*fieldconfz(:,2),nx,ny,nz);
            imageconfocaltmp=reshape(fieldconfm(:),nx,ny,nz);


            %%%%%%%%%%%%%%%%% supprime première ligne pour la FFT car brise la symmétrie %%%%%
            ximagetmp=ximage(2:nx);
            imageconfocalx(:,:,:)=imageconfocalxtmp(2:nx,2:nx,1:nz);
            imageconfocaly(:,:,:)=imageconfocalytmp(2:nx,2:nx,1:nz);
            imageconfocalz(:,:,:)=imageconfocalztmp(2:nx,2:nx,1:nz);
            imageconfocal(:,:,:)=imageconfocaltmp(2:nx,2:nx,1:nz);
            clear imageconfocalxtmp;
            clear imageconfocalytmp;
            clear imageconfocalztmp;
            clear imageconfocaltmp;
            clear ximage;
            ximage=ximagetmp;

            nx=nx-1;
            ny=ny-1;


            clear imageconfx;
            clear imageconfy;
            clear imageconfz;
            clear imageconfm;


            figure(500)

            set(500,'DefaultAxesFontName','Times')
            set(500,'DefaultAxesFontSize',12)
            set(500,'DefaultAxesFontWeight','Bold')
            set(500,'DefaultTextfontName','Times')
            set(500,'DefaultTextfontSize',12)
            set(500,'DefaultTextfontWeight','Bold')
            set(500,'Position',[0 0 1000 600])
            uicontrol('Style','text','Fontsize',16,'Fontweight','bold',...
                'units','normalized','Position', [0.4 0.78 0.24 0.2],'String','Confocal microscope');
            uicontrol('Style', 'popupmenu','Fontsize',12,'String',...
                {'2D-view cutting planes','(x,y)-plane','(x,z)-plane','(y,z)-plane'},...
                'units','normalized','Position', [0.45 0.65 0.15 0.05],...
                'Callback',{@plotconfocal,nx,ny,nz,ximage,ximage,zimage,imageconfocal,imageconfocalx,imageconfocaly,imageconfocalz,nprint});


            figure(560)
            set(560,'DefaultAxesFontName','Times')
            set(560,'DefaultAxesFontSize',12)
            set(560,'DefaultAxesFontWeight','Bold')
            set(560,'DefaultTextfontName','Times')
            set(560,'DefaultTextfontSize',12)
            set(560,'DefaultTextfontWeight','Bold')
            set(560,'Position',[0 0 1000 600])

            if (ntypefile == 0)
                load kxmic.mat -ascii
                load kymic.mat -ascii
                load masquemic.mat -ascii
            elseif (ntypefile == 2)
                kxmic=h5read(namefileh5,'/Microscopy/kxmic');
                kymic=h5read(namefileh5,'/Microscopy/kymic');
                masquemic=h5read(namefileh5,'/Microscopy/masquemic');
            end
            ii=0;
            nmasque=max(size(kxmic));
            for i=1:nmasque
                for j=1:nmasque
                    if (masquemic(i+(j-1)*nmasque) < 2)
                        ii=ii+1;kxmas(ii)=kxmic(i);kymas(ii)=kymic(j);
                    end
                end
            end
            plot(kxmas,kymas,'b+','MarkerSize',8,'LineWidth',3)
            title('Position in Fourier space of all the incident field to compute confocal')
            xlabel('$k_x/k_0$','Interpreter','latex','Fontsize',18)
            ylabel('$k_y/k_0$','Interpreter','latex','Fontsize',18)
            hold on
            rectangle('Position',[-numaperinc -numaperinc 2*numaperinc 2*numaperinc],'Curvature',[1 1],'linewidth',2,'edgecolor','red')

            axis([-1 1 -1 1]);
            axis square
	    printfig(nprint,'angleincconfocal')
        end
    end
end
