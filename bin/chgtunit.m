function [x,unitmes] = chgtunit(x)

xmax=max(x);

unitmes="[m]";

if (xmax <1) 
x=x*1000;
xmax=xmax*1000;
unitmes="[mm]";
end

if (xmax <1) 
x=x*1000;
xmax=xmax*1000;
unitmes="[µm]";
end

if (xmax <1) 
x=x*1000;
unitmes="[nm]";
end

end
