function plotgreenz(heps,event,x,y,z,greenr,greeni,nprint)

% Gets the value of the parameter from the slider.
Param = get(heps,'Value');

% Tranform in integer
k = int8(Param);

size(greenr)

% Puts the value of the parameter on the GUI.
uicontrol('Style', 'text', 'String', num2str(z(k),'%+1.2e\n'),...
    'Position', [560 15 80 20]);

% Plots the Graph.

figure(3)
set(3,'DefaultAxesFontName','Times')
set(3,'DefaultAxesFontSize',12)
set(3,'DefaultAxesFontWeight','Bold')
set(3,'DefaultTextfontName','Times')
set(3,'DefaultTextfontSize',12)
set(3,'DefaultTextfontWeight','Bold')
set(3,'Position',[0 600 1000 500])


subplot(1,2,1)
imagesc(x,y,greenr(:,:,k)');
axis xy

shading interp
xlabel('x')
ylabel('y')
axis image
title('Re(G)')
colorbar('vert')

subplot(1,2,2)
imagesc(x,y,greeni(:,:,k)');
axis xy

shading interp
xlabel('x')
ylabel('y')
axis image
title('Im(G)')
colorbar('vert')

if (nprint == 1)
    print('-f3','epsilon','-depsc')
end
