function plotgreen(hObject,event,nx,ny,nz,x,y,z,greenxxr,greenxxi,greenxyr,greenxyi,greenxzr,greenxzi,greenyxr,greenyxi,greenyyr,greenyyi,greenyzr,greenyzi,greenzxr,greenzxi,greenzyr,greenzyi,greenzzr,greenzzi,nprint)

val = get(hObject,'Value');

switch val


    case 1

        figure(3)
        set(3,'DefaultAxesFontName','Times')
        set(3,'DefaultAxesFontSize',12)
        set(3,'DefaultAxesFontWeight','Bold')
        set(3,'DefaultTextfontName','Times')
        set(3,'DefaultTextfontSize',12)
        set(3,'DefaultTextfontWeight','Bold')
        set(3,'Position',[0 600 1000 500])

        uicontrol('Style', 'text', 'String', num2str(z(1)),...
            'Position', [560 15 60 20]);

        maxepsr=max(max(max(greenxxr)));
        minepsr=min(min(min(greenxxr)));
        maxepsi=max(max(max(greenxxi)));
        minepsi=min(min(min(greenxxi)));

        subplot(1,2,1)
        imagesc(x,y,greenxxr(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Re(G)')
        colorbar('vert')
        if maxepsr == minepsr
            clim([maxepsr-1 maxepsr+1])
        else
            clim([minepsr maxepsr])
        end

        subplot(1,2,2)
        imagesc(x,y,greenxxi(:,:,1)')
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Im(G)')
        colorbar('vert')
        if maxepsi == minepsi
            clim([maxepsi-1 maxepsi+1])
        else
            clim([minepsi maxepsi])
        end

        uicontrol('Style', 'text', 'String', 'Choice of z',...
            'Position', [250 15 90 20]);

        uicontrol('Style', 'slider', 'Min',1,'Max', nz,...
      	  'val',1,'sliderstep',[1/(nz-1) 2/(nz-1)],...
    	  'Position', [350 10 200 30],'Callback', {@plotgreenz,x,y,z,greenxxr,greenxxi,nprint});

    case 2

        figure(3)
        set(3,'DefaultAxesFontName','Times')
        set(3,'DefaultAxesFontSize',12)
        set(3,'DefaultAxesFontWeight','Bold')
        set(3,'DefaultTextfontName','Times')
        set(3,'DefaultTextfontSize',12)
        set(3,'DefaultTextfontWeight','Bold')
        set(3,'Position',[0 600 1000 500])

        uicontrol('Style', 'text', 'String', num2str(z(1)),...
            'Position', [560 15 60 20]);

        maxepsr=max(max(max(greenxyr)));
        minepsr=min(min(min(greenxyr)));
        maxepsi=max(max(max(greenxyi)));
        minepsi=min(min(min(greenxyi)));

        subplot(1,2,1)
        imagesc(x,y,greenxyi(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Re(G)')
        colorbar('vert')
        if maxepsr == minepsr
            clim([maxepsr-1 maxepsr+1])
        else
            clim([minepsr maxepsr])
        end

        subplot(1,2,2)
        imagesc(x,y,greenxyi(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Im(G)')
        colorbar('vert')
        if maxepsi == minepsi
            clim([maxepsi-1 maxepsi+1])
        else
            clim([minepsi maxepsi])
        end

        uicontrol('Style', 'text', 'String', 'Choice of z',...
            'Position', [250 15 90 20]);

        uicontrol('Style', 'slider', 'Min',1,'Max', nz,...
      	  'val',1,'sliderstep',[1/(nz-1) 2/(nz-1)],...
    	  'Position', [350 10 200 30],'Callback', {@plotgreenz,x,y,z,greenxyr,greenxyi,nprint});

    case 3

        figure(3)
        set(3,'DefaultAxesFontName','Times')
        set(3,'DefaultAxesFontSize',12)
        set(3,'DefaultAxesFontWeight','Bold')
        set(3,'DefaultTextfontName','Times')
        set(3,'DefaultTextfontSize',12)
        set(3,'DefaultTextfontWeight','Bold')
        set(3,'Position',[0 600 1000 500])

        uicontrol('Style', 'text', 'String', num2str(z(1)),...
            'Position', [560 15 60 20]);

        maxepsr=max(max(max(greenxzr)));
        minepsr=min(min(min(greenxzr)));
        maxepsi=max(max(max(greenxzi)));
        minepsi=min(min(min(greenxzi)));

        subplot(1,2,1)
        imagesc(x,y,greenxzr(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Re(G)')
        colorbar('vert')
        if maxepsr == minepsr
            clim([maxepsr-1 maxepsr+1])
        else
            clim([minepsr maxepsr]);
        end

        subplot(1,2,2)
        imagesc(x,y,greenxzi(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Im(G)')
        colorbar('vert')
        if maxepsi == minepsi
            clim([maxepsi-1 maxepsi+1])
        else
            clim([minepsi maxepsi])
        end

        uicontrol('Style', 'text', 'String', 'Choice of z',...
            'Position', [250 15 90 20]);

        uicontrol('Style', 'slider', 'Min',1,'Max', nz,...
      	  'val',1,'sliderstep',[1/(nz-1) 2/(nz-1)],...
    	  'Position', [350 10 200 30],'Callback', {@plotgreenz,x,y,z,greenxzr,greenxzi,nprint});



    case 4

        figure(3)
        set(3,'DefaultAxesFontName','Times')
        set(3,'DefaultAxesFontSize',12)
        set(3,'DefaultAxesFontWeight','Bold')
        set(3,'DefaultTextfontName','Times')
        set(3,'DefaultTextfontSize',12)
        set(3,'DefaultTextfontWeight','Bold')
        set(3,'Position',[0 600 1000 500])

        uicontrol('Style', 'text', 'String', num2str(z(1)),...
            'Position', [560 15 60 20]);

        maxepsr=max(max(max(greenyxr)));
        minepsr=min(min(min(greenyxr)));
        maxepsi=max(max(max(greenyxi)));
        minepsi=min(min(min(greenyxi)));

        subplot(1,2,1)
        imagesc(x,y,greenyxr(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Re(G)')
        colorbar('vert')
        if maxepsr == minepsr
            clim([maxepsr-1 maxepsr+1])
        else
            clim([minepsr maxepsr])
        end

        subplot(1,2,2)
        imagesc(x,y,greenyxi(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Im(G)')
        colorbar('vert')
        if maxepsi == minepsi
            clim([maxepsi-1 maxepsi+1])
        else
            clim([minepsi maxepsi])
        end

        uicontrol('Style', 'text', 'String', 'Choice of z',...
            'Position', [250 15 90 20]);

        uicontrol('Style', 'slider', 'Min',1,'Max', nz,...
      	  'val',1,'sliderstep',[1/(nz-1) 2/(nz-1)],...
    	  'Position', [350 10 200 30],'Callback', {@plotgreenz,x,y,z,greenyxr,greenyxi,nprint});

    case 5

        figure(3)
        set(3,'DefaultAxesFontName','Times')
        set(3,'DefaultAxesFontSize',12)
        set(3,'DefaultAxesFontWeight','Bold')
        set(3,'DefaultTextfontName','Times')
        set(3,'DefaultTextfontSize',12)
        set(3,'DefaultTextfontWeight','Bold')
        set(3,'Position',[0 600 1000 500])

        uicontrol('Style', 'text', 'String', num2str(z(1)),...
            'Position', [560 15 60 20]);

        maxepsr=max(max(max(greenyyr)));
        minepsr=min(min(min(greenyyr)));
        maxepsi=max(max(max(greenyyi)));
        minepsi=min(min(min(greenyyi)));

        subplot(1,2,1)
        imagesc(x,y,greenyyr(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Re(G)')
        colorbar('vert')
        if maxepsr == minepsr
            clim([maxepsr-1 maxepsr+1])
        else
            clim([minepsr maxepsr])
        end

        subplot(1,2,2)
        imagesc(x,y,greenyyi(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Im(G)')
        colorbar('vert')
        if maxepsi == minepsi
            clim([maxepsi-1 maxepsi+1])
        else
            clim([minepsi maxepsi])
        end

        uicontrol('Style', 'text', 'String', 'Choice of z',...
            'Position', [250 15 90 20]);

        uicontrol('Style', 'slider', 'Min',1,'Max', nz,...
      	  'val',1,'sliderstep',[1/(nz-1) 2/(nz-1)],...
    	  'Position', [350 10 200 30],'Callback', {@plotgreenz,x,y,z,greenyyr,greenyyi,nprint});

    case 6

        figure(3)
        set(3,'DefaultAxesFontName','Times')
        set(3,'DefaultAxesFontSize',12)
        set(3,'DefaultAxesFontWeight','Bold')
        set(3,'DefaultTextfontName','Times')
        set(3,'DefaultTextfontSize',12)
        set(3,'DefaultTextfontWeight','Bold')
        set(3,'Position',[0 600 1000 500])

        uicontrol('Style', 'text', 'String', num2str(z(1)),...
            'Position', [560 15 60 20]);

        maxepsr=max(max(max(greenyzr)));
        minepsr=min(min(min(greenyzr)));
        maxepsi=max(max(max(greenyzi)));
        minepsi=min(min(min(greenyzi)));

        subplot(1,2,1)
        imagesc(x,y,greenyzr(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Re(G)')
        colorbar('vert')
        if maxepsr == minepsr
            clim([maxepsr-1 maxepsr+1])
        else
            clim([minepsr maxepsr])
        end

        subplot(1,2,2)
        imagesc(x,y,greenyzi(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Im(G)')
        colorbar('vert')
        if maxepsi == minepsi
            clim([maxepsi-1 maxepsi+1])
        else
            clim([minepsi maxepsi])
        end

        uicontrol('Style', 'text', 'String', 'Choice of z',...
            'Position', [250 15 90 20]);

        uicontrol('Style', 'slider', 'Min',1,'Max', nz,...
      	  'val',1,'sliderstep',[1/(nz-1) 2/(nz-1)],...
    	  'Position', [350 10 200 30],'Callback', {@plotgreenz,x,y,z,greenyzr,greenyzi,nprint});



    case 7

        figure(3)
        set(3,'DefaultAxesFontName','Times')
        set(3,'DefaultAxesFontSize',12)
        set(3,'DefaultAxesFontWeight','Bold')
        set(3,'DefaultTextfontName','Times')
        set(3,'DefaultTextfontSize',12)
        set(3,'DefaultTextfontWeight','Bold')
        set(3,'Position',[0 600 1000 500])

        uicontrol('Style', 'text', 'String', num2str(z(1)),...
            'Position', [560 15 60 20]);

        maxepsr=max(max(max(greenzxr)));
        minepsr=min(min(min(greenzxr)));
        maxepsi=max(max(max(greenzxi)));
        minepsi=min(min(min(greenzxi)));

        subplot(1,2,1)
        imagesc(x,y,greenzxr(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Re(G)')
        colorbar('vert')
        if maxepsr == minepsr
            clim([maxepsr-1 maxepsr+1])
        else
            clim([minepsr maxepsr])
        end

        subplot(1,2,2)
        imagesc(x,y,greenzxi(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Im(G)')
        colorbar('vert')
        if maxepsi == minepsi
            clim([maxepsi-1 maxepsi+1])
        else
            clim([minepsi maxepsi])
        end

        uicontrol('Style', 'text', 'String', 'Choice of z',...
            'Position', [250 15 90 20]);

        uicontrol('Style', 'slider', 'Min',1,'Max', nz,...
      	  'val',1,'sliderstep',[1/(nz-1) 2/(nz-1)],...
    	  'Position', [350 10 200 30],'Callback', {@plotgreenz,x,y,z,greenzxr,greenzxi,nprint});

    case 8

        figure(3)
        set(3,'DefaultAxesFontName','Times')
        set(3,'DefaultAxesFontSize',12)
        set(3,'DefaultAxesFontWeight','Bold')
        set(3,'DefaultTextfontName','Times')
        set(3,'DefaultTextfontSize',12)
        set(3,'DefaultTextfontWeight','Bold')
        set(3,'Position',[0 600 1000 500])

        uicontrol('Style', 'text', 'String', num2str(z(1)),...
            'Position', [560 15 60 20]);

        maxepsr=max(max(max(greenzyr)));
        minepsr=min(min(min(greenzyr)));
        maxepsi=max(max(max(greenzyi)));
        minepsi=min(min(min(greenzyi)));

        subplot(1,2,1)
        imagesc(x,y,greenzyr(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Re(G)')
        colorbar('vert')
        if maxepsr == minepsr
            clim([maxepsr-1 maxepsr+1])
        else
            clim([minepsr maxepsr])
        end

        subplot(1,2,2)
        imagesc(x,y,greenzyi(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Im(G)')
        colorbar('vert')
        if maxepsi == minepsi
            clim([maxepsi-1 maxepsi+1])
        else
            clim([minepsi maxepsi])
        end

        uicontrol('Style', 'text', 'String', 'Choice of z',...
            'Position', [250 15 90 20]);

        uicontrol('Style', 'slider', 'Min',1,'Max', nz,...
      	  'val',1,'sliderstep',[1/(nz-1) 2/(nz-1)],...
    	  'Position', [350 10 200 30],'Callback', {@plotgreenz,x,y,z,greenzyr,greenzyi,nprint});

    case 9

        figure(3)
        set(3,'DefaultAxesFontName','Times')
        set(3,'DefaultAxesFontSize',12)
        set(3,'DefaultAxesFontWeight','Bold')
        set(3,'DefaultTextfontName','Times')
        set(3,'DefaultTextfontSize',12)
        set(3,'DefaultTextfontWeight','Bold')
        set(3,'Position',[0 600 1000 500])

        uicontrol('Style', 'text', 'String', num2str(z(1)),...
            'Position', [560 15 60 20]);

        maxepsr=max(max(max(greenzzr)));
        minepsr=min(min(min(greenzzr)));
        maxepsi=max(max(max(greenzzi)));
        minepsi=min(min(min(greenzzi)));

        subplot(1,2,1)
        imagesc(x,y,greenzzr(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Re(G)')
        colorbar('vert')
        if maxepsr == minepsr
            clim([maxepsr-1 maxepsr+1])
        else
            clim([minepsr maxepsr])
        end

        subplot(1,2,2)
        imagesc(x,y,greenzzi(:,:,1)');
        axis xy

        shading interp
        xlabel('x')
        ylabel('y')
        axis image
        title('Im(G)')
        colorbar('vert')
        if maxepsi == minepsi
            clim([maxepsi-1 maxepsi+1])
        else
            clim([minepsi maxepsi])
        end

        uicontrol('Style', 'text', 'String', 'Choice of z',...
            'Position', [250 15 90 20]);

        uicontrol('Style', 'slider', 'Min',1,'Max', nz,...
      	  'val',1,'sliderstep',[1/(nz-1) 2/(nz-1)],...
    	  'Position', [350 10 200 30],'Callback', {@plotgreenz,x,y,z,greenzzr,greenzzi,nprint});


end
