c     k0 : nombre d'onde
c     xx0 : position de la focalisation en x
c     yy0 : position de la focalisation en y
c     zz0 : position de la focalisation en z
c     psit : rotation du champ electrique psit=0 suivant x et psit=90 suivant y (en degré)
c     E0 : amplitude des ondes planes qui forment le confocal. on peut le prendre égal a 1.
c     xs,ys,zs position ou est calculé le champ confocal: taille maximum nmax
c     nx,ny,nz nombre de position suivant x(y,z) ou est calculé le champ confocal: nx*ny*nz<nmax
c     FF0 : vecteur de sortie ou le champ est calculé. Attention il y a les composantes du champ à l'intérieur
c     3 k+1 x,3 k+2 y et 3 k +3 z
c     aretecube : discrétisation cubique du maillage de l'espace
c     ndipole : =nx*ny*nz
c     nmax : nombre de point de discrétisation max.
c     nfft2d : nfft2d taille du tableau pour faire les FFT sur confocal: supérieur a nx,ny. détermine le delta k
c     nfft2dmax : taille maxi des tableaux pour les FFT.
c     Egausxref,Egausyref,Egauszref : tableaux temporaires
c     P0 : puissance totale désirée pour le confocal
c     irra : sortie Irradiance au point de focalisation
c     numaperil : ouverture numérique du confocal.
c     planp : plan défini pour le calcul des FFT : a definir dans le main par
c     call dfftw_plan_dft_2d(plan2b, nfft2d,nfft2d, Eimagex, Eimagex,FFTW_FORWARD,FFTW_ESTIMATE)
      
      subroutine confocal(k0,xx0,yy0 ,zz0,psit,E0 ,xs,ys,zs ,FF0
     $     ,aretecube,nx,ny,nz ,ndipole,nmax,nfft2d ,nfft2dmax
     $     ,Egausxref,Egausyref,Egauszref ,P0 ,irra ,numaperil,nstop
     $     ,infostr ,planb)
      implicit none
      integer neps,nepsmax,ndipole,nmax,nfft2d,nfft2dmax,nstop,nx,ny,nz
     $     ,i
      double precision xs(nmax),ys(nmax) ,zs(nmax),psit,k0,aretecube,tmp
     $     ,P0,numaperil
      double complex  FF0(3*nmax),E0
      integer nfft2d2,nkx,nky,k,ii,jj,kk,indice,kkk,nnn,test
      double precision kx,ky,kz,kxx,kyy,kzz,deltak,kzt ,kmax,kp2,psi,pi
     $     ,const,const1,xx0,yy0,zz0,x0,y0,z0,fac,fluxinc ,irra
      double complex Egausxref(nfft2dmax *nfft2dmax),Egausyref(nfft2dmax
     $     *nfft2dmax) ,Egauszref(nfft2dmax*nfft2dmax) ,Axp ,Ayp ,Azp
     $     ,Axs,Ays,Azs,Ax,Ay,Az,icomp,exparg,const2
      character(64) infostr
      integer FFTW_BACKWARD
      integer*8 planb

c     changement unite angle, psi =0 defini pol p
      FFTW_BACKWARD=+1
      pi=dacos(-1.d0)
      psi=psit*pi/180.d0
      icomp=(0.d0,1.d0)
c     calcul du pas de discretisationen delta k
      nfft2d2=nfft2d/2
      kmax=k0*numaperil
      deltak=2.d0*pi/(dble(nfft2d)*aretecube)
      if (deltak.ge.kmax/2.d0) then
         nstop=1
         infostr='nfft2d too small for confocal'
         return
      endif
      fluxinc=0.d0
c     translation du faisceau gaussien pour que l'objet soit bien placé
c     pour le premier indice en -N/2dx et -N/2dy.
      x0=xx0-(xs(1)+dble(nfft2d2)*aretecube)
      y0=yy0-(ys(1)+dble(nfft2d2)*aretecube)
      z0=zz0

      if (nx.ge.nfft2d) then
         nstop=1
         infostr='object larger than the FFT box along x'
         return
      endif
      if (ny.eq.nfft2d) then
         nstop=1
         infostr='object larger than the FFT box along y'
         return
      endif
      if (nstop.eq.1) return
      fac=deltak*deltak

c     Boucle sur en z 
      do k=1,nz
c     kk indice de la cote suivant z
         kk=1+nx*ny*(k-1)

c     initialise
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)   
         do i=1,nfft2dmax*nfft2dmax
            Egausxref(i)=0.d0
            Egausyref(i)=0.d0
            Egauszref(i)=0.d0
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL  

c     Commence la boucle sur les delta k dans le repere x,y,z de la
c     surface

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(nkx,nky,ii,jj,kx,ky,kp2,kz)   
!$OMP& PRIVATE(const1,const2,Axp,Azp,Ays,Azs,Ax,Ay,Az,indice)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
         do nkx=-nfft2d2,nfft2d2-1
            do nky=-nfft2d2,nfft2d2-1

c     range pour la FFT suivant x
            if (nkx.ge.0) then
               ii=nkx+1
            else
               ii=nfft2d+nkx+1
            endif
c     range pour la FFT suivant y
               if (nky.ge.0) then
                  jj=nky+1
               else
                  jj=nfft2d+nky+1
               endif

               kx=deltak*dble(nkx)
               ky=deltak*dble(nky)
               kp2=kx*kx+ky*ky
               if (kp2.le.kmax*kmax*0.999d0) then
c     le faisceau se deplace vers les z positifs
                  kz=dsqrt(k0*k0-kp2)
c     polarisation p
                  const1=1.d0/dsqrt(kz*kz+kx*kx)
                  Axp=kz*const1              
                  Azp=-kx*const1
c     polarisation s
                  const1=1.d0/dsqrt(kz*kz+ky*ky)                 
                  Ays=kz*const1
                  Azs=-ky*const1 
c     composition des deux polarisations
                  Ax=E0*dcos(psi)*Axp
                  Ay=E0*dsin(psi)*Ays
                  Az=E0*(dcos(psi)*Azp+dsin(psi)*Azs)
c     changement de base pour retourner dans la base de la surface
                  exparg=cdexp(icomp*kz*zs(kk))               
                  indice=ii+nfft2d*(jj-1)
                  const2=cdexp(-icomp*(kx*x0+ky*y0+kz*z0))
                  Egausxref(indice)=Ax*const2*exparg
                  Egausyref(indice)=Ay*const2*exparg
                  Egauszref(indice)=Az*const2*exparg

               endif
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL  

c     fin boucle en delta k
c     calcul de la FFT
#ifdef USE_FFTW
         call dfftw_execute_dft(planb,Egausxref,Egausxref)
         call dfftw_execute_dft(planb,Egausyref,Egausyref)
         call dfftw_execute_dft(planb,Egauszref,Egauszref)
#else
!$OMP PARALLEL DEFAULT(SHARED)
!$OMP SECTIONS 
!$OMP SECTION   
         call fftsingletonz2d(Egausxref,nfft2d,nfft2d,FFTW_BACKWARD)
!$OMP SECTION   
         call fftsingletonz2d(Egausyref,nfft2d,nfft2d,FFTW_BACKWARD)
!$OMP SECTION  
         call fftsingletonz2d(Egauszref,nfft2d,nfft2d,FFTW_BACKWARD)
!$OMP END SECTIONS
!$OMP END PARALLEL
#endif
c     shift+remet dans FF0, le champ incident
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(nkx,nky,ii,jj,indice,nnn,kkk)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
         do nkx=-nfft2d2,-nfft2d2+nx-1
            do nky=-nfft2d2,-nfft2d2+ny-1
c     range pour la FFT suivant x
               if (nkx.ge.0) then
                  ii=nkx+1
               else
                  ii=nfft2d+nkx+1
               endif
c     range pour la FFT suivant y
               if (nky.ge.0) then
                  jj=nky+1
               else
                  jj=nfft2d+nky+1
               endif  

               indice=ii+nfft2d*(jj-1)
c     nnn c'est le numero du dipole dans la boite object qui correspond
c     a une maille de la FFT nkx,nky=indice
               nnn=(nkx+nfft2d2+1)+nx*(nky+nfft2d2)+nx*ny*(k-1)
               kkk=3*(nnn-1)
               FF0(kkk+1)=Egausxref(indice)*fac
               FF0(kkk+2)=Egausyref(indice)*fac
               FF0(kkk+3)=Egauszref(indice)*fac
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL  

      enddo

c      FF0=0.d0
      
      do k=1,nz
         kk=1+nx*ny*(k-1)
c     initialise
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)   
         do i=1,nfft2dmax*nfft2dmax
            Egausxref(i)=0.d0
            Egausyref(i)=0.d0
            Egauszref(i)=0.d0
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL  
        
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(nkx,nky,ii,jj,kx,ky,kp2,kz)   
!$OMP& PRIVATE(const1,const2,Axp,Azp,Ays,Azs,Ax,Ay,Az,indice)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
         do nkx=-nfft2d2,nfft2d2-1
            do nky=-nfft2d2,nfft2d2-1

c     range pour la FFT suivant x
            if (nkx.ge.0) then
               ii=nkx+1
            else
               ii=nfft2d+nkx+1
            endif
c     range pour la FFT suivant y
               if (nky.ge.0) then
                  jj=nky+1
               else
                  jj=nfft2d+nky+1
               endif

               kx=deltak*dble(nkx)
               ky=deltak*dble(nky)
               kp2=kx*kx+ky*ky
               if (kp2.le.kmax*kmax*0.999d0) then
c     le faisceau se deplace vers les z négatifs
                  kz=-dsqrt(k0*k0-kp2)
c     polarisation p
                  const1=1.d0/dsqrt(kz*kz+kx*kx)
                  Axp=kz*const1              
                  Azp=-kx*const1
c     polarisation s
                  const1=1.d0/dsqrt(kz*kz+ky*ky)                 
                  Ays=kz*const1
                  Azs=-ky*const1 
c     composition des deux polarisations
                  Ax=E0*dcos(psi)*Axp
                  Ay=E0*dsin(psi)*Ays
                  Az=E0*(dcos(psi)*Azp+dsin(psi)*Azs)
c     changement de base pour retourner dans la base de la surface
                  exparg=cdexp(icomp*kz*zs(kk))               
                  indice=ii+nfft2d*(jj-1)
                  const2=cdexp(-icomp*(kx*x0+ky*y0+kz*z0))
                  Egausxref(indice)=-Ax*const2*exparg
                  Egausyref(indice)=-Ay*const2*exparg
                  Egauszref(indice)=-Az*const2*exparg

               endif
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL  

c     fin boucle en delta k
c     calcul de la FFT
#ifdef USE_FFTW
         call dfftw_execute_dft(planb,Egausxref,Egausxref)
         call dfftw_execute_dft(planb,Egausyref,Egausyref)
         call dfftw_execute_dft(planb,Egauszref,Egauszref)
#else
!$OMP PARALLEL DEFAULT(SHARED)
!$OMP SECTIONS 
!$OMP SECTION   
         call fftsingletonz2d(Egausxref,nfft2d,nfft2d,FFTW_BACKWARD)
!$OMP SECTION   
         call fftsingletonz2d(Egausyref,nfft2d,nfft2d,FFTW_BACKWARD)
!$OMP SECTION  
         call fftsingletonz2d(Egauszref,nfft2d,nfft2d,FFTW_BACKWARD)
!$OMP END SECTIONS
!$OMP END PARALLEL
#endif
c     shift+remet dans FF0, le champ incident
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(nkx,nky,ii,jj,indice,nnn,kkk)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
         do nkx=-nfft2d2,-nfft2d2+nx-1
            do nky=-nfft2d2,-nfft2d2+ny-1
c     range pour la FFT suivant x
               if (nkx.ge.0) then
                  ii=nkx+1
               else
                  ii=nfft2d+nkx+1
               endif
c     range pour la FFT suivant y
               if (nky.ge.0) then
                  jj=nky+1
               else
                  jj=nfft2d+nky+1
               endif  

               indice=ii+nfft2d*(jj-1)
c     nnn c'est le numero du dipole dans la boite object qui correspond
c     a une maille de la FFT nkx,nky=indice
               nnn=(nkx+nfft2d2+1)+nx*(nky+nfft2d2)+nx*ny*(k-1)
               kkk=3*(nnn-1)
               FF0(kkk+1)=FF0(kkk+1)+Egausxref(indice)*fac
               FF0(kkk+2)=FF0(kkk+2)+Egausyref(indice)*fac
               FF0(kkk+3)=FF0(kkk+3)+Egauszref(indice)*fac
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL  

      enddo
   
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(nkx,nky,kx,ky,kp2,kz,indice)   
!$OMP& PRIVATE(const1,Axp,Azp,Ayp,Azs,Ax,Ay,Az)
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:fluxinc)
!$OMP&  COLLAPSE(2)      
      do nkx=-nfft2d2,nfft2d2-1
         do nky=-nfft2d2,nfft2d2-1
            kx=deltak*dble(nkx)
            ky=deltak*dble(nky)
            kp2=kx*kx+ky*ky
            if (kp2.le.kmax*kmax*0.999d0) then
c     le faisceau se deplace vers les z positifs
               kz=dsqrt(k0*k0-kp2) 
c     polarisation p
               const1=1.d0/dsqrt(kz*kz+kx*kx)
               Axp=kz*const1              
               Azp=-kx*const1
c     polarisation s
               const1=1.d0/dsqrt(kz*kz+ky*ky)             
               Ays=kz*const1
               Azs=-ky*const1
c     composition des deux polarisations
               Ax=E0*dcos(psi)*Axp
               Ay=E0*dsin(psi)*Ays
               Az=E0*(dcos(psi)*Azp+dsin(psi)*Azs)
c     calcul du fluxinc: somme des fluxinc de chacunes des ondes planes
               fluxinc=fluxinc+(cdabs(Ax)**2+cdabs(Ay)**2 +cdabs(Az)
     $              **2)*kz           
            endif
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL  

      fluxinc=fluxinc*2.d0
      irra=fac/(8.d0*pi*1.d-7*299792458.d0)/k0*4.d0*pi*pi
      fluxinc=fluxinc*irra
      tmp=P0/fluxinc
      irra=P0/pi*k0*k0
      tmp=dsqrt(tmp)
      E0=E0*tmp
      fluxinc=P0
      write(*,*) 'Irradiance',irra,'W/m2',tmp
      write(*,*) 'Field',E0,'V/m'
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)        
      do i=1,ndipole
         FF0(3*i-2)=FF0(3*i-2)*tmp
         FF0(3*i-1)=FF0(3*i-1)*tmp
         FF0(3*i)=FF0(3*i)*tmp
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

      end

c**********************************************************************
c**********************************************************************
      subroutine demiconfocal(k0,xx0,yy0 ,zz0,psit,E0 ,xs,ys,zs ,FF0
     $     ,aretecube,nx,ny,nz ,ndipole,nmax,nfft2d ,nfft2dmax
     $     ,Egausxref,Egausyref,Egauszref ,P0 ,irra, numaperil,nstop
     $     ,infostr ,planb)
      implicit none
      integer neps,nepsmax,ndipole,nmax,nfft2d,nfft2dmax,nstop,nx,ny,nz
     $     ,i
      double precision xs(nmax),ys(nmax) ,zs(nmax),psit,k0,aretecube,tmp
     $     ,P0,numaperil
      double complex  FF0(3*nmax),E0
      integer nfft2d2,nkx,nky,k,ii,jj,kk,indice,kkk,nnn,test
      double precision kx,ky,kz,kxx,kyy,kzz,deltak,kzt ,kmax,kp2,psi,pi
     $     ,const,const1,xx0,yy0,zz0,x0,y0,z0,fac,fluxinc ,irra
      double complex Egausxref(nfft2dmax *nfft2dmax),Egausyref(nfft2dmax
     $     *nfft2dmax) ,Egauszref(nfft2dmax*nfft2dmax) ,Axp ,Ayp ,Azp
     $     ,Axs,Ays,Azs,Ax,Ay,Az,icomp,exparg,const2
      character(64) infostr
      integer FFTW_BACKWARD
      integer*8 planb


c     changement unite angle, psi =0 defini pol p
      FFTW_BACKWARD=+1
      pi=dacos(-1.d0)
      psi=psit*pi/180.d0
      icomp=(0.d0,1.d0)
c     calcul du pas de discretisationen delta k
      nfft2d2=nfft2d/2
      kmax=k0*numaperil

      deltak=2.d0*pi/(dble(nfft2d)*aretecube)
      if (deltak.ge.kmax/2.d0) then
         nstop=1
         infostr='nfft2d too small for confocal'
         return
      endif
      
      fluxinc=0.d0
c     translation du faisceau gaussien pour que l'objet soit bien placé
c     pour le premier indice en -N/2dx et -N/2dy.
      x0=xx0-(xs(1)+dble(nfft2d2)*aretecube)
      y0=yy0-(ys(1)+dble(nfft2d2)*aretecube)
      z0=zz0

      if (nx.ge.nfft2d) then
         nstop=1
         infostr='object larger than the FFT box along x'
         return
      endif
      if (ny.eq.nfft2d) then
         nstop=1
         infostr='object larger than the FFT box along y'
         return
      endif
      if (nstop.eq.1) return
      fac=deltak*deltak

c     Boucle sur en z 
      do k=1,nz
c     kk indice de la cote suivant z
         kk=1+nx*ny*(k-1)

c     initialise
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)   
         do i=1,nfft2dmax*nfft2dmax
            Egausxref(i)=0.d0
            Egausyref(i)=0.d0
            Egauszref(i)=0.d0
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL  

c     Commence la boucle sur les delta k dans le repere x,y,z de la
c     surface

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(nkx,nky,ii,jj,kx,ky,kp2,kz)   
!$OMP& PRIVATE(const1,const2,Axp,Azp,Ays,Azs,Ax,Ay,Az,indice)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
         do nkx=-nfft2d2,nfft2d2-1
            do nky=-nfft2d2,nfft2d2-1

c     range pour la FFT suivant x
            if (nkx.ge.0) then
               ii=nkx+1
            else
               ii=nfft2d+nkx+1
            endif
c     range pour la FFT suivant y
               if (nky.ge.0) then
                  jj=nky+1
               else
                  jj=nfft2d+nky+1
               endif

               kx=deltak*dble(nkx)
               ky=deltak*dble(nky)
               kp2=kx*kx+ky*ky
               if (kp2.le.kmax*kmax*0.999d0) then
c     le faisceau se deplace vers les z positifs
                  kz=dsqrt(k0*k0-kp2)
c     polarisation p
                  const1=1.d0/dsqrt(kz*kz+kx*kx)
                  Axp=kz*const1              
                  Azp=-kx*const1
c     polarisation s
                  const1=1.d0/dsqrt(kz*kz+ky*ky)                 
                  Ays=kz*const1
                  Azs=-ky*const1 
c     composition des deux polarisations
                  Ax=E0*dcos(psi)*Axp
                  Ay=E0*dsin(psi)*Ays
                  Az=E0*(dcos(psi)*Azp+dsin(psi)*Azs)
c     changement de base pour retourner dans la base de la surface
                  exparg=cdexp(icomp*kz*zs(kk))               
                  indice=ii+nfft2d*(jj-1)
                  const2=cdexp(-icomp*(kx*x0+ky*y0+kz*z0))
                  Egausxref(indice)=Ax*const2*exparg
                  Egausyref(indice)=Ay*const2*exparg
                  Egauszref(indice)=Az*const2*exparg

               endif
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL  

c     fin boucle en delta k
c     calcul de la FFT
#ifdef USE_FFTW
         call dfftw_execute_dft(planb,Egausxref,Egausxref)
         call dfftw_execute_dft(planb,Egausyref,Egausyref)
         call dfftw_execute_dft(planb,Egauszref,Egauszref)
#else
!$OMP PARALLEL DEFAULT(SHARED)
!$OMP SECTIONS 
!$OMP SECTION   
         call fftsingletonz2d(Egausxref,nfft2d,nfft2d,FFTW_BACKWARD)
!$OMP SECTION   
         call fftsingletonz2d(Egausyref,nfft2d,nfft2d,FFTW_BACKWARD)
!$OMP SECTION  
         call fftsingletonz2d(Egauszref,nfft2d,nfft2d,FFTW_BACKWARD)
!$OMP END SECTIONS
!$OMP END PARALLEL
#endif
c     shift+remet dans FF0, le champ incident
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(nkx,nky,ii,jj,indice,nnn,kkk)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
         do nkx=-nfft2d2,-nfft2d2+nx-1
            do nky=-nfft2d2,-nfft2d2+ny-1
c     range pour la FFT suivant x
               if (nkx.ge.0) then
                  ii=nkx+1
               else
                  ii=nfft2d+nkx+1
               endif
c     range pour la FFT suivant y
               if (nky.ge.0) then
                  jj=nky+1
               else
                  jj=nfft2d+nky+1
               endif  

               indice=ii+nfft2d*(jj-1)
c     nnn c'est le numero du dipole dans la boite object qui correspond
c     a une maille de la FFT nkx,nky=indice
               nnn=(nkx+nfft2d2+1)+nx*(nky+nfft2d2)+nx*ny*(k-1)
               kkk=3*(nnn-1)
               FF0(kkk+1)=Egausxref(indice)*fac
               FF0(kkk+2)=Egausyref(indice)*fac
               FF0(kkk+3)=Egauszref(indice)*fac
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL  

      enddo

   
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(nkx,nky,kx,ky,kp2,kz,indice)   
!$OMP& PRIVATE(const1,Axp,Azp,Ayp,Azs,Ax,Ay,Az)
!$OMP DO SCHEDULE(STATIC)  REDUCTION(+:fluxinc)
!$OMP&  COLLAPSE(2)      
      do nkx=-nfft2d2,nfft2d2-1
         do nky=-nfft2d2,nfft2d2-1
            kx=deltak*dble(nkx)
            ky=deltak*dble(nky)
            kp2=kx*kx+ky*ky
            if (kp2.le.kmax*kmax*0.999d0) then
c     le faisceau se deplace vers les z positifs
               kz=dsqrt(k0*k0-kp2) 
c     polarisation p
               const1=1.d0/dsqrt(kz*kz+kx*kx)
               Axp=kz*const1              
               Azp=-kx*const1
c     polarisation s
               const1=1.d0/dsqrt(kz*kz+ky*ky)             
               Ays=kz*const1
               Azs=-ky*const1
c     composition des deux polarisations
               Ax=E0*dcos(psi)*Axp
               Ay=E0*dsin(psi)*Ays
               Az=E0*(dcos(psi)*Azp+dsin(psi)*Azs)
c     calcul du fluxinc: somme des fluxinc de chacunes des ondes planes
               fluxinc=fluxinc+(cdabs(Ax)**2+cdabs(Ay)**2 +cdabs(Az)
     $              **2)*kz           
            endif
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL  

      irra=fac/(8.d0*pi*1.d-7*299792458.d0)/k0*4.d0*pi*pi
      fluxinc=fluxinc*irra
      tmp=P0/fluxinc
      irra=P0/pi*k0*k0
      tmp=dsqrt(tmp)
      E0=E0*tmp
      fluxinc=P0
      write(*,*) 'Irradiance',irra,'W/m2',tmp
      write(*,*) 'Field',E0,'V/m'
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)        
      do i=1,ndipole
         FF0(3*i-2)=FF0(3*i-2)*tmp
         FF0(3*i-1)=FF0(3*i-1)*tmp
         FF0(3*i)=FF0(3*i)*tmp
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         
  

      end
