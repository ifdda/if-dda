c$$$  implicit none
c$$$  integer LDA,NDIM,NLAR,NOU,STATUS,STEPERR,NLOOP,MAXIT,i,j,IM,ns
c$$$  parameter(lda=200,nlar=40)
c$$$  double precision NORM,TOL,TOLE,RESIDU
c$$$  double complex XI(lda),XR(lda),xsol(lda),B(lda),WRK(lda,nlar)
c$$$  $     ,mat(lda,lda),diago(lda),OMEGA,icomp
c$$$  
c$$$  
c$$$  ndim=100
c$$$  ns=1
c$$$  icomp=(0.d0,1.d0)
c$$$  do i=1,ndim
c$$$  do j=1,ndim
c$$$  mat(i,j)=0.d0
c$$$  enddo
c$$$  mat(i,i)=(1.d0,0.d0)*dble(i)
c$$$  b(i)=(1.d0,0.d0)
c$$$  xi(i)=(0.d0,0.d0)
c$$$  enddo
c$$$  
c$$$  tol=1.d-3
c$$$  tole=tol
c$$$  nloop=0
c$$$  nou=0
c$$$  MAXIT=100
c$$$  
c$$$  do i=1,ndim
c$$$  b(i)=b(i)
c$$$  xi(i)=xi(i)
c$$$  enddo
c$$$  
c$$$  10   call IDRS(Xsol,Xi,XR,B,lda,ndim,nlar,nou,ns,WRK,NLOOP,MAXIT,TOL
c$$$  $     ,RESIDU,NORM,OMEGA,STATUS,STEPERR)
c$$$  
c$$$  write(*,*) 'donnees',nloop,nou
c$$$  
c$$$  write(*,*) 'TOL',tol,STATUS
c$$$  if (STATUS.lt.0) then
c$$$  write(*,*) 'stop nstat',STATUS,STEPERR
c$$$  stop
c$$$  endif
c$$$  
c$$$  do i=1,ndim
c$$$  xr(i)=0.d0
c$$$  do j=1,ndim
c$$$  xr(i)=xr(i)+mat(i,j)*xi(j)
c$$$  enddo
c$$$  c         write(*,*) 'xr xi',xr(i),xi(i)
c$$$  enddo
c$$$  
c$$$  if (STATUS.ne.1) goto 10
c$$$  
c$$$  
c$$$  write(*,*) '********************',NLOOP
c$$$  DO I=1,NDIM
c$$$  WRITE(*,*) 'SOL',XSOL(I)
c$$$  xi(i)=xsol(i)
c$$$  ENDDO
c$$$  stop
c$$$  c     calcul erreur relative
c$$$  tole=0.d0
c$$$  do i=1,ndim
c$$$  tole=tole+cdabs(xr(i)-b(i))
c$$$  enddo
c$$$  tole=tole/norm
c$$$  write(*,*) 'tole',tole
c$$$  
c$$$  if (tole.ge.tol) goto 10
c$$$  
c$$$  
c$$$  end
c*************************************************
      SUBROUTINE IDRS(X,Xi,XR,B,lda,ndim,nlar,nou,ns,WRK,ITNO,MAXIT ,TOL
     $     ,RESIDU,NORM,OMEGA,is,STATUS ,STEPERR)
      IMPLICIT NONE

      INTEGER ii,nou,ndim,IM,ITNO,LDA,MAXIT,STATUS,STEPERR,ILOOP,nlar,ns
      DOUBLE COMPLEX B(lda),WRK(lda,nlar),X(lda),xi(lda),xr(lda)

*     .. Local Scalars ..
      integer ic,is,nc,nseed,ldans
      DOUBLE COMPLEX OMEGA,ctmp,ctmp2,ctmp3
      DOUBLE PRECISION TOL,NORM,RESIDU,sunif,ran,rho
      parameter (ldans=8)
      double complex matc(ldans,ldans),bcc(ldans),deterz(2) ,work(ldans)
     $     ,icomp
      integer ipvt(ldans),job,info
      
      IF (NOU.EQ.1) GOTO 10
      IF (NOU.EQ.2) GOTO 20
      IF (NOU.EQ.3) GOTO 30
      IF (NOU.EQ.4) GOTO 40

      STATUS = 0
      STEPERR = -1

c     1:r
c     2:v
c     3:t
c     4:q
c     5:e
c     6:P 5+1 5+ns
c     7:Q 5+ns+1 5+2*ns
c     8:E 5+2*ns+1 5+3*ns

      if (5+3*ns.gt.nlar) then
         write(*,*) 'nlar too small'
         STEPERR=1
         STATUS=-1
         return
      endif

      
c     calcul norme et Ax0
      NORM=0.d0
!     $OMP PARALLEL DEFAULT(SHARED) PRIVATE(ii)
!     $OMP DO SCHEDULE(STATIC)  REDUCTION(+:NORM)
      do ii=1,ndim
         x(ii)=xi(ii)
         NORM=NORM+cdabs(B(ii))**2.d0
      enddo
!     $OMP ENDDO
!     $OMP END PARALLEL      
      NORM=dsqrt(NORM)
      write(*,*) 'NORM',NORM
      nou=1
      return

c     initialise r0=b-Ax0
 10   ctmp=0.d0
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:ctmp)
      do ii=1,ndim
         WRK(ii,1)=B(ii)-xr(ii)
         WRK(ii,2)=0.d0
         WRK(ii,3)=0.d0
         WRK(ii,4)=0.d0
         WRK(ii,5)=0.d0
         ctmp=ctmp+WRK(ii,1)*dconjg(WRK(ii,1))
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
c     calcul residu initial

      write(*,*) 'residu initial',cdabs(cdsqrt(ctmp))/norm
      
c***************************************************************
c     create the vector P.
c     initialise ran
      nseed=0
      ran=SUNIF(4*nseed+1)
      icomp=(0.d0,1.d0)

      do is=1,ns
         do ii=1,ndim       
            WRK(ii,5+ns+is)=SUNIF(-1)+icomp*SUNIF(-1)
            WRK(ii,5+is)=WRK(ii,5+ns+is)
         enddo
      enddo

c     orthogonalise les vecteurs P
      nc=1
      do is=1,ns
         do ic=1,nc-1
            ctmp=0.d0
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:ctmp)                 
            do ii=1,ndim
               ctmp=ctmp+WRK(ii,5+ns+is)*dconjg(WRK(ii,5+ic))
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL      
            
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)            
            do ii=1,ndim
               WRK(ii,5+is)=WRK(ii,5+is)-ctmp*WRK(ii,5+ic)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL                  
         enddo
         nc=nc+1            
c     normalise les vecteurs
         ctmp=0.d0
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:ctmp)                 
         do ii=1,ndim
            ctmp=ctmp+dconjg(WRK(ii,5+is))*WRK(ii,5+is)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL      
         ctmp=cdsqrt(ctmp)
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)
         do ii=1,ndim
            WRK(ii,5+is)=dconjg(WRK(ii,5+is)/ctmp)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL         

      enddo

c***********************************************************
c     initialise  E et Q
      is=-1
 200  is=is+1
c     calcul de v=Ar
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)      
      do ii=1,ndim
         xi(ii)=WRK(ii,1)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL                  
      nou=2
      return

 20   ctmp=0.d0
c     calcul de omega=(v,r)/(v,v)
      ctmp2=0.d0
      ctmp3=0.d0
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:ctmp,ctmp2,ctmp3)      
      do ii=1,ndim
         WRK(ii,2)=xr(ii)
         ctmp=ctmp+dconjg(WRK(ii,2))*WRK(ii,1)
         ctmp2=ctmp2+dconjg(WRK(ii,2))*WRK(ii,2)
c         ctmp3=ctmp3+WRK(ii,1)*dconjg(WRK(ii,1))
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      if (cdabs(ctmp2).le.0.d0) then
         STEPERR=2
         STATUS=-1
         return 
      endif
      omega=ctmp/ctmp2
c$$$      rho=cdabs(ctmp)/dsqrt(cdabs(ctmp2*ctmp3))
c$$$      if (rho.le.0.7d0) then
c$$$         omega=0.7d0*omega/rho
c$$$         write(*,*)'omega',omega
c$$$      endif
c     calcul de q=omega*r,e=-omega*v, r=r+e,x=x+q
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)
      do ii=1,ndim
         WRK(ii,4)=omega*WRK(ii,1)
         WRK(ii,5)=-omega*WRK(ii,2)
         WRK(ii,1)=WRK(ii,1)+WRK(ii,5)
         x(ii)=x(ii)+WRK(ii,4)
c     stock Q et E is va de 0 a ns-1
         WRK(ii,5+2*ns-is)=WRK(ii,4)
         WRK(ii,5+3*ns-is)=WRK(ii,5)
c     write(*,*) 'Q',WRK(ii,4),ii,5+2*ns-is,is
c     write(*,*) 'E',WRK(ii,5),ii,5+3*ns-is,is
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL

      if (is.lt.ns-1) goto 200
c***********************************************************
      
c     commence la boucle
      ITNO=ns-1
 100  ITNO=ITNO+1
c     write(*,*) 'ITNO',ITNO

c     solve c from P^t E c=P^t r
c     calcul de la matrice et du second membre: P^t E et P^t r
      matc=0.d0
      bcc=0.d0
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii,is,ic)
!$OMP DO  SCHEDULE(STATIC) COLLAPSE(3) REDUCTION(+:matc)                  
      do ii=1,ndim
         do ic=1,ns
            do is=1,ns
               matc(is,ic)=matc(is,ic)+WRK(ii,5+is)*WRK(ii,5+2*ns+ic)
            enddo       
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL          
       
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii,is)
!$OMP DO  SCHEDULE(STATIC) COLLAPSE(2) REDUCTION(+:bcc)    
      do ii=1,ndim
         do is=1,ns
            bcc(is)=bcc(is)+WRK(ii,5+is)*WRK(ii,1)
         enddo
      enddo         
!$OMP ENDDO 
!$OMP END PARALLEL         


      call ZGETRF( ns, ns , matc, ldans, IPVt, INFO )
      if (INFO.ne.0) then
         write(*,*) 'problem in zgetrf precondionner',info
         stop
      endif
      ii=1
      call  zgetrs( 'N',ns, ii , matc, ldans, IPVt,bcc,ldans,INFO)

c     v=r-E*c
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)      
      do ii=1,ndim
         WRK(ii,2)=WRK(ii,1)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL 

      do is=1,ns
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) 
         do ii=1,ndim
            WRK(ii,2)=WRK(ii,2)-WRK(ii,5+2*ns+is)*bcc(is)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      enddo
      
      if (mod(ITNO,ns+1).eq.ns) then
c     stock bc
         do is=1,ns
            WRK(is,3)=bcc(is)
         enddo
         
c     calcul t=Av
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)
         do ii=1,ndim
            xi(ii)=WRK(ii,2)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL         
         nou=3
         return

 30      ctmp=0.d0
c     reli bc
         do is=1,ns
            bcc(is)=WRK(is,3)
         enddo

c     omega=(t,v)/(t,t)
         ctmp=0.d0
         ctmp2=0.d0
         ctmp3=0.d0
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:ctmp,ctmp2,ctmp3)         
         do ii=1,ndim
            WRK(ii,3)=xr(ii)
            ctmp=ctmp+dconjg(WRK(ii,3))*WRK(ii,2)
            ctmp2=ctmp2+dconjg(WRK(ii,3))*WRK(ii,3)
c            ctmp3=ctmp3+WRK(ii,1)*dconjg(WRK(ii,1))
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL         

         if (cdabs(ctmp2).le.0.d0) then
            STEPERR=3
            STATUS=-1
            return 
         endif
         omega=ctmp/ctmp2
c$$$         rho=cdabs(ctmp)/dsqrt(cdabs(ctmp2*ctmp3))
c$$$         if (rho.le.0.7d0) then
c$$$            omega=0.7d0*omega/rho
c$$$            write(*,*)'omega',omega
c$$$         endif
c     calcul de e=-omega*t-E c et q=omega*v-Q c
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)        
         do ii=1,ndim
            WRK(ii,5)=-omega*WRK(ii,3)
            WRK(ii,4)=omega*WRK(ii,2)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL

         do is=1,ns
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)
            do ii=1,ndim
               WRK(ii,5)=WRK(ii,5)-WRK(ii,5+2*ns+is)*bcc(is)
               WRK(ii,4)=WRK(ii,4)-WRK(ii,5+ns+is)*bcc(is)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL         
         enddo

      else

c     calcul q=-Qc+omega v et e=-Aq
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)
         do ii=1,ndim
            WRK(ii,4)=omega*WRK(ii,2)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         do is=1,ns
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)
            do ii=1,ndim
               WRK(ii,4)=WRK(ii,4)-WRK(ii,5+ns+is)*bcc(is)
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         enddo
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)
         do ii=1,ndim
            xi(ii)=WRK(ii,4)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         nou=4
         return

 40      ctmp=0.d0
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)
         do ii=1,ndim
            WRK(ii,5)=-xr(ii)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL         
      endif

c     calcul du residu r=r+e et x=x+q
      ctmp=0.d0
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC) REDUCTION(+:ctmp)
      do ii=1,ndim
         WRK(ii,1)=WRK(ii,1)+WRK(ii,5)
         x(ii)=x(ii)+WRK(ii,4)
         ctmp=ctmp+dconjg(WRK(ii,1))*WRK(ii,1)
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      if (mod(ITNO,ns+1).ne.ns) then
         RESIDU=dsqrt(dreal(ctmp))/NORM
         write(*,*) 'RESIDU',RESIDU,ITNO
c     verifie la convergence
         if (RESIDU.le.TOL) then
            STATUS=1       
            nou=5
            return
         endif
         if (RESIDU.ge.100.d0) then
            STATUS=-1
            STEPERR=0
            return
         endif
      endif


c     shift les tableaux Q et E
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(ii)
!$OMP DO  SCHEDULE(STATIC)
      do ii=1,ndim
         do is=ns,2,-1
            WRK(ii,5+ns+is)=WRK(ii,5+ns+is-1)
            WRK(ii,5+2*ns+is)=WRK(ii,5+2*ns+is-1)
         enddo
         WRK(ii,5+ns+1)=WRK(ii,4)
         WRK(ii,5+2*ns+1)=WRK(ii,5)	
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL     
      if (ITNO.le.MAXIT) goto 100
      STATUS=-1
      STEPERR=0
      return
      
      END
