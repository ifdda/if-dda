c     routine for compute the image of a confocal microscope.
c     Only the case of the reflexion is investigated.
      subroutine microsconfocal(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $     ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $     ,ntotalm,ntotal,ldabi,nlar,nmax,nxm,nym,nzm,nx,ny,nz ,nx2,ny2
     $     ,nxy2,nz2,ndipole,nbsphere,nbsphere3,nproche,nrig,nfft2d
     $     ,tabfft2 ,tabdip,XI ,XR,wrk ,FF,FF0 ,FFloc ,polarisa ,epsilon
     $     ,methodeit,tolinit,tol1 ,nloop ,ncompte,nlim,xs ,ys,zs
     $     ,aretecube ,numaper,numaperinc,numaperinc2,psiinc ,nquicklens
     $     ,eps0,k0 ,P0 ,irra,w0 ,gross,stepz,zinf,zsup,Eimagex ,Eimagey
     $     ,Eimagez ,Efourierx ,Efouriery ,Efourierz,Eimageincx
     $     ,Eimageincy ,Eimageincz ,Efourierincx ,Efourierincy
     $     ,Efourierincz ,Ediffkzpos ,Ediffkzneg,kxy,xy ,nside,masque
     $     ,nmasque,ideltam ,imaxk0,nbitermic,deltak,deltakx, ntypemic
     $     ,planf ,planb ,plan2f ,plan2b,nmat ,file_id ,group_idmic
     $     ,nstop ,infostr)

#ifdef USE_HDF5
      use HDF5
#endif

      implicit none
c     variables en argument      
      integer ntotalm,ntotal,ldabi,nlar,nmax,nxm,nym,nzm,nx,ny,nz ,nx2
     $     ,ny2,nxy2,nz2,nbsphere,nbsphere3,nloop,ncompte,nlim,nstop
     $     ,nfft2d ,ndipole,nproche,nrig,nmat,ipol,npol
     $     ,nquicklens ,nside,niter,niterii,ntypemic
      integer tabfft2(nfft2d)
      double precision tol,tolinit,tol1,aretecube,eps0,k0,k02,P0,irra,w0
     $     ,I0,numaper,numaperinc,numaperinc2,gross,deltax,zlens,x,y,z
     $     ,Emod,kp,psiinc
      DOUBLE PRECISION,DIMENSION(nxm*nym*nzm)::xs,ys,zs
      double complex, dimension(8*nxm*nym*nzm) :: FFTTENSORxx,
     $     FFTTENSORxy,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz, FFTTENSORzz
     $     ,vectx,vecty,vectz
      double complex, dimension(3*nxm*nym*nzm) :: xr,xi
      double complex, dimension(3*nxm*nym*nzm,nlar) :: wrk
      double complex, dimension(3*nxm*nym*nzm) :: FF,FF0,FFloc
      double complex, dimension(nxm*nym*nzm,3,3) :: polarisa,epsilon
      double complex E0,Eloc(3),Em(3),epsani(3,3),Uincx,Uincy,Uincz
      double complex Eimagex(nfft2d*nfft2d),Eimagey(nfft2d*nfft2d)
     $     ,Eimagez(nfft2d*nfft2d),Efourierx(nfft2d*nfft2d)
     $     ,Efouriery(nfft2d*nfft2d) ,Efourierz(nfft2d*nfft2d)
     $     ,Eimageincx(nfft2d*nfft2d),Eimageincy(nfft2d*nfft2d)
     $     ,Eimageincz(nfft2d*nfft2d),Efourierincx(nfft2d*nfft2d)
     $     ,Efourierincy(nfft2d*nfft2d),Efourierincz(nfft2d*nfft2d)
      double complex Ediffkzpos(nfft2d,nfft2d,3),Ediffkzneg(nfft2d
     $     ,nfft2d,3)
      double precision kxy(nfft2d),xy(nfft2d)
      integer, dimension(nxm*nym*nzm) :: Tabdip
      character(64) infostr,beam
      character(12) methodeit
      
      integer*8 planf,planb,plan2f,plan2b,planfn,planbn
      integer FFTW_FORWARD,FFTW_ESTIMATE,FFTW_BACKWARD

c     variable pour le bright field
      integer i,j,ii,jj,k,kk,idelta,jdelta,nsens,imaxk0,indice ,indicex
     $     ,indicey,nfft2d2,ikxinc,jkyinc,imul
      double precision deltak,numaperk,phi,theta,zero,pi,kx,ky,kz,ss,pp
     $     ,u1,u2,tmp,normal(3),xmin,xmax,ymin,ymax,deltakx,deltaky
     $     ,deltakxy,kxinc,kyinc,u(3),v(3),sintmp,costmp,deltakm
      double complex tmpx,tmpy,tmpz,Ex,Ey,Ez,Emx,Emy,Emz,ctmp,ctmp1
     $     ,icomp,zfocus,phase
      integer ideltam,nbitermic,nmasque
      double complex masque(nmasque*nmasque)

c     variable pour le confocal
      integer ix,iy,iz,nplanz,nplanze
      double precision const1,kzinc,Axp,Azp,Ays,Azs,x0,y0,z0,kp2,psi
     $     ,stepz,zinf,zsup
      double complex exparg,Ax,Ay,Az

      double complex , dimension (:,:,:), allocatable :: imageconfx
     $     ,imageconfy,imageconfz

      character(LEN=100) :: datasetname,h5filemicros
      character(LEN=40) :: name
#ifndef USE_HDF5
      integer,parameter:: hid_t=4
#endif

      integer(hid_t) :: file_id,file_idmicros
      integer(hid_t) :: group_idmic,group_id_conf,group_id_conf_fourier
     $     ,group_id_conf_image
      integer :: dim(4)
      integer error,nmicsavefield
      character( 4 )  fil4
     
      write(*,*) '***** Confocal microscope ******'
      write(*,*) 'Numerical aperture : ',numaperinc,nbitermic,ideltam


      
      
c     initialise
      zero=0.d0
      pi=dacos(-1.d0)
      beam='pwavelinear'
      nfft2d2=nfft2d/2
      icomp=(0.d0,1.d0)
      numaperk=k0*numaperinc
      k02=k0*k0
c     compte the  number of plan for the z scan
      zinf=zinf*1.d-9
      zsup=zsup*1.d-9
c     stepz sur variable zlens deja mis en metre et peut etre change de
c     signe d'ou le dabs
      stepz=dabs(stepz)
      if (zinf.gt.zsup) then
         infostr='lower bound larger than the upper bound in z-stack'
         nstop=1
         return
      endif

      if (stepz.le.1.d-9.or.zsup.eq.zinf) then
         zsup=zinf
         stepz=0.d0
         nplanz=1
         nplanze=1
      else
         nplanz=nint((zsup-zinf)/stepz)+1
         nplanze=nplanz/2
      endif
      write(*,*) 'Number of plane in the stack',nplanz
      if (nplanz.gt.nfft2d) then
         infostr='too many plane in the z-stack'
         nstop=1
         return
      endif
      
      write(*,*) 'Size of the FFT',nfft2d
      if (nfft2d.gt.16384) then
         nstop=1
         infostr='nfft2d too large'
         return
      endif
      if (deltak.ge.numaperk) then
         nstop=1
         infostr='In FFT lens nfft2d too small'
         return
      endif

      if (nside.eq.1) then
         write(*,*) 'Transmission'
      else if(nside.eq.-1) then
         write(*,*) 'Reflexion'
      else
         write(*,*) 'pb with side',nside
         infostr='problem with side'
         nstop=-1
         return
      endif

      
      nmicsavefield=3
      if (nmicsavefield.ge.1) then
#ifdef USE_HDF5         
         write(*,*) 'write data for the confocal'
         h5filemicros='microscopy.h5'
         call hdf5create(h5filemicros, file_idmicros)
         call h5gcreate_f(file_idmicros,"Confocal", group_id_conf,
     $        error)
c     write k0,numaper,nb illu,psi,Delta k Delta x,imaxk0,nfft2d
         dim(1)=1
         dim(2)=1
         datasetname='k0'
         call hdf5write(group_id_conf,datasetname,k0,dim)
         datasetname='numaper'
         call hdf5write(group_id_conf,datasetname,numaperinc,dim)
         datasetname='deltax'
         call hdf5write(group_id_conf,datasetname,aretecube,dim)
         datasetname='deltak'
         deltakx=2.d0*pi/dble(nfft2d)/aretecube
         call hdf5write(group_id_conf,datasetname,deltakx,dim)
         datasetname='polarization'
         call hdf5write(group_id_conf,datasetname,psiinc,dim)         
         datasetname='nfft2d'
         call hdf5write1d_int(group_id_conf,datasetname,nfft2d,dim)
         datasetname='nfourier'
         imaxk0=nint(k0/deltakx)+1
         k=2*imaxk0+1
         call hdf5write1d_int(group_id_conf,datasetname,k,dim)         
         datasetname='nbillu'
         call hdf5write1d_int(group_id_conf,datasetname,nbitermic,dim)
         call h5gcreate_f(group_id_conf,"Fourier", group_id_conf_fourier
     $        ,error)
         call h5gcreate_f(group_id_conf,"Image", group_id_conf_image
     $        ,error)
#endif         
      endif


c     allocate des tableaux de sauvegarde
      allocate(imageconfx(nfft2d,nfft2d,nplanz))
      allocate(imageconfy(nfft2d,nfft2d,nplanz))
      allocate(imageconfz(nfft2d,nfft2d,nplanz))

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC) COLLAPSE(3)    
      do i=1,nfft2d
         do j=1,nfft2d
            do k=1,nplanz
               imageconfx(i,j,k)=0.d0
               imageconfy(i,j,k)=0.d0
               imageconfz(i,j,k)=0.d0
            enddo
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      
c     initalise
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
      do i=1,nfft2d*nfft2d
         Eimageincx(i)=0.d0
         Eimageincy(i)=0.d0
         Eimageincz(i)=0.d0
         Eimagex(i)=0.d0
         Eimagey(i)=0.d0
         Eimagez(i)=0.d0                          
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL 
      
c     calcul puissance
      P0=P0/dble(nbitermic)
c     P0=1.d0
c     npol=1
      call irradiance(P0,w0,E0,irra)
      I0=cdabs(E0)**2
      psi=psiinc*pi/180.d0
      niterii=0

       write(*,*) 'Magnitude of each illumination:',E0
c     sommation 
      do idelta=-ideltam,ideltam
         do jdelta=-ideltam,ideltam
            kxinc=dble(idelta)*deltak
            kyinc=dble(jdelta)*deltak
            i=ideltam+idelta+1
            j=ideltam+jdelta+1
c            if (idelta.eq.0.and.jdelta.eq.0) then
            if (cdabs(masque(i+(j-1)*(2*ideltam+1))).le.2.d0) then
               niterii=niterii+1
               write(*,*) '*** incidence',niterii,'/',nbitermic,' *****'
c     calcul champ incident
               kp2=kxinc*kxinc+kyinc*kyinc
               kzinc=dsqrt(k02-kp2)
c     polarisation p
               const1=1.d0/dsqrt(kzinc*kzinc+kxinc*kxinc)
               Axp=kzinc*const1              
               Azp=-kxinc*const1
c     polarisation s
               const1=1.d0/dsqrt(kzinc*kzinc+kyinc*kyinc)                 
               Ays=kzinc*const1
               Azs=-kyinc*const1 
c     composition des deux polarisations
               Ax=E0*dcos(psi)*Axp
               Ay=E0*dsin(psi)*Ays
               Az=E0*(dcos(psi)*Azp+dsin(psi)*Azs)
                  
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,exparg)
!$OMP DO SCHEDULE(STATIC)
               do i=1,nbsphere
c     changement de base pour retourner dans la base de la surface
                  exparg=cdexp(icomp*(kxinc*xs(i)+kyinc*ys(i)+kzinc
     $                 *zs(i)))                  
                  FF0(3*i-2)=Ax*exparg
                  FF0(3*i-1)=Ay*exparg
                  FF0(3*i)=Az*exparg
                  xi(3*i-2)=FF0(3*i-2)
                  xi(3*i-1)=FF0(3*i-1)
                  xi(3*i)=FF0(3*i)
               enddo
!$OMP ENDDO 
!$OMP END PARALLEL
               write(*,*) 'Begin computation of the field',nproche
c     calcul champ local
               if (nrig.eq.0) then
                  tol=tolinit
                  ncompte=0
                  nloop=0
                  if (nproche.eq.-1) then
                     call inverserig(FFTTENSORxx,FFTTENSORxy
     $                    ,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz
     $                    ,FFTTENSORzz,vectx,vecty,vectz ,Tabdip
     $                    ,ntotalm,ntotal,ldabi,nlar,nmax,ndipole,nxm
     $                    ,nym ,nzm,nx,ny,nz,nx2,ny2,nxy2,nz2
     $                    ,nbsphere,nbsphere3,XI ,XR,wrk,FF,FF0,FFloc
     $                    ,polarisa,methodeit,tol,tol1,nloop,ncompte
     $                    ,nlim ,planf,planb,nstop,infostr)
                     if (nstop.eq.1) return
                  else
                     call inverserigopt(FFTTENSORxx,FFTTENSORxy
     $                    ,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz
     $                    ,FFTTENSORzz,vectx,vecty,vectz,ntotalm
     $                    ,ntotal ,ldabi,nlar,nmax,nxm,nym,nzm,nx,ny
     $                    ,nz,nx2,ny2 ,nxy2,nz2,nbsphere,nbsphere3,XI
     $                    ,XR,wrk,FF,FF0 ,FFloc,polarisa,methodeit
     $                    ,tol,tol1,nloop ,ncompte,nlim,planf,planb
     $                    ,nstop ,infostr)
                     if (nstop.eq.1) return
                  endif
               elseif (nrig.eq.1) then 
c     **************************************        
c     Renormalized Born approximation field
c     **************************************
                  write(*,*) 'Renormalized Born approximation'
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
                  do i=1,nbsphere3
                     FFloc(i)=FF0(i)
                  enddo
!$OMP ENDDO 
!$OMP END PARALLEL          

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,ii,jj)    
!$OMP DO SCHEDULE(STATIC)
                  do i=1,nbsphere
                     k=3*(i-1)
                     do ii=1,3
                        FF(k+ii)=0.d0
                        do jj=1,3
                           FF(k+ii)=FF(k+ii)+polarisa(i,ii,jj)
     $                          *FFloc(k+jj)
                        enddo
                     enddo
                  enddo
!$OMP ENDDO 
!$OMP END PARALLEL
               elseif (nrig.eq.2) then 
c     **************************************        
c     Born approximation field
c     **************************************
                  write(*,*) 'Born approximation'
                  nsens=-1
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(k,kk,Em,Eloc,ii,jj,epsani)   
!$OMP DO SCHEDULE(STATIC) 
                  do k=1,nbsphere
                     kk=3*(k-1)
                     Em(1)=FF0(kk+1)
                     Em(2)=FF0(kk+2)
                     Em(3)=FF0(kk+3)
                     do ii=1,3
                        do jj=1,3
                           epsani(ii,jj)=epsilon(k,ii,jj)
                        enddo
                     enddo 
                     call local_macro(Eloc,Em,epsani,aretecube,k0
     $                    ,nsens)
                     FFloc(kk+1)=Eloc(1)
                     FFloc(kk+2)=Eloc(2)
                     FFloc(kk+3)=Eloc(3)
                  enddo
!$OMP ENDDO 
!$OMP END PARALLEL          

c     dipole
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,ii,jj)    
!$OMP DO SCHEDULE(STATIC)
                  do i=1,nbsphere
                     k=3*(i-1)
                     do ii=1,3
                        FF(k+ii)=0.d0
                        do jj=1,3
                           FF(k+ii)=FF(k+ii)+polarisa(i,ii,jj)
     $                          *FFloc(k+jj)
                        enddo
                     enddo
                  enddo
!$OMP ENDDO 
!$OMP END PARALLEL
               elseif (nrig.eq.3) then
c     renormalized Born ordre 1
                  write(*,*) 'Born series order 1'
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC) 
                  do i=1,nbsphere3
                     xr(i)=FF0(i)           
                  enddo
!$OMP ENDDO 
!$OMP END PARALLEL
                  
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)  
!$OMP DO SCHEDULE(STATIC)
                  do i=1,nbsphere
                     k=3*(i-1)
                     xi(k+1)=-polarisa(i,1,1)*xr(k+1)-polarisa(i,1,2)
     $                    *xr(k+2)-polarisa(i,1,3)*xr(k+3)
                     xi(k+2)=-polarisa(i,2,1)*xr(k+1)-polarisa(i,2,2)
     $                    *xr(k+2)-polarisa(i,2,3)*xr(k+3)
                     xi(k+3)=-polarisa(i,3,1)*xr(k+1)-polarisa(i,3,2)
     $                    *xr(k+2)-polarisa(i,3,3)*xr(k+3)
                  enddo        
!$OMP ENDDO 
!$OMP END PARALLEL
                  
                  call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy
     $                 ,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz
     $                 ,FFTTENSORzz ,vectx,vecty,vectz,Tabdip,ntotalm
     $                 ,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny,nz
     $                 ,nx2,ny2,nxy2,nz2 ,XI,XR,planf,planb)

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k) 
!$OMP DO SCHEDULE(STATIC)       
                  do i=1,nbsphere
                     k=3*(i-1)
                     FFloc(k+1)=xr(k+1)
                     FFloc(k+2)=xr(k+2)
                     FFloc(k+3)=xr(k+3)
                     FF(k+1)=polarisa(i,1,1)*xr(k+1)+polarisa(i,1,2)
     $                    *xr(k+2)+polarisa(i,1,3)*xr(k+3)
                     FF(k+2)=polarisa(i,2,1)*xr(k+1)+polarisa(i,2,2)
     $                    *xr(k+2)+polarisa(i,2,3)*xr(k+3)
                     FF(k+3)=polarisa(i,3,1)*xr(k+1)+polarisa(i,3,2)
     $                    *xr(k+2)+polarisa(i,3,3)*xr(k+3)
                  enddo            
!$OMP ENDDO 
!$OMP END PARALLEL      

                  
               elseif (nrig.eq.4) then
c     Rytov renormalize
                  write(*,*) 'Rytov approximation on local field'
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
                  do i=1,nbsphere3
                     xr(i)=0.d0       
                  enddo
!$OMP ENDDO 
!$OMP END PARALLEL
                  
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC)
                  do i=1,nbsphere
                     k=3*(i-1)
                     xi(k+1)=-polarisa(i,1,1)*FF0(k+1)-polarisa(i,1
     $                    ,2)*FF0(k+2)-polarisa(i,1,3)*FF0(k+3)
                     xi(k+2)=-polarisa(i,2,1)*FF0(k+1)-polarisa(i,2
     $                    ,2)*FF0(k+2)-polarisa(i,2,3)*FF0(k+3)
                     xi(k+3)=-polarisa(i,3,1)*FF0(k+1)-polarisa(i,3
     $                    ,2)*FF0(k+2)-polarisa(i,3,3)*FF0(k+3)
                  enddo        
!$OMP ENDDO 
!$OMP END PARALLEL
                  
                  call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy
     $                 ,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz
     $                 ,FFTTENSORzz ,vectx,vecty,vectz,Tabdip,ntotalm
     $                 ,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny,nz
     $                 ,nx2,ny2,nxy2,nz2 ,XI,XR,planf,planb)

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,tmp)
!$OMP DO SCHEDULE(STATIC)      
                  do i=1,nbsphere
                     k=3*(i-1)
                     tmp=dsqrt(dreal(FF0(k+1)*dconjg(FF0(k+1))+FF0(k
     $                    +2)*dconjg(FF0(k+2))+FF0(k+3)*dconjg(FF0(k
     $                    +3))))*1.d-6
                     if (cdabs(FF0(k+1)).le.tmp) then
                        FFloc(k+1)=0.d0
                     else
                        FFloc(k+1)=FF0(k+1)*cdexp(xr(k+1)/FF0(k+1))
                     endif
                     if (cdabs(FF0(k+2)).le.tmp) then
                        FFloc(k+2)=0.d0
                     else
                        FFloc(k+2)=FF0(k+2)*cdexp(xr(k+2)/FF0(k+2))
                     endif
                     if (cdabs(FF0(k+3)).le.tmp) then
                        FFloc(k+3)=0.d0
                     else
                        FFloc(k+3)=FF0(k+3)*cdexp(xr(k+3)/FF0(k+3))
                     endif
                  enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
                  
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC)       
                  do i=1,nbsphere
                     k=3*(i-1)  
                     FF(k+1)=polarisa(i,1,1)*FFloc(k+1)+polarisa(i,1
     $                    ,2)*FFloc(k+2)+polarisa(i,1,3)*FFloc(k+3)
                     FF(k+2)=polarisa(i,2,1)*FFloc(k+1)+polarisa(i,2
     $                    ,2)*FFloc(k+2)+polarisa(i,2,3)*FFloc(k+3)
                     FF(k+3)=polarisa(i,3,1)*FFloc(k+1)+polarisa(i,3
     $                    ,2)*FFloc(k+2)+polarisa(i,3,3)*FFloc(k+3)
                  enddo            
!$OMP ENDDO 
!$OMP END PARALLEL      

               elseif (nrig.eq.5) then
c     Rytov
                  write(*,*)
     $                 'Rytov approximation on macroscopic field'
c     a3=aretecube*aretecube*aretecube
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)   
!$OMP DO SCHEDULE(STATIC) 
                  do i=1,nbsphere
                     k=3*(i-1)
                     xr(k+1)=-FF0(k+1)*(epsilon(i,1,1)-eps0)/3.d0
                     xr(k+2)=-FF0(k+2)*(epsilon(i,2,2)-eps0)/3.d0
                     xr(k+3)=-FF0(k+3)*(epsilon(i,3,3)-eps0)/3.d0
                  enddo
!$OMP ENDDO 
!$OMP END PARALLEL
                  nsens=-1
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,Em,Eloc,ii,jj,epsani)
!$OMP DO SCHEDULE(STATIC)
                  do i=1,nbsphere
                     k=3*(i-1)
                     Em(1)=FF0(k+1)
                     Em(2)=FF0(k+2)
                     Em(3)=FF0(k+3)
                     do ii=1,3
                        do jj=1,3
                           epsani(ii,jj)=epsilon(i,ii,jj)
                        enddo
                     enddo 
                     call local_macro(Eloc,Em,epsani,aretecube,k0
     $                    ,nsens)
                     FF(k+1)=Eloc(1)
                     FF(k+2)=Eloc(2)
                     FF(k+3)=Eloc(3)
                     xi(k+1)=-polarisa(i,1,1)*FF(k+1)-polarisa(i,1,2)
     $                    *FF(k+2)-polarisa(i,1,3)*FF(k+3)
                     xi(k+2)=-polarisa(i,2,1)*FF(k+1)-polarisa(i,2,2)
     $                    *FF(k+2)-polarisa(i,2,3)*FF(k+3)
                     xi(k+3)=-polarisa(i,3,1)*FF(k+1)-polarisa(i,3,2)
     $                    *FF(k+2)-polarisa(i,3,3)*FF(k+3)
                  enddo        
!$OMP ENDDO 
!$OMP END PARALLEL
                  
                  call produitfftmatvect3(FFTTENSORxx,FFTTENSORxy
     $                 ,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz
     $                 ,FFTTENSORzz ,vectx,vecty,vectz,Tabdip,ntotalm
     $                 ,ntotal,nmax ,ndipole,nxm ,nym,nzm,nx,ny,nz
     $                 ,nx2,ny2,nxy2,nz2 ,XI,XR,planf,planb)

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,tmp)
!$OMP DO SCHEDULE(STATIC)      
                  do i=1,nbsphere
                     k=3*(i-1)
                     tmp=dsqrt(dreal(FF(k+1)*dconjg(FF(k+1))+FF(k+2)
     $                    *dconjg(FF(k+2))+FF(k+3)*dconjg(FF(k+3))))
     $                    *1.d -6
                     if (cdabs(FF(k+1)).le.tmp) then
                        FFloc(k+1)=0.d0
                     else
                        FFloc(k+1)=FF(k+1)*cdexp(xr(k+1)/FF(k+1))
                     endif
                     if (cdabs(FF(k+2)).le.tmp) then
                        FFloc(k+2)=0.d0
                     else
                        FFloc(k+2)=FF(k+2)*cdexp(xr(k+2)/FF(k+2))
                     endif
                     if (cdabs(FF(k+3)).le.tmp) then
                        FFloc(k+3)=0.d0
                     else
                        FFloc(k+3)=FF(k+3)*cdexp(xr(k+3)/FF(k+3))
                     endif
                  enddo            
!$OMP ENDDO 
!$OMP END PARALLEL
                  
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)
!$OMP DO SCHEDULE(STATIC)      
                  do i=1,nbsphere
                     k=3*(i-1)  
                     FF(k+1)=polarisa(i,1,1)*FFloc(k+1)+polarisa(i,1
     $                    ,2)*FFloc(k+2)+polarisa(i,1,3)*FFloc(k+3)
                     FF(k+2)=polarisa(i,2,1)*FFloc(k+1)+polarisa(i,2
     $                    ,2)*FFloc(k+2)+polarisa(i,2,3)*FFloc(k+3)
                     FF(k+3)=polarisa(i,3,1)*FFloc(k+1)+polarisa(i,3
     $                    ,2)*FFloc(k+2)+polarisa(i,3,3)*FFloc(k+3)
                  enddo            
!$OMP ENDDO 
!$OMP END PARALLEL      
                  
               elseif (nrig.eq.6) then
c     Beam propagation method
                  write(*,*) 'Beam propagation method'
                  kp=dsqrt(kxinc*kxinc+kyinc*kyinc)
                  beam='pwavelinear'
                  FF0=0.d0
                  theta=dasin(kp/k0)*180.d0/pi
                  phi=datan2(kyinc,kxinc)*180.d0/pi

                  call beampropagationmacro(xs,ys,zs,aretecube,k0,w0
     $                 ,E0,ss,pp,theta,phi,zero,zero,zero,beam
     $                 ,epsilon,ndipole ,nx ,ny,nz,nxm,nym ,nzm,nmax
     $                 ,nfft2d,Efourierx,Efouriery,Efourierz,FF0
     $                 ,FFloc,FF ,plan2f,plan2b ,nstop,infostr)

                  if (nstop.eq.1) return

                  
!     $OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)
!     $OMP DO SCHEDULE(STATIC)       
                  do i=1,nbsphere
                     k=3*(i-1)  
                     FF(k+1)=polarisa(i,1,1)*FFloc(k+1)+polarisa(i,1
     $                    ,2)*FFloc(k+2)+polarisa(i,1,3)*FFloc(k+3)
                     FF(k+2)=polarisa(i,2,1)*FFloc(k+1)+polarisa(i,2
     $                    ,2)*FFloc(k+2)+polarisa(i,2,3)*FFloc(k+3)
                     FF(k+3)=polarisa(i,3,1)*FFloc(k+1)+polarisa(i,3
     $                    ,2)*FFloc(k+2)+polarisa(i,3,3)*FFloc(k+3)
                  enddo            
!     $OMP ENDDO 
!     $OMP END PARALLEL    
               elseif (nrig.eq.7) then
c     Beam propagation method
                  kp=dsqrt(kxinc*kxinc+kyinc*kyinc)
                  beam='pwavelinear'
                  FF0=0.d0
                  theta=dasin(kp/k0)*180.d0/pi
                  phi=datan2(kyinc,kxinc)*180.d0/pi

                  call beampropagation(xs,ys,zs,aretecube,k0,w0,E0,ss
     $                 ,pp,theta,phi,zero,zero,zero,beam,epsilon
     $                 ,ndipole,nx ,ny,nz,nxm,nym ,nzm,nmax,nfft2d
     $                 ,Efourierx,Efouriery,Efourierz,FF0 ,FFloc,FF
     $                 ,plan2f ,plan2b,nstop,infostr)

                  if (nstop.eq.1) return

                  
!     $OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)
!     $OMP DO SCHEDULE(STATIC)       
                  do i=1,nbsphere
                     k=3*(i-1)  
                     FF(k+1)=polarisa(i,1,1)*FFloc(k+1)+polarisa(i,1
     $                    ,2)*FFloc(k+2)+polarisa(i,1,3)*FFloc(k+3)
                     FF(k+2)=polarisa(i,2,1)*FFloc(k+1)+polarisa(i,2
     $                    ,2)*FFloc(k+2)+polarisa(i,2,3)*FFloc(k+3)
                     FF(k+3)=polarisa(i,3,1)*FFloc(k+1)+polarisa(i,3
     $                    ,2)*FFloc(k+2)+polarisa(i,3,3)*FFloc(k+3)
                  enddo            
!     $OMP ENDDO 
!     $OMP END PARALLEL    

               elseif  (nrig.eq.8) then                  
 
c     polarisation p
                  const1=1.d0/dsqrt(kzinc*kzinc+kxinc*kxinc)
                  Axp=kzinc*const1              
                  Azp=-kxinc*const1
c     polarisation s
                  const1=1.d0/dsqrt(kzinc*kzinc+kyinc*kyinc)                 
                  Ays=kzinc*const1
                  Azs=-kyinc*const1 
c     composition des deux polarisations
                  Ax=E0*dcos(psi)*Axp
                  Ay=E0*dsin(psi)*Ays
                  Az=E0*(dcos(psi)*Azp+dsin(psi)*Azs)

                  Emod=dsqrt(cdabs(Ax)**2.d0+cdabs(Ay)**2.d0
     $                 +cdabs(Az)**2.d0)
                  Uincx=Ax/Emod
                  Uincy=Ay/Emod
                  Uincz=Az/Emod

                  tol=tolinit
                  ncompte=0
                  nloop=0
                  if (nproche.eq.-1) then
                     call inverserigscalaire(FFTTENSORxx,vectx,Tabdip
     $                    ,FFTTENSORyy(1:nmax),FFTTENSORzz(1:nmax)
     $                    ,Uincx ,Uincy,Uincz ,ntotalm,ntotal,ldabi
     $                    ,nlar ,nmax,ndipole,nxm,nym ,nzm,nx ,ny,nz
     $                    ,nx2 ,ny2,nxy2,nz2,nbsphere,XI ,XR,wrk ,FF
     $                    ,FF0 ,FFloc,polarisa,methodeit,tol,tol1
     $                    ,nloop ,ncompte,nlim ,planf ,planb,nstop
     $                    ,infostr)
                  else
                     call inverserigoptscalaire(FFTTENSORxx,vectx
     $                    ,FFTTENSORyy(1:nmax),FFTTENSORzz(1:nmax)
     $                    ,Uincx ,Uincy,Uincz ,ntotalm,ntotal,ldabi
     $                    ,nlar ,nmax,nxm,nym,nzm,nx,ny,nz ,nx2 ,ny2
     $                    ,nxy2 ,nz2,nbsphere,XI,XR,wrk,FF,FF0 ,FFloc
     $                    ,polarisa ,methodeit,tol,tol1,nloop,ncompte
     $                    ,nlim,planf ,planb ,nstop ,infostr)
                  endif
                  if (nstop.eq.1) return
                  
               elseif  (nrig.eq.9) then
c     polarisation p
                  const1=1.d0/dsqrt(kzinc*kzinc+kxinc*kxinc)
                  Axp=kzinc*const1              
                  Azp=-kxinc*const1
c     polarisation s
                  const1=1.d0/dsqrt(kzinc*kzinc+kyinc*kyinc)                 
                  Ays=kzinc*const1
                  Azs=-kyinc*const1 
c     composition des deux polarisations
                  Ax=E0*dcos(psi)*Axp
                  Ay=E0*dsin(psi)*Ays
                  Az=E0*(dcos(psi)*Azp+dsin(psi)*Azs)

                  Emod=dsqrt(cdabs(Ax)**2.d0+cdabs(Ay)**2.d0
     $                 +cdabs(Az)**2.d0)
                  Uincx=Ax/Emod
                  Uincy=Ay/Emod
                  Uincz=Az/Emod

                  tol=tolinit
                  ncompte=0
                  nloop=0

                  if (nproche.eq.-1) then
                     call inverserigscalaire(FFTTENSORxx,vectx,Tabdip
     $                    ,FFTTENSORyy(1:nmax),FFTTENSORzz(1:nmax)
     $                    ,Uincx ,Uincy,Uincz ,ntotalm,ntotal,ldabi
     $                    ,nlar ,nmax,ndipole,nxm,nym ,nzm,nx ,ny,nz
     $                    ,nx2 ,ny2,nxy2,nz2,nbsphere,XI ,XR,wrk ,FF
     $                    ,FF0 ,FFloc,polarisa,methodeit,tol,tol1
     $                    ,nloop ,ncompte ,nlim ,planf ,planb,nstop
     $                    ,infostr)
                  else
                     call inverserigoptscalaire(FFTTENSORxx,vectx
     $                    ,FFTTENSORyy(1:nmax),FFTTENSORzz(1:nmax)
     $                    ,Uincx ,Uincy,Uincz ,ntotalm,ntotal,ldabi
     $                    ,nlar ,nmax,nxm,nym,nzm,nx,ny,nz ,nx2 ,ny2
     $                    ,nxy2 ,nz2,nbsphere,XI,XR,wrk,FF,FF0 ,FFloc
     $                    ,polarisa ,methodeit,tol,tol1,nloop,ncompte
     $                    ,nlim,planf ,planb ,nstop ,infostr)
                  endif
                  if (nstop.eq.1) return
               endif
c     *********************************************************
c     fin calcul champ local
c     *********************************************************

c     *********************************************************            
c     calcul champ diffracte
c     *********************************************************
               if (nquicklens.eq.1) then
                  call diffractefft2dlens(nx,ny,nz,nxm,nym,nzm,nfft2d
     $                 ,tabfft2,k0,xs,ys,zs,aretecube,Efourierx
     $                 ,Efouriery,Efourierz,FF,imaxk0,deltakx,deltaky
     $                 ,Ediffkzneg,numaperk,nside,plan2f,plan2b ,nstop
     $                 ,infostr)
                  if (nstop.eq.1) return
               else
                  deltaky=deltakx
                  do i=-imaxk0,imaxk0
                     kx=deltakx*dble(i)
                     do j=-imaxk0,imaxk0
                        ky=deltaky*dble(j)
                        
                        if (dsqrt(kx*kx+ky*ky).le.numaperk) then
                           kz=dsqrt(k02-kx*kx-ky*ky)
                           normal(1)=kx/k0
                           normal(2)=ky/k0
                           normal(3)=dsqrt(1.d0-normal(1)*normal(1)
     $                          -normal(2)*normal(2))*dble(nside)
                           Emx=0.d0
                           Emy=0.d0
                           Emz=0.d0
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(ii,kk,ctmp,ctmp1)
!$OMP DO SCHEDULE(STATIC) REDUCTION(+:Emx,Emy,Emz)
                           do ii=1,nbsphere
                              kk=3*(ii-1)
                              ctmp=cdexp(-icomp*k0*(normal(1)*xs(ii)
     $                             +normal(2)*ys(ii)+normal(3)
     $                             *zs(ii)))
                              ctmp1=normal(1)*FF(kk+1)+normal(2)
     $                             *FF(kk+2)+normal(3)*FF(kk+3)
                              Emx=Emx+ctmp*(FF(kk+1)-ctmp1*normal(1))
                              Emy=Emy+ctmp*(FF(kk+2)-ctmp1*normal(2))
                              Emz=Emz+ctmp*(FF(kk+3)-ctmp1*normal(3))
                           enddo
!$OMP ENDDO 
!$OMP END PARALLEL                     
                           ctmp=-2.d0*pi*icomp*kz                   
                           kk=i+nfft2d2+1+nfft2d*(j+nfft2d2)
                           ii=imaxk0+i+1
                           jj=imaxk0+j+1

                           Ediffkzneg(ii,jj,1)=Emx*k02/ctmp
                           Ediffkzneg(ii,jj,2)=Emy*k02/ctmp
                           Ediffkzneg(ii,jj,3)=Emz*k02/ctmp
                        endif
                     enddo
                  enddo
               endif   
c     *********************************************************            
c     fin champ diffracte
c     *********************************************************
c     sauvegarde les champs pour chaque incidence dans le plan de Fourier
               if (nmicsavefield.eq.1.or.nmicsavefield.eq.3) then
                  write(*,*) 'write the fourier data'
                  k=1
                  call integer_string4(niterii,fil4)
                  name='Nb_illumination'//fil4
                  do i=-imaxk0,imaxk0
                     do j=-imaxk0,imaxk0
                        ii=imaxk0+i+1
                        jj=imaxk0+j+1
                        kk=i+nfft2d2+1+nfft2d*(j+nfft2d2)
                        Efourierincx(kk)=Ediffkzneg(ii,jj,1)
                        Efourierincy(kk)=Ediffkzneg(ii,jj,2)
                        Efourierincz(kk)=Ediffkzneg(ii,jj,3)
                     enddo
                  enddo
                  
                  call writehdf5mic(Efourierincx,Efourierincy
     $                 ,Efourierincz,nfft2d,imaxk0,Ediffkzpos,k,name
     $                 ,group_id_conf_fourier)

                  dim(1)=3
                  dim(2)=3
                  datasetname='Nb_illumination'//fil4//'kinc'
                  u(1)=kxinc
                  u(2)=kyinc
                  u(3)=kzinc
                  call hdf5write1d(group_id_conf_fourier,datasetname,u,
     $                 dim)

                  datasetname='Nb_illumination'//fil4//'uinc'
                  tmp=dsqrt(cdabs(Ax)*cdabs(Ax)+cdabs(Ay)*cdabs(Ay)
     $                 +cdabs(Az)*cdabs(Az))
                  u(1)=cdabs(Ax)/tmp
                  u(2)=cdabs(Ay)/tmp
                  u(3)=cdabs(Az)/tmp
                  call hdf5write1d(group_id_conf_fourier,datasetname,u,
     $                 dim)
                  dim(1)=1
                  dim(2)=1
                  datasetname='Nb_illumination'//fil4//'NormeE0'
                  call hdf5write(group_id_conf_fourier,datasetname,tmp
     $                 ,dim)
               endif
c     *********************************************************            
c     calcul image
c     *********************************************************  

               call deltakroutine(kxinc,kyinc,deltakx,deltaky,k0 ,ikxinc
     $              ,jkyinc)
               deltakxy=deltakx*deltaky

c     boucle sur z
               do iz=1,nplanz

                  zlens=zinf+stepz*dble(iz-1)
                  z0=zlens

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
                  do i=1,nfft2d*nfft2d
                     Efourierincx(i)=0.d0
                     Efourierincy(i)=0.d0
                     Efourierincz(i)=0.d0
                     Efourierx(i)=0.d0
                     Efouriery(i)=0.d0
                     Efourierz(i)=0.d0                          
                  enddo
!$OMP ENDDO 
!$OMP END PARALLEL

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,indicex,indicey,indice)
!$OMP& PRIVATE(kx,ky,kz,u1,u2,normal,tmp,tmpx,tmpy,tmpz,ii,jj)
!$OMP& PRIVATE(Ex,Ey,Ez,zfocus,u,v,costmp,sintmp,kp)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
                  do i=-imaxk0,imaxk0 
                     do j=-imaxk0,imaxk0
                        if (i.ge.0) then
                           indicex=i+1
                        else
                           indicex=nfft2d+i+1
                        endif
                        if (j.ge.0) then
                           indicey=j+1
                        else
                           indicey=nfft2d+j+1
                        endif
                        indice=indicex+nfft2d*(indicey-1)

                        kx=deltakx*dble(i)
                        ky=deltaky*dble(j)
                        tmp=kx*kx+ky*ky
                        if (tmp.le.numaperk*numaperk) then
                           kz=dsqrt(k02-tmp)*dble(nside)
                           zfocus=cdexp(icomp*kz*zlens)
                           ii=imaxk0+i+1
                           jj=imaxk0+j+1                        
                           Efourierincx(indice)=Ediffkzneg(ii,jj,1)
     $                          *zfocus
                           Efourierincy(indice)=Ediffkzneg(ii,jj,2)
     $                          *zfocus                        
                           Efourierincz(indice)=Ediffkzneg(ii,jj,3)
     $                          *zfocus
                           Efourierx(indice)=Efourierincx(indice)
                           Efouriery(indice)=Efourierincy(indice)
                           Efourierz(indice)=Efourierincz(indice)

c     pour l'instant ne calcule pas avec le champ incident.
                           if (nside.eq.10) then
                              if (i.eq.ikxinc.and.j.eq.jkyinc) then
                                 Efourierincx(indice)
     $                                =Efourierincx(indice)+Ax/deltakxy
     $                                *zfocus
                                 Efourierincy(indice)
     $                                =Efourierincy(indice)+Ay/deltakxy
     $                                *zfocus
                                 Efourierincz(indice)
     $                                =Efourierincz(indice)+Az/deltakxy
     $                                *zfocus
                              endif
                           endif

                           
                           u1=-ky/k0
                           u2=kx/k0
                           tmp=dsqrt(u1*u1+u2*u2)

                           if (tmp.ne.0.d0) then      
                              u(1)=kx/k0
                              u(2)=ky/k0
                              u(3)=dsqrt(1.d0-u(1)*u(1)-u(2)*u(2))
     $                             *dble(nside)
                              v(1)=-kx/k0/gross
                              v(2)=-ky/k0/gross
                              v(3)=dsqrt(1.d0-v(1)*v(1)-v(2)*v(2))
     $                             *dble(nside)
                              u1=u(2)*v(3)-u(3)*v(2)
                              u2=-u(1)*v(3)+u(3)*v(1)
                              costmp=u(1)*v(1)+u(2)*v(2)+u(3)*v(3)
                              sintmp=dsqrt(u1*u1+u2*u2)
                              u1=u1/sintmp
                              u2=u2/sintmp
                              tmp=dsqrt(u(3)/v(3))

                              tmpx=(u1*u1+(1.d0-u1*u1)*costmp)
     $                             *Efourierincx(indice)+u1*u2*(1.d0
     $                             -costmp)*Efourierincy(indice)+u2
     $                             *sintmp*Efourierincz(indice)
                              tmpy=u1*u2*(1.d0-costmp)
     $                             *Efourierincx(indice)+(u2*u2+(1.d0-u2
     $                             *u2)*costmp)*Efourierincy(indice)-u1
     $                             *sintmp*Efourierincz(indice)
                              tmpz=-u2*sintmp*Efourierincx(indice)+u1
     $                             *sintmp*Efourierincy(indice)+costmp
     $                             *Efourierincz(indice)
                              Efourierincx(indice)=tmpx*tmp
                              Efourierincy(indice)=tmpy*tmp
                              Efourierincz(indice)=tmpz*tmp

                              tmpx=(u1*u1+(1.d0-u1*u1)*costmp)
     $                             *Efourierx(indice)+u1*u2*(1.d0
     $                             -costmp)*Efouriery(indice)+u2
     $                             *sintmp*Efourierz(indice)
                              tmpy=u1*u2*(1.d0-costmp)
     $                             *Efourierx(indice)+(u2*u2+(1.d0-u2
     $                             *u2)*costmp)*Efouriery(indice)-u1
     $                             *sintmp*Efourierz(indice)
                              tmpz=-u2*sintmp*Efourierx(indice)+u1
     $                             *sintmp*Efouriery(indice)+costmp
     $                             *Efourierz(indice)
                              Efourierx(indice)=tmpx*tmp
                              Efouriery(indice)=tmpy*tmp
                              Efourierz(indice)=tmpz*tmp
                           endif
                        endif
                     enddo
                  enddo
!$OMP ENDDO 
!$OMP END PARALLEL
                  
                  call fouriertoimage(deltakx,deltaky,gross,Efourierx
     $                 ,Efouriery,Efourierz,Efourierincx,Efourierincy
     $                 ,Efourierincz,nfft2D ,nfft2d2 ,plan2b,plan2f)

c     boucle sur x et y
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(ix,iy,x0,y0,indice,ctmp)   
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)                  
                  do ix=-nfft2d2,nfft2d2-1
                     do iy=-nfft2d2,nfft2d2-1
                        x0=aretecube*dble(ix)
                        y0=aretecube*dble(iy)
                        indice=(ix+nfft2d2+1)+nfft2d*(iy+nfft2d2)
                        ctmp=cdexp(-icomp*(-kxinc*x0-kyinc*y0+kzinc*z0))
                        imageconfx(ix+nfft2d2+1,iy+nfft2d2+1,iz)
     $                       =imageconfx(ix+nfft2d2+1,iy+nfft2d2+1,iz)
     $                       +Efourierincx(indice)*ctmp
                        imageconfy(ix+nfft2d2+1,iy+nfft2d2+1,iz)
     $                       =imageconfy(ix+nfft2d2+1,iy+nfft2d2+1,iz)
     $                       +Efourierincy(indice)*ctmp
                        imageconfz(ix+nfft2d2+1,iy+nfft2d2+1,iz)
     $                       =imageconfz(ix+nfft2d2+1,iy+nfft2d2+1,iz)
     $                       +Efourierincz(indice)*ctmp
                     enddo
                  enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      
c     sauvegarde les champs pour chaque incidence dans le plan image
                  if (nmicsavefield.ge.2.and.iz.eq.nplanze) then
                     write(*,*) 'write the image data'
                     k=0
                     j=0
                     call integer_string4(niterii,fil4)
                     name='Nb_illumination'//fil4
                     call writehdf5mic(Efourierincx,Efourierincy
     $                    ,Efourierincz,nfft2d,j,Ediffkzpos,k,name
     $                    ,group_id_conf_image)
                     dim(1)=3
                     dim(2)=3
                     datasetname='Nb_illumination'//fil4//'kinc'
                     u(1)=kxinc
                     u(2)=kyinc
                     u(3)=kzinc
                     call hdf5write1d(group_id_conf_image,datasetname,u,
     $                    dim)
                     datasetname='Nb_illumination'//fil4//'uinc'
                     tmp=dsqrt(cdabs(Ax)*cdabs(Ax)+cdabs(Ay)*cdabs(Ay)
     $                    +cdabs(Az)*cdabs(Az))
                     u(1)=cdabs(Ax)/tmp
                     u(2)=cdabs(Ay)/tmp
                     u(3)=cdabs(Az)/tmp
                     call hdf5write1d(group_id_conf_image,datasetname,u,
     $                    dim)
                     dim(1)=1
                     dim(2)=1
                     datasetname='Nb_illumination'//fil4//'NormeE0'
                     call hdf5write(group_id_conf_image,datasetname,tmp
     $                    ,dim)
                  endif
                              
c     fin boucle z                  
               enddo

               
c     if masque
            endif
            if (nstop.eq.-1) then
               infostr =
     $              'Calculation cancelled during iterative method'
               if (nmicsavefield.ge.1) then
#ifdef USE_HDF5                  
                  CALL h5gclose_f(group_id_conf,error)
                  CALL h5gclose_f(group_id_conf_fourier,error)
                  CALL h5gclose_f(group_id_conf_image,error)      
                  call h5fclose_f(file_idmicros,error)
                  file_idmicros=0
#endif                    
               endif               
               return
            endif
c     fin boucles des kxinc et kyinc
         enddo
      enddo

      deltax=2.d0*pi/dble(nfft2d)/deltakx

      if (nmat.eq.0) then
         open(400,file='kxfourier.mat')
         open(401,file='ximage.mat')
         open(402,file='zimage.mat')
         do i=-nfft2d2,nfft2d2-1
            write(401,*) deltax*dble(i)
         enddo
         do iz=1,nplanz
            zlens=zinf+stepz*dble(iz-1)
            write(402,*) zlens
         enddo
         do i=-imaxk0,imaxk0
            kx=deltakx*dble(i)
            write(400,*) kx/k0
         enddo
         close(400)
         close(401)
         close(402)

         open(310,file='kxmic.mat')
         open(311,file='kymic.mat')
         open(312,file='masquemic.mat')         
         do jdelta=-ideltam,ideltam
            do idelta=-ideltam,ideltam          
               kxinc=dble(idelta)*deltak
               kyinc=dble(jdelta)*deltak
               i=ideltam+idelta+1
               j=ideltam+jdelta+1
               if (jdelta.eq.0) write(310,*) kxinc/k0
               if (idelta.eq.0) write(311,*) kyinc/k0
               tmp=cdabs(masque(i+(j-1)*(2*ideltam+1)))
               write(312,*) tmp
            enddo
         enddo
         close (310)
         close (311)
         close (312)

c     ouvre les fichiers de sauvegarde de l'image confocale
         open(301,file='fieldconfm.mat')
         open(302,file='fieldconfx.mat')
         open(303,file='fieldconfy.mat')
         open(304,file='fieldconfz.mat')
  
         
         do k=1,nplanz
            do j=1,nfft2D
               do i=1,nfft2D
                  x0=dreal(imageconfx(i,j,k)*dconjg(imageconfx(i,j,k)))
                  y0=dreal(imageconfy(i,j,k)*dconjg(imageconfy(i,j,k)))
                  z0=dreal(imageconfz(i,j,k)*dconjg(imageconfz(i,j,k)))
                  
                  write(302,*) dreal(imageconfx(i,j,k))
     $                 ,dimag(imageconfx(i,j,k))
                  write(303,*) dreal(imageconfy(i,j,k))
     $                 ,dimag(imageconfy(i,j,k))
                  write(304,*) dreal(imageconfz(i,j,k))
     $                 ,dimag(imageconfz(i,j,k))
                  write(301,*) dsqrt(x0+y0+z0)
               enddo
            enddo
         enddo
         close(301)
         close(302)
         close(303)
         close(304)
         
      elseif (nmat.eq.2) then
         dim(1)=nplanz
         dim(2)=nfft2d
         datasetname='z Image'
         do iz=1,nplanz
            xy(iz)=zinf+stepz*dble(iz-1)
         enddo
         call hdf5write1d(group_idmic,datasetname,xy,dim)
         dim(1)=nfft2d
         dim(2)=nfft2d
         datasetname='x Image'
         do i=-nfft2d2,nfft2d2-1
            xy(i+nfft2d2+1)=deltax*dble(i)
         enddo
         call hdf5write1d(group_idmic,datasetname,xy,dim)
         
         do idelta=-ideltam,ideltam
            kxy(idelta+ideltam+1)=deltak*dble(idelta)/k0
         enddo
         dim(1)=ideltam*2+1
         dim(2)=nfft2d
         datasetname='kxmic'
         call hdf5write1d(group_idmic,datasetname,kxy,dim)
         datasetname='kymic'
         call hdf5write1d(group_idmic,datasetname,kxy,dim)
         dim(1)=(ideltam*2+1)**2
         dim(2)=nmasque*nmasque
         dim(3)=1
         dim(4)=1
         datasetname='masquemic'
         do jdelta=-ideltam,ideltam
            do idelta=-ideltam,ideltam
               i=ideltam+idelta+1
               j=ideltam+jdelta+1
               masque(i+(j-1)*(2*ideltam+1))=cdabs(masque(i+(j-1)*(2
     $              *ideltam+1))) *(1.d0,0.d0)
            enddo
         enddo
         call hdf5write1d(group_idmic,datasetname,dreal(masque),dim)
c     passe en intensite
         
         dim(1)=nfft2d*nfft2d*nplanz
         dim(2)=nfft2d*nfft2d*nplanz             

         datasetname='Confocal field x component real part'
         call hdf5write1d(group_idmic,datasetname,dreal(imageconfx),dim)
         datasetname='Confocal field x component imaginary part'
         call hdf5write1d(group_idmic,datasetname,dimag(imageconfx),dim)
         datasetname='Confocal field y component real part'
         call hdf5write1d(group_idmic,datasetname,dreal(imageconfy),dim)
         datasetname='Confocal field y component imaginary part'
         call hdf5write1d(group_idmic,datasetname,dimag(imageconfy),dim)
         datasetname='Confocal field z component real part'
         call hdf5write1d(group_idmic,datasetname,dreal(imageconfz),dim)
         datasetname='Confocal field z component imaginary part'
         call hdf5write1d(group_idmic,datasetname,dimag(imageconfz),dim)

         datasetname='Confocal field modulus'         
         do k=1,nplanz
            do j=1,nfft2D
               do i=1,nfft2D
                  x0=dreal(imageconfx(i,j,k)*dconjg(imageconfx(i,j,k)))
                  y0=dreal(imageconfy(i,j,k)*dconjg(imageconfy(i,j,k)))
                  z0=dreal(imageconfz(i,j,k)*dconjg(imageconfz(i,j,k)))
                  imageconfx(i,j,k)=dsqrt(x0+y0+z0)
               enddo
            enddo
         enddo
         call hdf5write1d(group_idmic,datasetname,dreal(imageconfx),dim)
               
      endif

c     sauvegarde le plan milieu pour l'interface graphique.

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,indice)   
!$OMP DO SCHEDULE(STATIC)       
      do j=1,nfft2D
         do i=1,nfft2D
            indice=i+nfft2D*(j-1)
            Eimagex(indice)=imageconfx(i,j,nplanze)
            Eimagey(indice)=imageconfy(i,j,nplanze)
            Eimagez(indice)=imageconfz(i,j,nplanze)
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)   
      do i=-nfft2d2,nfft2d2-1
         xy(i+nfft2d2+1)=deltax*dble(i)*gross
         kxy(i+nfft2d2+1)=deltakx*dble(i)/k0
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      
      deallocate(imageconfx)
      deallocate(imageconfy)
      deallocate(imageconfz)
      
      if (nmicsavefield.ge.1) then
#ifdef USE_HDF5           
         CALL h5gclose_f(group_id_conf,error)
         CALL h5gclose_f(group_id_conf_fourier,error)
         CALL h5gclose_f(group_id_conf_image,error)      
         call h5fclose_f(file_idmicros,error)
         file_idmicros=0
#endif           
      endif

      
      end
