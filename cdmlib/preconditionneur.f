c     cette routine multiplie un vecteur par la matrice inverse estimée
c     de la matrice toeplitz initialie.

      subroutine preconditionneur(FF,xi,xr,tabdip,polarisa,chanxxvect
     $     ,chanxyvect,chanxzvect,chanyyvect,chanyzvect,chanzzvect,nx,ny
     $     ,nz ,nbsphere,ndipole,nxm,nym,nzm,aretecube,k0,ntest,sdetnn
     $     ,ipvtnn ,vectxx ,vectyy,vectzz,plan2b,plan2f ,infostr ,nstop)
      implicit none
      integer nx,ny,nz,nbsphere,nstop,nxm,nym,nzm,ntest,nz3,ndipole
      double precision x,y,z,x0,y0,z0,aretecube,k0,test,tmp,tmp1,tmp2
      integer, dimension(nxm*nym*nzm) :: Tabdip
      double complex FF(3*nxm*nym*nzm),polarisa(nxm*nym*nzm,3,3),xi(3
     $     *nxm*nym*nzm),xr(3*nxm*nym*nzm),propaesplibre(3,3)

      character(64) infostr

      integer i,j,k,ii,jj,kk,ix,iy,iz,i2,j2,k2,kkk,l,ll,k1,j1
      double precision dn1x,dn2x,dn1y,dn2y
      double complex polamoy,dn11c,dn12c,dn21c,dn22c
      double complex chanxxvect(nxm*nym*nzm) ,chanxyvect(nxm*nym*nzm)
     $     ,chanxzvect(nxm*nym*nzm) ,chanyyvect(nxm*nym*nzm)
     $     ,chanyzvect(nxm*nym*nzm) ,chanzzvect(nxm*nym*nzm)

      double complex sdetnn(3*nzm,3*nzm ,nxm*nym)

      integer n1,n2,n3
      double precision dnxy,normefrobenius

      integer job,lda,info,lwork,NRHS
      
      integer nmaxcompo
      double complex vectxx(nx*ny),vectyy(nx*ny),vectzz(nx*ny)
      double complex , dimension (:,:), allocatable :: sdettmp
      double complex , dimension (:), allocatable :: Txx,Txy,Txz ,Tyy
     $     ,Tyz ,Tzz,chanxx,chanxy,chanxz,chanyy,chanyz,chanzz,worktmp
     $     ,work
      integer , dimension (:),allocatable ::ipvttmp
      integer jx,jy,ipvtnn(nz*3,nx*ny)
      double complex , dimension (:,:), allocatable :: spola

      integer FFTW_FORWARD,FFTW_ESTIMATE,FFTW_BACKWARD
      integer*8 plan2b,plan2f
      integer valuesf(8),valuesi(8)
      double precision tf,ti
      character(64) message
      character(8)  :: date
      character(10) :: time
      character(5)  :: zone
      
      FFTW_FORWARD=-1
      FFTW_BACKWARD=+1
      FFTW_ESTIMATE=64
      nz3=nz*3
      if (ntest.eq.0) then

c     ****************************************
c     calcul polarisabilité moyenne
c     ****************************************
c         message='Chan'
c         call cpu_time(ti)
c         call date_and_time(date,time,zone,valuesi)
         polamoy=0.d0
         k=0
         do i=1,nbsphere
            polamoy=polamoy+polarisa(i,2,2)
            if (cdabs(polarisa(i,2,2)).ne.0) k=k+1
         enddo
         polamoy=polamoy/dble(k)

         normefrobenius=0.d0
         tmp=0.d0
         tmp1=0.d0
         tmp2=0.d0


         dnxy=dble(nx*ny)

c     Il faut effectuer une TF de longueur nx sur ny*nz, cad ny*nz FFT
c     de longueur nx
         x0=0.d0
         y0=0.d0
         z0=0.d0
c     boucle sur ny et nz
         nmaxcompo=nx*ny
         allocate(Txx(1:nmaxcompo))
         allocate(Txy(1:nmaxcompo))
         allocate(Txz(1:nmaxcompo))
         allocate(Tyy(1:nmaxcompo))
         allocate(Tyz(1:nmaxcompo))
         allocate(Tzz(1:nmaxcompo))
         allocate(chanxx(1:nmaxcompo))
         allocate(chanxy(1:nmaxcompo))
         allocate(chanxz(1:nmaxcompo))
         allocate(chanyy(1:nmaxcompo))
         allocate(chanyz(1:nmaxcompo))
         allocate(chanzz(1:nmaxcompo))
         allocate(spola(nx,ny))

         do i=1,nz
         
c     calcul du propagateur
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(k,x,j,y,propaesplibre)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
            do j=1,ny
               do k=1,nx               
                  x=aretecube*dble(k-1)
                  y=aretecube*dble(j-1)
                  z=aretecube*dble(i-1)
                  call propa_espace_libre(x,y,z,x0,y0,z0,k0
     $                 ,propaesplibre)
                  Txx(k+nx*(j-1))=-propaesplibre(1,1)
                  Txy(k+nx*(j-1))=-propaesplibre(1,2)
                  Txz(k+nx*(j-1))=-propaesplibre(1,3)
                  Tyy(k+nx*(j-1))=-propaesplibre(2,2)
                  Tyz(k+nx*(j-1))=-propaesplibre(2,3)
                  Tzz(k+nx*(j-1))=-propaesplibre(3,3)
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL  


c     calcul de spola
c$$$            kk=nx*ny*(i-1)
c$$$            do ix=1,nx
c$$$               do iy=1,ny
c$$$                  spola(ix,iy)=0.d0
c$$$                  do jx=1,nx-ix+1
c$$$                     do jy=1,ny-iy+1
c$$$                        spola(ix,iy)=spola(ix,iy)+(polarisa(jx+nx *(jy-1
c$$$     $                       )+kk,2,2))
c$$$                     enddo
c$$$                  enddo
c$$$                  spola(ix,iy)=spola(ix,iy)/dnxy
c$$$               enddo
c$$$            enddo


!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,k1,j1)
!$OMP& PRIVATE(dn11c,dn21c,dn12c,dn22c,dn1x,dn2x,dn1y,dn2y)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
            do j=1,ny
               do k=1,nx    
                  dn1x=dble(k-1)
                  dn2x=dble(nx-k+1)
                  dn1y=dble(j-1) 
                  dn2y=dble(ny-j+1)
c                  dn11c=spola(1,1)+spola(k,j)-spola(1,j)-spola(k,1)
c                  dn12c=spola(1,j)-spola(k,j)
c                  dn21c=spola(k,1)-spola(k,j)
c                  dn22c=spola(k,j)
c                  write(*,*) '***********',k,j
c                  write(*,*) 'dn2x*dn2y',dn2x*dn2y/dnxy,spola(k,j)
c     $                 /polamoy
c                  write(*,*) 'dn1x*dn1y',dn1x*dn1y/dnxy,(spola(1,1)
c     $                 +spola(k,j)-spola(1,j)-spola(k,1))/polamoy
c                  write(*,*) 'dn1x*dn2y',dn1x*dn2y/dnxy,(spola(1,j)
c     $                 -spola(k,j))/polamoy
c                  write(*,*) 'dn2x*dn1y',dn2x*dn1y/dnxy,(spola(k,1)
c     $                 -spola(k,j))/polamoy
                  
                  k1=nx-k+2

                  j1=ny-j+2

                  if (k.eq.1) then
                     k1=1
                  endif
                  if (j.eq.1) then
                     j1=1
                  endif
c$$$
c$$$                  chanxx(k1+nx*(j1-1))=dn11c*Txx(k1+nx*(j1-1))+dn12c
c$$$     $                 *Txx(k1+nx*(j-1))+dn21c*Txx(k+nx*(j1-1))+dn22c
c$$$     $                 *Txx(k+nx*(j-1))
c$$$                  chanxy(k1+nx*(j1-1))=dn11c*Txy(k1+nx*(j1-1))-dn12c
c$$$     $                 *Txy(k1+nx*(j-1))-dn21c*Txy(k+nx*(j1-1))+dn22c
c$$$     $                 *Txy(k+nx*(j-1))
c$$$                  chanxz(k1+nx*(j1-1))=dn11c*Txz(k1+nx*(j1-1))+dn12c
c$$$     $                 *Txz(k1+nx*(j-1))-dn21c*Txz(k+nx*(j1-1))-dn22c
c$$$     $                 *Txz(k+nx*(j-1))
c$$$                  chanyy(k1+nx*(j1-1))=dn11c*Tyy(k1+nx*(j1-1))+dn12c
c$$$     $                 *Tyy(k1+nx*(j-1))+dn21c*Tyy(k+nx*(j1-1))+dn22c
c$$$     $                 *Tyy(k+nx*(j-1))
c$$$                  chanyz(k1+nx*(j1-1))=dn11c*Tyz(k1+nx*(j1-1))-dn12c
c$$$     $                 *Tyz(k1+nx*(j-1))+dn21c*Tyz(k+nx*(j1-1))-dn22c
c$$$     $                 *Tyz(k+nx*(j-1))
c$$$                  chanzz(k1+nx*(j1-1))=dn11c*Tzz(k1+nx*(j1-1))+dn12c
c$$$     $                 *Tzz(k1+nx*(j-1))+dn21c*Tzz(k+nx*(j1-1))+dn22c
c$$$     $                 *Tzz(k+nx*(j-1))

                  



                  chanxx(k1+nx*(j1-1))=(dn1x*dn1y*Txx(k1+nx*(j1-1))+dn1x
     $                 *dn2y*Txx(k1+nx*(j-1))+dn2x*dn1y*Txx(k+nx*(j1-1))
     $                 +dn2x*dn2y*Txx(k+nx*(j-1)))/dnxy*polamoy
                  chanxy(k1+nx*(j1-1))=(dn1x*dn1y*Txy(k1+nx*(j1-1))-dn1x
     $                 *dn2y*Txy(k1+nx*(j-1))-dn2x*dn1y*Txy(k+nx*(j1-1))
     $                 +dn2x*dn2y*Txy(k+nx*(j-1)))/dnxy*polamoy
                  chanxz(k1+nx*(j1-1))=(dn1x*dn1y*Txz(k1+nx*(j1-1))+dn1x
     $                 *dn2y*Txz(k1+nx*(j-1))-dn2x*dn1y*Txz(k+nx*(j1-1))
     $                 -dn2x*dn2y*Txz(k+nx*(j-1)))/dnxy*polamoy
                  chanyy(k1+nx*(j1-1))=(dn1x*dn1y*Tyy(k1+nx*(j1-1))+dn1x
     $                 *dn2y*Tyy(k1+nx*(j-1))+dn2x*dn1y*Tyy(k+nx*(j1-1))
     $                 +dn2x*dn2y*Tyy(k+nx*(j-1)))/dnxy*polamoy
                  chanyz(k1+nx*(j1-1))=(dn1x*dn1y*Tyz(k1+nx*(j1-1))-dn1x
     $                 *dn2y*Tyz(k1+nx*(j-1))+dn2x*dn1y*Tyz(k+nx*(j1-1))
     $                 -dn2x*dn2y*Tyz(k+nx*(j-1)))/dnxy*polamoy
                  chanzz(k1+nx*(j1-1))=(dn1x*dn1y*Tzz(k1+nx*(j1-1))+dn1x
     $                 *dn2y*Tzz(k1+nx*(j-1))+dn2x*dn1y*Tzz(k+nx*(j1-1))
     $                 +dn2x*dn2y*Tzz(k+nx*(j-1)))/dnxy*polamoy

               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL


            if (i.eq.1) then
               chanxx(1)=chanxx(1)+1.d0
               chanyy(1)=chanyy(1)+1.d0
               chanzz(1)=chanzz(1)+1.d0
            endif
c            write(*,*)  chanxx(1),chanyy(1),chanzz(1),i

c               do k=1,nx
c                  do j=1,ny
c                     write(*,*) 'chanxx',chanxx(k+nx*(j-1)),k,j
c                     write(*,*) 'chanxy',chanxy(k+nx*(j-1)),k,j
c                     write(*,*) 'chanxz',chanxz(k+nx*(j-1)),k,j
c                     write(*,*) 'chanyy',chanyy(k+nx*(j-1)),k,j
c                     write(*,*) 'chanyz',chanyz(k+nx*(j-1)),k,j
c                     write(*,*) 'chanzz',chanzz(k+nx*(j-1)),k,j
c                  enddo
c               enddo
#ifdef USE_FFTW
            call dfftw_execute_dft(plan2b, chanxx, chanxx)   
            call dfftw_execute_dft(plan2b, chanxy, chanxy)   
            call dfftw_execute_dft(plan2b, chanxz, chanxz)   
            call dfftw_execute_dft(plan2b, chanyy, chanyy)   
            call dfftw_execute_dft(plan2b, chanyz, chanyz)   
            call dfftw_execute_dft(plan2b, chanzz, chanzz)   
#else 
            call fftsingletonz2d(chanxx,nx,ny,FFTW_BACKWARD)
            call fftsingletonz2d(chanxy,nx,ny,FFTW_BACKWARD)
            call fftsingletonz2d(chanxz,nx,ny,FFTW_BACKWARD)
            call fftsingletonz2d(chanyy,nx,ny,FFTW_BACKWARD)
            call fftsingletonz2d(chanyz,nx,ny,FFTW_BACKWARD)
            call fftsingletonz2d(chanzz,nx,ny,FFTW_BACKWARD)
#endif
            kk=nx*ny*(i-1)
c     réécrit ce vecteur dans sa partie
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(j,k,ii,kkk)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
            do j=1,ny
               do k=1,nx
                  ii=k+nx*(j-1)
                  kkk=j+ny*(k-1)+kk
                  chanxxvect(kkk)=chanxx(ii)
                  chanxyvect(kkk)=chanxy(ii)
                  chanxzvect(kkk)=chanxz(ii)
                  chanyyvect(kkk)=chanyy(ii)
                  chanyzvect(kkk)=chanyz(ii)
                  chanzzvect(kkk)=chanzz(ii)
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         enddo
c         do i=1,nx*ny*nz
c            write(*,*) 'chanxx',chanxxvect(i),i
c            write(*,*) 'chanxy',chanxyvect(i),i
c            write(*,*) 'chanxz',chanxzvect(i),i
c            write(*,*) 'chanyy',chanyyvect(i),i
c            write(*,*) 'chanyz',chanyzvect(i),i
c            write(*,*) 'chanzz',chanzzvect(i),i
c         enddo

         deallocate(Txx)
         deallocate(Txy)
         deallocate(Txz)
         deallocate(Tyy)
         deallocate(Tyz)
         deallocate(Tzz)
         deallocate(chanxx)
         deallocate(chanxy)
         deallocate(chanxz)
         deallocate(chanyy)
         deallocate(chanyz)
         deallocate(chanzz)
         deallocate(spola)

         k=1
         job=11
         lda=nz3
         lwork=4*nz3
         allocate(Sdettmp(nz3,nz3))
         allocate(worktmp(nz3))
         allocate(work(lwork))
         allocate(ipvttmp(nz3))

!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(l,ll,kk,kkk,ix,iy,i,j)
!$OMP& PRIVATE(test,Sdettmp,ipvttmp,info,worktmp,work)
!$OMP DO SCHEDULE(STATIC)           
         do l=1,nx*ny

            do kk=1,nz
               do kkk=1,nz
                  ll=l+nx*ny*abs(kk-kkk)
                  ix=3*(kk-1)
                  iy=3*(kkk-1)
                  test=dble(sign(k,kk-kkk))
                  Sdettmp(ix+1,iy+1)=chanxxvect(ll)
                  Sdettmp(ix+1,iy+2)=chanxyvect(ll)
                  Sdettmp(ix+1,iy+3)=chanxzvect(ll)*test
                  Sdettmp(ix+2,iy+1)=chanxyvect(ll)
                  Sdettmp(ix+2,iy+2)=chanyyvect(ll)
                  Sdettmp(ix+2,iy+3)=chanyzvect(ll)*test
                  Sdettmp(ix+3,iy+1)=chanxzvect(ll)*test
                  Sdettmp(ix+3,iy+2)=chanyzvect(ll)*test
                  Sdettmp(ix+3,iy+3)=chanzzvect(ll)                     
               enddo
            enddo     

            call ZGETRF( nz3, nz3, sdettmp, nz3, IPVttmp, INFO )
            if (INFO.ne.0) then
               write(*,*) 'problem in zgetrf precondionner',info,i
               stop
            endif
            do i=1,nz3
               do j=1,nz3
                  Sdetnn(i,j,l)=Sdettmp(i,j)
               enddo
               IPVtnn(i,l)=IPVttmp(i)
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL

         deallocate(Sdettmp)
         deallocate(worktmp)
         deallocate(work)
         deallocate(ipvttmp)
       

c         call cpu_time(tf)
c         call date_and_time(date,time,zone,valuesf)
c         
c         call calculatedate(valuesf,valuesi,tf,ti,message)
      
      elseif (ntest.eq.1) then
c         message='produit'
c         call cpu_time(ti)
c         call date_and_time(date,time,zone,valuesi)


 
         do i=1,nz
c     sauvegarde dans un petit vecteur

!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(j,k,kk)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
            do j=1,ny
               do k=1,nx
                  kk=tabdip(k+nx*(j-1)+nx*ny*(i-1))
                  if (kk.eq.0) then
                     vectxx(k+nx*(j-1))=0.d0
                     vectyy(k+nx*(j-1))=0.d0
                     vectzz(k+nx*(j-1))=0.d0
                  else
                     vectxx(k+nx*(j-1))=FF(3*(kk-1)+1)
                     vectyy(k+nx*(j-1))=FF(3*(kk-1)+2)
                     vectzz(k+nx*(j-1))=FF(3*(kk-1)+3)
                  endif
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL

#ifdef USE_FFTW
            call dfftw_execute_dft(plan2b,vectxx,vectxx)
            call dfftw_execute_dft(plan2b,vectyy,vectyy)
            call dfftw_execute_dft(plan2b,vectzz,vectzz)
#else 
            call fftsingletonz2d(vectxx,nx,ny,FFTW_BACKWARD)
            call fftsingletonz2d(vectyy,nx,ny,FFTW_BACKWARD)
            call fftsingletonz2d(vectzz,nx,ny,FFTW_BACKWARD)         
#endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(j,k,kk,kkk)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
            do j=1,ny
               do k=1,nx
                  kk=k+nx*(j-1)
                  kkk=i+nz*(j-1)+nz*ny*(k-1)
                  xi(3*(kkk-1)+1)=vectxx(kk)
                  xi(3*(kkk-1)+2)=vectyy(kk)
                  xi(3*(kkk-1)+3)=vectzz(kk)
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL

         enddo

         lda=nz3
         NRHS=1
c     ***********************************************
c     fin calcul vecteur 
c     ***********************************************

         
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(l,i,j,k,kk,ii,jj)
!$OMP DO SCHEDULE(STATIC) 
         do l=1,nx*ny
            call  zgetrs( 'N',nz3, NRHS, Sdetnn(1:nz3,1:nz3,l), LDA,
     $           IPVtnn(1:nz3,l),xi((l-1)*nz3+1:l*nz3),LDA,INFO)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         

c     ****************************************
c     refait les opérations inverses
c     ****************************************
         dnxy=dble(nx*ny)
         do i=1,nz
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(j,k,kk,kkk)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
            do j=1,ny
               do k=1,nx
                  kk=k+nx*(j-1)
                  kkk=i+nz*(j-1)+nz*ny*(k-1)
                  vectxx(kk)=xi(3*(kkk-1)+1)
                  vectyy(kk)=xi(3*(kkk-1)+2)
                  vectzz(kk)=xi(3*(kkk-1)+3)
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL

#ifdef USE_FFTW
            call dfftw_execute_dft(plan2f,vectxx,vectxx)
            call dfftw_execute_dft(plan2f,vectyy,vectyy)
            call dfftw_execute_dft(plan2f,vectzz,vectzz)
#else 
            call fftsingletonz2d(vectxx,nx,ny,FFTW_FORWARD)
            call fftsingletonz2d(vectyy,nx,ny,FFTW_FORWARD)
            call fftsingletonz2d(vectzz,nx,ny,FFTW_FORWARD)         
#endif
            
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(j,k,kk)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)      
            do j=1,ny
               do k=1,nx
                  kk=tabdip(k+nx*(j-1)+nx*ny*(i-1))
                  if (kk.ne.0) then
                     FF(3*(kk-1)+1)=vectxx(k+nx*(j-1))/dnxy
                     FF(3*(kk-1)+2)=vectyy(k+nx*(j-1))/dnxy
                     FF(3*(kk-1)+3)=vectzz(k+nx*(j-1))/dnxy
                  endif
               enddo
            enddo   
!$OMP ENDDO 
!$OMP END PARALLEL
                     
         enddo



c         call cpu_time(tf)
c         call date_and_time(date,time,zone,valuesf)      
c         call calculatedate(valuesf,valuesi,tf,ti,message)
         
      endif
   
      
      end
