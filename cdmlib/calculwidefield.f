      subroutine calculwidefield(nmat,nrig,nx,ny,nz,nbsphere,subunit
     $     ,nmaxpp,nxmp,nymp,nzmp,nquad,nxm,nym,nzm,nfft2d,ntotalm,nmax
     $     ,speckseed,irra,numaperil ,aretecube ,k0,lambda ,theta, phi
     $     ,pp,ss, P0,w0,xdip ,ydip ,zdip,xgaus, ygaus, zgaus,tolinit,E0
     $     ,E0m,ssm,ppm,thetam ,phim,nbinc,xs,ys ,zs ,xswf,yswf ,zswf,xi
     $     ,xr,FFloc,FF,FF0 ,incidentfieldx , incidentfieldy
     $     ,incidentfieldz,incidentfield ,Eimagex ,Eimagey,Eimagez
     $     ,polarisa ,epsilon,beam,infostr,namefileinc ,FFTTENSORxx
     $     ,FFTTENSORxy,FFTTENSORxz ,FFTTENSORyy ,FFTTENSORyz
     $     ,FFTTENSORzz,vectx,vecty,vectz ,plan2f,plan2b, group_idnf
     $     ,nstop)

#ifdef USE_HDF5
      use HDF5
#endif     
      
      implicit none
      integer nx,ny,nz,nxmp,nymp,nzmp,nquad,nxm,nym ,nzm,nfft2d,nmat
     $     ,nbsphere,nrig,nstop,speckseed,ntotalm,nmax
      double precision aretecube,k0,theta, phi, pp, ss, P0, w0, xgaus,
     $     ygaus, zgaus,xdip,ydip,zdip,tolinit,tol,lambda,numaperil,irra
      DOUBLE PRECISION,DIMENSION(nxm*nym*nzm)::xs,ys,zs
      DOUBLE PRECISION,DIMENSION(nxm*nym*nzm)::xswf,yswf,zswf
      double complex, dimension(3*nxm*nym*nzm) :: xr,xi, FFloc,FF,FF0
      double complex, dimension(8*nxm*nym*nzm) :: FFTTENSORxx,
     $     FFTTENSORxy,FFTTENSORxz,FFTTENSORyy,FFTTENSORyz,
     $     FFTTENSORzz,vectx,vecty,vectz
      DOUBLE PRECISION, DIMENSION(nxm*nym*nzm) :: incidentfield
      double complex, dimension(nxm*nym*nzm) :: incidentfieldx
      double complex, dimension(nxm*nym*nzm) :: incidentfieldy
      double complex, dimension(nxm*nym*nzm) :: incidentfieldz
      double complex, dimension(nxm*nym*nzm,3,3) :: polarisa,epsilon
      double complex Eimagex(nfft2d*nfft2d),Eimagey(nfft2d*nfft2d)
     $     ,Eimagez(nfft2d*nfft2d)
      integer nbinc
      double precision thetam(10), phim(10), ppm(10), ssm(10)
      double complex E0m(10)
      double complex E0
      character(64) beam,infostr,namefileinc
#ifndef USE_HDF5
      integer,parameter:: hid_t=4
#endif
      integer(hid_t) :: group_idnf

      integer nxmpp,nympp,nzmpp,nmaxpp,nxm2,nym2,nzm2,nxym2,l,subunit,i
     $     ,j,k,cntwf, FFTW_FORWARD,FFTW_ESTIMATE,FFTW_BACKWARD,ii,jj,ll
     $     ,nsubunit,comparaison,nloin

      integer dim(4)
      double precision xmax,xmin,ymax,ymin,zmax,zmin,x,y,z,tmp
      integer*8 plan2f,plan2b,planfn,planbn
      character(LEN=100) :: datasetname

      
      write(*,*) '*************************************************'      
      write(*,*) '************* STUDY LARGE NEAR  FIELD ***********'
      write(*,*) '*************************************************'
      
      nxmpp=nx+2*nxmp
      nympp=ny+2*nymp
      nzmpp=nz+2*nzmp
      nmaxpp=nxmpp*nympp*nzmpp
      FFTW_FORWARD=-1
      FFTW_BACKWARD=+1
      FFTW_ESTIMATE=64
      nloin=0
      nxm2=2*nxmpp
      nym2=2*nympp
      nzm2=2*nzmpp
      nxym2=nxm2*nym2
c 0=rigorous, 1=renormalized Born, 2=Born, 3=Born order 1, 4=renormalized
c Rytov, 5=Rytov, 6=Beam propagation, 7=renormalized Beam propagation]
c      if (nrig.eq.0.or.nrig.eq.3.or.nrig.eq.4.or.nrig.eq.5) then
         
#ifdef USE_FFTW
         call dfftw_plan_dft_3d(planbn,nxm2,nym2,nzm2,vectx,vectx
     $        ,FFTW_BACKWARD,FFTW_ESTIMATE)
         call dfftw_plan_dft_3d(planfn,nxm2,nym2,nzm2,vectx,vectx
     $        ,FFTW_FORWARD,FFTW_ESTIMATE)
#endif

         if (nquad.eq.0) then
            call greencalculfft(nxmpp,nympp,nzmpp,nxm2,nym2,nzm2,nxym2
     $           ,nxm,nym,nzm,aretecube,k0,FFTTENSORxx,FFTTENSORxy
     $           ,FFTTENSORxz,FFTTENSORyy ,FFTTENSORyz,FFTTENSORzz
     $           ,planbn)
         else
            call greencalculquadfft(nxmpp,nympp,nzmpp,nxm2,nym2,nzm2
     $           ,nxym2,nxm,nym,nzm,nquad,tolinit,aretecube,k0
     $           ,FFTTENSORxx,FFTTENSORxy,FFTTENSORxz ,FFTTENSORyy
     $           ,FFTTENSORyz,FFTTENSORzz,planbn) 
         endif
c      endif
            
         
c     compte the size and edge of the box
      xmax=-1.d300
      xmin=1.d300
      ymax=-1.d300
      ymin=1.d300
      zmax=-1.d300
      zmin=1.d300      
!$OMP PARALLEL 
!$OMP DO SCHEDULE(STATIC) REDUCTION(max:xmax,ymax,zmax)
!$OMP& REDUCTION(min:xmin,ymin,zmin)  
      do i=1,nbsphere
         xmax=max(xmax,xs(i))
         xmin=min(xmin,xs(i))
         ymax=max(ymax,ys(i))
         ymin=min(ymin,ys(i))
         zmax=max(zmax,zs(i))
         zmin=min(zmin,zs(i))
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL   
         
      write(*,*) 'Size of the box for the near field'
      write(*,*) 'xmin and xmax =',xmin,xmax
      write(*,*) 'ymin and ymax =',ymin,ymax
      write(*,*) 'zmin and zmax =',zmin,zmax
      write(*,*) 'meshsize = ',aretecube
c     compute new position of computation, dipole and incident field at
c     the new grid
      write(*,*) 'Discretization of the box for the near field'
      write(*,*) 'nx =',nx,'additional',nxmp,'total',nxmpp
      write(*,*) 'ny =',ny,'additional',nymp,'total',nympp
      write(*,*) 'nz =',nz,'additional',nzmp,'total',nzmpp
      
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
      do i=1,3*nmaxpp
         xi(i)=0.d0
         xr(i)=0.d0
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         
      l=1
      subunit=0
c     initialize and reuse polarisa : pola= background
        
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)
      do i=1,nmaxpp            
         polarisa(i,1,1)=1.d0
         polarisa(i,2,2)=1.d0
         polarisa(i,3,3)=1.d0
         polarisa(i,1,2)=0.d0
         polarisa(i,1,3)=0.d0
         polarisa(i,2,1)=0.d0
         polarisa(i,2,3)=0.d0
         polarisa(i,3,1)=0.d0
         polarisa(i,3,2)=0.d0
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      cntwf = 0
c     write xyz wf for hdf5
      if (nmat.eq.2) then
         do i=1,nzmpp
            zswf(i)=zmin+dble(i-nzmp-1)*aretecube
         enddo
         do j=1,nympp
            yswf(j)=ymin+dble(j-nymp-1)*aretecube
         enddo
         do k=1,nxmpp
            xswf(k)=xmin+dble(k-nxmp-1)*aretecube
         enddo
         dim(1)=nzmpp
         dim(2)=nmax
         datasetname='zwf'
         call hdf5write1d(group_idnf,datasetname,zswf,dim)
         dim(1)=nympp
         datasetname='ywf'
         call hdf5write1d(group_idnf,datasetname,yswf,dim)
         dim(1)=nxmpp
         datasetname='xwf'
         call hdf5write1d(group_idnf,datasetname,xswf,dim)
         
      endif
      
      if (beam(1:8).eq.'gfftwave' .or. beam(1:7).eq.'speckle' .or.
     $     beam(1:11).eq.'demispeckle'.or.beam(1:8).eq.'confocal'
     $     .or. beam(1:12).eq.'demiconfocal') then
         do i=1,nzmpp
            do j=1,nympp
               do k=1,nxmpp
                  cntwf = cntwf + 1
                  zswf(cntwf) = zmin+dble(i-nzmp-1)*aretecube
                  yswf(cntwf) = ymin+dble(j-nymp-1)*aretecube
                  xswf(cntwf) = xmin+dble(k-nxmp-1)*aretecube
               enddo
            enddo
         enddo
         write(*,*) 'beam wide',beam
         if (beam(1:8).eq.'gfftwave') then
            call  gaussianchampfft(xswf,yswf,zswf,aretecube,k0,w0,E0
     $           ,ss,pp,theta,phi,xgaus,ygaus,zgaus,beam,nmaxpp,nxmpp
     $           ,nympp ,nzmpp,nxm,nym ,nzm,nmax,nfft2d,Eimagex
     $           ,Eimagey,Eimagez ,xr,tolinit,plan2f,plan2b,nstop
     $           ,infostr)
         elseif (beam(1:7).eq.'speckle') then
            call speckle(k0,xgaus,ygaus,zgaus,pp ,E0 ,xswf ,yswf
     $           ,zswf ,xr,aretecube,nxmpp,nympp,nzmpp ,nmaxpp,nmax
     $           ,nfft2d ,nfft2d ,Eimagex,Eimagey,Eimagez,speckseed
     $           ,P0 ,irra,numaperil ,nstop ,infostr ,plan2b)
         elseif (beam(1:11).eq.'demispeckle') then
            call demispeckle(k0,xgaus,ygaus,zgaus,pp ,E0 ,xswf ,yswf
     $           ,zswf ,xr,aretecube,nxmpp,nympp,nzmpp ,nmaxpp,nmax
     $           ,nfft2d ,nfft2d ,Eimagex,Eimagey,Eimagez,speckseed
     $           ,P0 ,irra,numaperil ,nstop ,infostr ,plan2b)
         elseif (beam(1:8).eq.'confocal') then
            call confocal(k0,xgaus,ygaus,zgaus,pp ,E0 ,xswf ,yswf
     $           ,zswf ,xr,aretecube,nxmpp,nympp,nzmpp ,nmaxpp,nmax
     $           ,nfft2d ,nfft2d ,Eimagex,Eimagey,Eimagez,P0 ,irra
     $           ,numaperil ,nstop ,infostr ,plan2b)
         elseif (beam(1:12).eq.'demiconfocal') then
            call demiconfocal(k0,xgaus,ygaus,zgaus,pp ,E0 ,xswf ,yswf
     $           ,zswf ,xr,aretecube,nxmpp,nympp,nzmpp ,nmaxpp,nmax
     $           ,nfft2d ,nfft2d ,Eimagex,Eimagey,Eimagez,P0 ,irra
     $           ,numaperil ,nstop ,infostr ,plan2b)
         endif
         subunit=0
         cntwf=0
         do i=1,nzmpp
            do j=1,nympp
               do k=1,nxmpp
                  cntwf = cntwf + 1
                  x=xmin+dble(k-nxmp-1)*aretecube
                  y=ymin+dble(j-nymp-1)*aretecube
                  z=zmin+dble(i-nzmp-1)*aretecube
                  zswf(cntwf) = z
                  yswf(cntwf) = y
                  xswf(cntwf) = x
                  
                  if (j.eq.1.and.k.eq.1.and.nmat.eq.0) write(69,*) z
                  if (i.eq.1.and.k.eq.1.and.nmat.eq.0) write(68,*) y
                  if (j.eq.1.and.i.eq.1.and.nmat.eq.0) write(67,*) x
                  
                  subunit=subunit+1              
                  nsubunit=3*(subunit-1)
                  
                  if (comparaison(x,y,z,xs(l),ys(l),zs(l)
     $                 ,lambda).eq.1.and.l.le.nbsphere)  then
                     ll=3*(l-1)        
c     signe - pour compenser le signe moins dans la routibe AX: E=E0-(-Ap)
                     xi(nsubunit+1)=-FF(ll+1)
                     xi(nsubunit+2)=-FF(ll+2)
                     xi(nsubunit+3)=-FF(ll+3)    
                     do ii=1,3
                        do jj=1,3
                           polarisa(subunit,ii,jj)=epsilon(l,ii,jj)
                        enddo
                     enddo
                     l=l+1
                  endif
                  
               enddo
            enddo
         enddo

      else
         
         do i=1,nzmpp
            do j=1,nympp
               do k=1,nxmpp
                  cntwf = cntwf + 1
                  x=xmin+dble(k-nxmp-1)*aretecube
                  y=ymin+dble(j-nymp-1)*aretecube
                  z=zmin+dble(i-nzmp-1)*aretecube
                  zswf(cntwf) = z
                  yswf(cntwf) = y
                  xswf(cntwf) = x

                  if (j.eq.1.and.k.eq.1.and.nmat.eq.0) write(69,*) z
                  if (i.eq.1.and.k.eq.1.and.nmat.eq.0) write(68,*) y
                  if (j.eq.1.and.i.eq.1.and.nmat.eq.0) write(67,*) x
                  
                  subunit=subunit+1              
                  nsubunit=3*(subunit-1)
                  if (comparaison(x,y,z,xs(l),ys(l),zs(l)
     $                 ,lambda).eq.1.and.l.le.nbsphere)  then
                     ll=3*(l-1)        
c     signe - pour compenser le signe moins dans la routibe AX: E=E0-(-Ap)
                     xi(nsubunit+1)=-FF(ll+1)
                     xi(nsubunit+2)=-FF(ll+2)
                     xi(nsubunit+3)=-FF(ll+3)
                     xr(nsubunit+1)=FF0(ll+1)
                     xr(nsubunit+2)=FF0(ll+2)
                     xr(nsubunit+3)=FF0(ll+3)
                     do ii=1,3
                        do jj=1,3
                           polarisa(subunit,ii,jj)=epsilon(l,ii,jj)
                        enddo
                     enddo
                     l=l+1
                  else
c     comptute the incident field not computed yet
                     if (beam(1:11).eq.'pwavelinear') then
                        call ondeplane(x,y,z,k0,E0,ss,pp,theta,phi
     $                       ,xr(nsubunit+1),xr(nsubunit+2)
     $                       ,xr(nsubunit +3),nstop,infostr)
                     elseif (beam(1:13).eq.'pwavecircular') then
                        call ondecirce(x,y,z,k0,E0,ss,theta,phi
     $                       ,xr(nsubunit+1),xr(nsubunit+2)
     $                       ,xr(nsubunit +3))
                     elseif (beam(1:15).eq.'wavelinearmulti') then
                        call ondeplanemulti(x,y,z,k0,E0m,ssm,ppm
     $                       ,thetam,phim,nbinc,xr(nsubunit+1)
     $                       ,xr(nsubunit+2),xr(nsubunit+3),nstop
     $                       ,infostr)
                     elseif (beam(1:7).eq.'antenna') then
                        call dipoleinc(xdip,ydip,zdip,theta,phi,x ,y
     $                       ,z,aretecube,k0,E0,xr(nsubunit+1)
     $                       ,xr(nsubunit+2),xr(nsubunit +3),nstop
     $                       ,infostr)
                     elseif (beam(1:11).eq.'gwavelinear') then
                        call gaussianchamp(x,y,z,xgaus,ygaus,zgaus
     $                       ,theta,phi,w0,k0,ss,pp,E0,xr(nsubunit+1)
     $                       ,xr(nsubunit+2),xr(nsubunit+3),tol,nloin
     $                       ,nstop,infostr)
                     elseif (beam(1:13).eq.'gwavecircular') then
                        call gaussianchampcirc(x,y,z,xgaus,ygaus
     $                       ,zgaus,theta,phi,w0,k0,ss,E0,xr(nsubunit
     $                       +1),xr(nsubunit+2),xr(nsubunit+3),tol
     $                       ,nloin,nstop,infostr)
                     elseif (beam(1:15).eq.'gparawavelinear') then
                        call gaussianparalinear(x,y,z,xgaus,ygaus
     $                       ,zgaus,theta,phi,w0,k0,ss,pp,E0
     $                       ,xr(nsubunit+1),xr(nsubunit+2)
     $                       ,xr(nsubunit+3),nstop,infostr)
                     elseif (beam(1:17).eq.'gparawavecircular') then
                        call gaussianparacirc(x,y,z,xgaus,ygaus,zgaus
     $                       ,theta,phi,w0,k0,ss,E0,xr(nsubunit+1)
     $                       ,xr(nsubunit+2),xr(nsubunit+3),nstop
     $                       ,infostr)
                     elseif (beam(1:13).eq.'arbitrary') then
                        call incidentarbitrarypos(x,y,z,aretecube
     $                       ,xr(nsubunit+1),xr(nsubunit+2)
     $                       ,xr(nsubunit +3),nstop,namefileinc
     $                       ,infostr)
                     endif
                  endif
               enddo
            enddo
         enddo
      endif
c     Close Far field discretization
      close(67)
      close(68)
      close(69)         
      
c     save the incident field for wide field
      if (nmat.eq.0) then
         do i=1,subunit
            incidentfieldx(i) = xr(3*i-2)
            incidentfieldy(i) = xr(3*i-1)
            incidentfieldz(i) = xr(3*i)
            incidentfield(i) = dsqrt(dreal(xr(3*i-2)*dconjg(xr(3*i
     $           -2))+xr(3*i-1)*dconjg(xr(3*i-1))+xr(3*i)*dconjg(xr(3
     $           *i))))
            write(136,*) dreal(xr(3*i-2)),dimag(xr(3*i-2))
            write(137,*) dreal(xr(3*i-1)),dimag(xr(3*i-1))
            write(138,*) dreal(xr(3*i)),dimag(xr(3*i))     
            write(139,*) incidentfield(i)
         enddo
      else
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)   
!$OMP DO SCHEDULE(STATIC)            
         do i=1,subunit
            incidentfieldx(i) = xr(3*i-2)
            incidentfieldy(i) = xr(3*i-1)
            incidentfieldz(i) = xr(3*i)
            incidentfield(i) = dsqrt(dreal(xr(3*i-2)*dconjg(xr(3*i
     $           -2))+xr(3*i-1)*dconjg(xr(3*i-1))+xr(3*i)*dconjg(xr(3
     $           *i))))          
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         if (nmat.eq.2) then
            dim(1)=subunit
            dim(2)=nmax
            datasetname='Incident field modulus wf'
            call hdf5write1d(group_idnf,datasetname,incidentfield,
     $           dim)
            datasetname='Incident field x component real part wf'
            call hdf5write1d(group_idnf,datasetname
     $           ,dreal(incidentfieldx),dim)
            datasetname
     $           ='Incident field x component imaginary part wf'
            call hdf5write1d(group_idnf,datasetname
     $           ,dimag(incidentfieldx),dim)
            datasetname='Incident field y component real part wf'
            call hdf5write1d(group_idnf,datasetname
     $           ,dreal(incidentfieldy),dim)
            datasetname
     $           ='Incident field y component imaginary part wf'
            call hdf5write1d(group_idnf,datasetname
     $           ,dimag(incidentfieldy),dim)
            datasetname='Incident field z component real part wf'
            call hdf5write1d(group_idnf,datasetname
     $           ,dreal(incidentfieldz),dim)
            datasetname
     $           ='Incident field z component imaginary part wf'
            call hdf5write1d(group_idnf,datasetname
     $           ,dimag(incidentfieldz),dim)
         endif
         
      endif
c     Close Intensity of the incident field wide field
      close(136)
      close(137)
      close(138)
      close(139)         
c     calcul du champ la position local par le produit matrice vecteur
c     FFT:

      call produitfftmatvectopt(FFTTENSORxx,FFTTENSORxy,FFTTENSORxz
     $     ,FFTTENSORyy,FFTTENSORyz,FFTTENSORzz,vectx,vecty,vectz
     $     ,ntotalm,nmaxpp*8,nmaxpp,nxm,nym,nzm,nxmpp,nympp,nzmpp
     $     ,nxm2 ,nym2 ,nxym2,nzm2,xi,xr,planfn,planbn)

c     remet la valeur du champ calculé précédemment dans l'objet quand
c     on n'est pas en rigoureux.
      if (nrig.ne.0) then
         subunit=0
         l=1
         do i=1,nzmpp
            do j=1,nympp
               do k=1,nxmpp
                  x=xmin+dble(k-nzmp-1)*aretecube
                  y=ymin+dble(j-nymp-1)*aretecube
                  z=zmin+dble(i-nzmp-1)*aretecube
                  subunit=subunit+1              
                  nsubunit=3*(subunit-1)
                  if (comparaison(x,y,z,xs(l),ys(l),zs(l)
     $                 ,lambda).eq.1.and.l.le.nbsphere)  then
                     ll=3*(l-1)
                     tmp=cdabs(epsilon(l,1,1)-1.d0)
     $                    +cdabs(epsilon(l,2,2)-1.d0)+cdabs(epsilon(l
     $                    ,3,3)-1.d0)+cdabs(epsilon(l,1,2))
     $                    +cdabs(epsilon(l,1,3))+cdabs(epsilon(l,2
     $                    ,1))+cdabs(epsilon(l,2,3))+cdabs(epsilon(l
     $                    ,3,1))+cdabs(epsilon(l,3,2))

                     if (tmp.ge.1.d-8) then
                        xr(nsubunit+1)=FFloc(ll+1)
                        xr(nsubunit+2)=FFloc(ll+2)
                        xr(nsubunit+3)=FFloc(ll+3)
                     endif
                     l=l+1
                  endif
               enddo
            enddo
         enddo
         
      endif


      end
