      subroutine diffractefft2dinckzneg(speckseed,nfft2d,k0,E0,ss,pp
     $     ,theta,phi, thetam,phim, ppm, ssm,E0m,nbinc,xdip,ydip,zdip
     $     ,xgaus,ygaus,zgaus,numaperil,w0,aretecube,tol,Eloinx,Eloiny
     $     ,Eloinz ,imax,deltakx ,deltaky,Ediffkzpos,numaper,beam,plan2f
     $     ,plan2b ,nstop,infostr)
      implicit none
      integer nfft2d,nstop,speckseed
      double precision k0,numaper,aretecube,numaperil
      double complex Ediffkzpos(nfft2d,nfft2d,3)


      integer nfft2d2,imax,i,j,k,indice,ii,jj
      double precision deltakx,deltaky,deltax,deltay,kx,ky,kz,fac,pi
      double complex icomp,Eloinx(nfft2d*nfft2d) ,Eloiny(nfft2d *nfft2d)
     $     ,Eloinz(nfft2d*nfft2d),Ex,Ey,Ez
      character(64) infostr

c     variable pour champ incident
      integer nloin,ikxinc,jkyinc,iref
      double precision ss,pp,theta,phi,xdip ,ydip,zdip,xgaus,ygaus
     $     ,zgaus,w0,x,y,z,tol,tolc,Emod,Emodmax,kxinc,kyinc,const1,kmax
     $     ,kp2,psi,ran,sunif
      double complex E0,Em(3),Axp,Ays,Azp,Azs,const2
      character(64) beam
      integer nbinc
      double precision thetam(10), phim(10), ppm(10), ssm(10)
      double complex E0m(10)
      integer indicex,indicey
      integer*8 plan2f,plan2b
      integer FFTW_FORWARD
      
      pi=dacos(-1.d0)
      icomp=(0.d0,1.d0)
      kmax=k0*numaperil
      
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,ii,jj)
!$OMP DO  SCHEDULE(STATIC) COLLAPSE(2)      
      do i=-imax,imax
         do j=-imax,imax
            ii=imax+i+1
            jj=imax+j+1
            Ediffkzpos(ii,jj,1)=0.d0
            Ediffkzpos(ii,jj,2)=0.d0
            Ediffkzpos(ii,jj,3)=0.d0
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
 
      if (beam(1:7).eq.'speckle') then
         psi=pp*pi/180.d0
         iref=4*speckseed+5
         ran=SUNIF(iref)
         do i=-imax,imax
            do j=-imax,imax
               kx=dble(i)*deltakx
               ky=dble(j)*deltaky               
               ii=imax+i+1
               jj=imax+j+1
               kp2=kx*kx+ky*ky
               if (kp2.le.kmax*kmax*0.999d0) then
                  
                  ran=SUNIF(-1)
                  kz=-dsqrt(k0*k0-kp2)
                  const2=cdexp(-icomp*(kx*xgaus+ky*ygaus+kz*zgaus))
     $                 *cdexp(icomp*2.d0*pi*ran)
c     polarisation p
                  const1=1.d0/dsqrt(kz*kz+kx*kx)
                  Axp=kz*const1              
                  Azp=-kx*const1
c     polarisation s
                  const1=1.d0/dsqrt(kz*kz+ky*ky)
                  Ays=kz*const1
                  Azs=-ky*const1 
c     composition des deux polarisations   
                  Ediffkzpos(ii,jj,1)=-E0*dcos(psi)*Axp*const2
                  Ediffkzpos(ii,jj,2)=-E0*dsin(psi)*Ays*const2
                  Ediffkzpos(ii,jj,3)=-E0*(dcos(psi)*Azp+dsin(psi)*Azs)
     $                 *const2
               endif
            enddo
         enddo
      elseif (beam(1:8).eq.'confocal') then
         psi=pp*pi/180.d0
         do i=-imax,imax
            do j=-imax,imax
               kx=dble(i)*deltakx
               ky=dble(j)*deltaky               
               ii=imax+i+1
               jj=imax+j+1
               kp2=kx*kx+ky*ky
               if (kp2.le.kmax*kmax*0.999d0) then            
                  kz=-dsqrt(k0*k0-kp2)
                   const2=cdexp(-icomp*(kx*xgaus+ky*ygaus+kz*zgaus))
c     polarisation p
                  const1=1.d0/dsqrt(kz*kz+kx*kx)
                  Axp=kz*const1              
                  Azp=-kx*const1
c     polarisation s
                  const1=1.d0/dsqrt(kz*kz+ky*ky)
                  Ays=kz*const1
                  Azs=-ky*const1 
c     composition des deux polarisations      
                  Ediffkzpos(ii,jj,1)=-E0*dcos(psi)*Axp*const2
                  Ediffkzpos(ii,jj,2)=-E0*dsin(psi)*Ays*const2
                  Ediffkzpos(ii,jj,3)=-E0*(dcos(psi)*Azp+dsin(psi)*Azs)
     $                 *const2
               endif
            enddo
         enddo
      endif
      return
      end
