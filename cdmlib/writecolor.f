c$$$      implicit none
c$$$
c$$$      character*100 data
c$$$      character*12 color
c$$$
c$$$
c$$$      data='toto'
c$$$      color='red'
c$$$      call  writecolor(data,color)
c$$$
c$$$      end

      subroutine writecolor(data,color)
      implicit none

      character*100 data
      character*12 color
      
      integer i
      character*1 val1
      character*2 val2
      character*3 val3

c     1 boldface
c     3 italic
c     4 underline
c     5 blink
c     7 bgblack
c     9 cross
c     31 reddark
c     32 greendark
c     33 brown
c     34 bluedark
c     35 violet
c     36 cyandark
c     41 bgreddark
c     42 bggreendark
c     43 bgbrown
c     44 bgbluedark
c     45 bgviolet
c     46 bgcyandark
c     91 red
c     92 green      
c     93 yellow
c     94 blue
c     95 pink
c     96 cyan
c     100 bggray
c     101 bgred
c     102 bggreen
c     103 bgyellow
c     104 bgblue
c     105 bgpink
c     106 bgcyan      

      if (color(1:8).eq.'boldface') then
         write(*,*) achar(27)//'[1m'//data//achar(27)//'[0m'
      endif
      if (color(1:6).eq.'italic') then
         write(*,*) achar(27)//'[3m'//data//achar(27)//'[0m'
      endif
      if (color(1:9).eq.'underline') then
         write(*,*) achar(27)//'[4m'//data//achar(27)//'[0m'
      endif
      if (color(1:5).eq.'blink') then
         write(*,*) achar(27)//'[5m'//data//achar(27)//'[0m'
      endif
      if (color(1:7).eq.'bgblack') then
         write(*,*) achar(27)//'[7m'//data//achar(27)//'[0m'
      endif
      if (color(1:5).eq.'cross') then
         write(*,*) achar(27)//'[9m'//data//achar(27)//'[0m'
      endif
      if (color(1:7).eq.'reddark') then
         write(*,*) achar(27)//'[31m'//data//achar(27)//'[0m'
      endif
      if (color(1:9).eq.'greendark') then
         write(*,*) achar(27)//'[32m'//data//achar(27)//'[0m'
      endif
      if (color(1:5).eq.'brown') then
         write(*,*) achar(27)//'[33m'//data//achar(27)//'[0m'
      endif
      if (color(1:8).eq.'bluedark') then
         write(*,*) achar(27)//'[34m'//data//achar(27)//'[0m'
      endif
      if (color(1:6).eq.'violet') then
         write(*,*) achar(27)//'[35m'//data//achar(27)//'[0m'
      endif
      if (color(1:8).eq.'cyandark') then
         write(*,*) achar(27)//'[36m'//data//achar(27)//'[0m'
      endif
      if (color(1:9).eq.'bgreddark') then
         write(*,*) achar(27)//'[41m'//data//achar(27)//'[0m'
      endif
      if (color(1:11).eq.'bggreendark') then
         write(*,*) achar(27)//'[42m'//data//achar(27)//'[0m'
      endif
      if (color(1:7).eq.'bgbrown') then
         write(*,*) achar(27)//'[43m'//data//achar(27)//'[0m'
      endif
      if (color(1:10).eq.'bgbluedark') then
         write(*,*) achar(27)//'[44m'//data//achar(27)//'[0m'
      endif
      if (color(1:8).eq.'bgviolet') then
         write(*,*) achar(27)//'[45m'//data//achar(27)//'[0m'
      endif
      if (color(1:10).eq.'bgcyandark') then
         write(*,*) achar(27)//'[46m'//data//achar(27)//'[0m'
      endif
      if (color(1:3).eq.'red') then
         write(*,*) achar(27)//'[91m'//data//achar(27)//'[0m'
      endif
      if (color(1:5).eq.'green') then
         write(*,*) achar(27)//'[92m'//data//achar(27)//'[0m'
      endif
      if (color(1:6).eq.'yellow') then
         write(*,*) achar(27)//'[93m'//data//achar(27)//'[0m'
      endif
      if (color(1:4).eq.'blue') then
         write(*,*) achar(27)//'[94m'//data//achar(27)//'[0m'
      endif
      if (color(1:4).eq.'pink') then
         write(*,*) achar(27)//'[95m'//data//achar(27)//'[0m'
      endif
      if (color(1:4).eq.'cyan') then
         write(*,*) achar(27)//'[96m'//data//achar(27)//'[0m'
      endif
      if (color(1:6).eq.'bggray') then
         write(*,*) achar(27)//'[100m'//data//achar(27)//'[0m'
      endif
      if (color(1:5).eq.'bgred') then
         write(*,*) achar(27)//'[101m'//data//achar(27)//'[0m'
      endif
      if (color(1:7).eq.'bggreen') then
         write(*,*) achar(27)//'[102m'//data//achar(27)//'[0m'
      endif
      if (color(1:8).eq.'bgyellow') then
         write(*,*) achar(27)//'[103m'//data//achar(27)//'[0m'
      endif
      if (color(1:6).eq.'bgblue') then
         write(*,*) achar(27)//'[104m'//data//achar(27)//'[0m'
      endif
      if (color(1:6).eq.'bgpink') then
         write(*,*) achar(27)//'[105m'//data//achar(27)//'[0m'
      endif
      if (color(1:6).eq.'bgcyan') then
         write(*,*) achar(27)//'[106m'//data//achar(27)//'[0m'
      endif
      
      return
c$$$      stop
c$$$      do i=1,9
c$$$         call integer_string1(i,val1)
c$$$         write(*,*) 'A great color is '//achar(27)//'['//val1//'m pink '
c$$$     $        //achar(27)//'[0m','Number',i
c$$$      enddo
c$$$
c$$$      do i=10,99
c$$$         call integer_string2(i,val2)
c$$$         write(*,*) 'A great color is '//achar(27)//'['//val2//'m pink '
c$$$     $        //achar(27)//'[0m','Number',i
c$$$      enddo
c$$$      do i=100,999
c$$$         call integer_string3(i,val3)
c$$$         write(*,*) 'A great color is '//achar(27)//'['//val3//'m pink '
c$$$     $        //achar(27)//'[0m','Number',i
c$$$      enddo
      
      
      end
