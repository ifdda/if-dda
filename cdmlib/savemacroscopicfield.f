      subroutine  savemacroscopicfield(nmat,ndipole,nmax,testgreen
     $     ,tabdip,aretecube,k0,FFloc,epsilon,macroscopicfield
     $     ,macroscopicfieldx,macroscopicfieldy ,macroscopicfieldz,wrk
     $     ,group_idnf)

#ifdef USE_HDF5
      use HDF5
#endif
      
      integer nmat,ndipole,nmax,testgreen,tabdip(nmax)
      double precision aretecube,k0,macroscopicfield(nmax)
      double complex FFloc(3*nmax),epsilon(nmax,3,3)
     $     ,macroscopicfieldx(nmax),macroscopicfieldy(nmax)
     $     ,macroscopicfieldz(nmax),wrk(3*nmax,12)
#ifndef USE_HDF5
      integer,parameter:: hid_t=4
#endif
      integer(hid_t) :: file_id
      integer(hid_t) :: group_idnf
      integer :: dim(4)
      
      integer nsens,nm,nx,ny,nz
      character(LEN=100) :: datasetname
      double complex Em(3),Eloc(3),epsani(3,3)
      
      nsens=1
      if (nmat.eq.0.and.testgreen.eq.0) then
         do i=1,ndipole
            k=tabdip(i)
            if (k.ne.0) then
               ii=3*(k-1)
               Eloc(1)= FFloc(ii+1)
               Eloc(2)= FFloc(ii+2)
               Eloc(3)= FFloc(ii+3)
               do ii=1,3
                  do jj=1,3
                     epsani(ii,jj)=epsilon(k,ii,jj)
                  enddo
               enddo 
               
               call local_macro(Eloc,Em,epsani,aretecube,k0,nsens)
               macroscopicfieldx(k)=Em(1)
               macroscopicfieldy(k)=Em(2)
               macroscopicfieldz(k)=Em(3)
               write(44,*) dreal(Em(1)),dimag(Em(1))
               write(45,*) dreal(Em(2)),dimag(Em(2))
               write(46,*) dreal(Em(3)),dimag(Em(3))
               macroscopicfield(k)= dsqrt(dreal(Em(1)
     $              *dconjg(Em(1))+Em(2)*dconjg(Em(2))+Em(3)
     $              *dconjg(Em(3))))
               write(47,*) macroscopicfield(k)
            else
               write(44,*) 0.d0,0.d0
               write(45,*) 0.d0,0.d0
               write(46,*) 0.d0,0.d0
               write(47,*) 0.d0
            endif
         enddo      
      elseif (nmat.eq.0.and.testgreen.gt.1) then

         if (testgreen.eq.2) then
            nx=70
            ny=71
            nz=72
         elseif (testgreen.eq.3) then
            nx=73
            ny=74
            nz=75
         elseif (testgreen.eq.4) then
            nx=76
            ny=77
            nz=78
         endif
         
         do i=1,ndipole
            k=tabdip(i)
            if (k.ne.0) then
               ii=3*(k-1)
               Eloc(1)= FFloc(ii+1)
               Eloc(2)= FFloc(ii+2)
               Eloc(3)= FFloc(ii+3)
               do ii=1,3
                  do jj=1,3
                     epsani(ii,jj)=epsilon(k,ii,jj)
                  enddo
               enddo 
               
               call local_macro(Eloc,Em,epsani,aretecube,k0,nsens)
               macroscopicfieldx(k)=Em(1)
               macroscopicfieldy(k)=Em(2)
               macroscopicfieldz(k)=Em(3)
               write(nx,*) dreal(Em(1)),dimag(Em(1))
               write(ny,*) dreal(Em(2)),dimag(Em(2))
               write(nz,*) dreal(Em(3)),dimag(Em(3))
            else
               write(nx,*) 0.d0,0.d0
               write(ny,*) 0.d0,0.d0
               write(nz,*) 0.d0,0.d0
            endif
         enddo
      elseif (nmat.ne.0) then
         nsens=1
!     $OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,ii,jj,Eloc,epsani,Em)   
!     $OMP DO SCHEDULE(STATIC)
         do i=1,ndipole
            k=tabdip(i)
            if (k.ne.0) then
               ii=3*(k-1)
               Eloc(1)= FFloc(ii+1)
               Eloc(2)= FFloc(ii+2)
               Eloc(3)= FFloc(ii+3)
               do ii=1,3
                  do jj=1,3
                     epsani(ii,jj)=epsilon(k,ii,jj)
                  enddo
               enddo                      
               call local_macro(Eloc,Em,epsani,aretecube,k0,nsens)
               macroscopicfieldx(k)=Em(1)
               macroscopicfieldy(k)=Em(2)
               macroscopicfieldz(k)=Em(3)
               macroscopicfield(k)= dsqrt(dreal(Em(1)
     $              *dconjg(Em(1))+Em(2)*dconjg(Em(2))+Em(3)
     $              *dconjg(Em(3))))
            endif
         enddo
!     $OMP ENDDO 
!     $OMP END PARALLEL
         if (nmat.eq.2) then
!     $OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k)   
!     $OMP DO SCHEDULE(STATIC)
            do i=1,ndipole
               k=tabdip(i)
               if (k.ne.0) then
                  wrk(i,1)=macroscopicfieldx(k)
                  wrk(i,2)=macroscopicfieldy(k)
                  wrk(i,3)=macroscopicfieldz(k)
                  wrk(i,4)=dsqrt(dreal(macroscopicfieldx(k)
     $                 *dconjg(macroscopicfieldx(k))
     $                 +macroscopicfieldy(k)
     $                 *dconjg(macroscopicfieldy(k))
     $                 +macroscopicfieldz(k)
     $                 *dconjg(macroscopicfieldz(k))))
               else
                  wrk(i,1)=0.d0
                  wrk(i,2)=0.d0
                  wrk(i,3)=0.d0
                  wrk(i,4)=0.d0
               endif
            enddo
!     $OMP ENDDO 
!     $OMP END PARALLEL    

            dim(1)=ndipole
            dim(2)=nmax*3
            if (testgreen.eq.0) then
               datasetname='Macroscopic field modulus'
               call hdf5write1d(group_idnf,datasetname,dreal(wrk(:,4))
     $              ,dim)
               datasetname='Macroscopic field x component real part'
               call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,1))
     $              ,dim)
               datasetname
     $              ='Macroscopic field x component imaginary part'
               call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,1))
     $              ,dim)
               datasetname='Macroscopic field y component real part'
               call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,2))
     $              ,dim)
               datasetname
     $              ='Macroscopic field y component imaginary part'
               call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,2))
     $              ,dim)
               datasetname='Macroscopic field z component real part'
               call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,3))
     $              ,dim)
               datasetname
     $              ='Macroscopic field z component imaginary part'
               call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,3))
     $              ,dim)
            elseif (testgreen.eq.2) then
               datasetname='Green tensor xx component real part'
               call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,1))
     $              ,dim)
               datasetname='Green tensor xx component imaginary part'
               call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,1))
     $              ,dim)
               datasetname='Green tensor yx component real part'
               call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,2))
     $              ,dim)
               datasetname ='Green tensor yx component imaginary part'
               call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,2))
     $              ,dim)
               datasetname='Green tensor zx component real part'
               call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,3))
     $              ,dim)
               datasetname='Green tensor zx component imaginary part'
               call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,3))
     $              ,dim)
            elseif (testgreen.eq.3) then
               datasetname='Green tensor xy component real part'
               call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,1))
     $              ,dim)
               datasetname='Green tensor xy component imaginary part'
               call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,1))
     $              ,dim)
               datasetname='Green tensor yy component real part'
               call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,2))
     $              ,dim)
               datasetname='Green tensor yy component imaginary part'
               call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,2))
     $              ,dim)
               datasetname='Green tensor zy component real part'
               call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,3))
     $              ,dim)
               datasetname='Green tensor zy component imaginary part'
               call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,3))
     $              ,dim)
            elseif (testgreen.eq.4) then
               datasetname='Green tensor xz component real part'
               call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,1))
     $              ,dim)
               datasetname='Green tensor xz component imaginary part'
               call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,1))
     $              ,dim)
               datasetname='Green tensor yz component real part'
               call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,2))
     $              ,dim)
               datasetname='Green tensor yz component imaginary part'
               call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,2))
     $              ,dim)
               datasetname='Green tensor zz component real part'
               call hdf5write1d(group_idnf,datasetname,dreal(Wrk(:,3))
     $              ,dim)
               datasetname='Green tensor zz component imaginary part'
               call hdf5write1d(group_idnf,datasetname,dimag(Wrk(:,3))
     $              ,dim)
            endif
            
         endif       
      endif

      if (testgreen.eq.4.or.testgreen.eq.0) then
         close(44)
         close(45)
         close(46)
         close(47)         
         close(70)
         close(71)
         close(72)
         close(73)
         close(74)
         close(75)
         close(76)
         close(77)
         close(78)
      endif
    

  
      end
