c     cette routine multiplie un vecteur par la matrice inverse estimée
c     de la matrice toeplitz initialie.

      subroutine preconditionneurscalopt(FF,xi,xr,polarisa ,chanxxvect
     $     ,uincx,uincy,uincz,nx,ny,nz ,nbsphere,nxm ,nym,nzm,aretecube
     $     ,k0,ntest,sdetnn,ipvtnn ,vectxx ,plan2b ,plan2f ,nrig,infostr
     $     ,nstop)
      implicit none
      integer nx,ny,nz,nbsphere,nstop,nxm,nym,nzm,ntest,nrig
      double precision x,y,z,x0,y0,z0,aretecube,k0,test,tmp,tmp1,tmp2,r
      double complex FF(3*nxm*nym*nzm),polarisa(nxm*nym*nzm,3,3),xi(3
     $     *nxm*nym*nzm),xr(3*nxm*nym*nzm),propaesplibre(3,3),uincx
     $     ,uincy,uincz,icomp

      character(64) infostr

      integer i,j,k,ii,jj,kk,ix,iy,iz,i2,j2,k2,kkk,l,ll,k1,j1
      double precision dn1x,dn2x,dn1y,dn2y
      double complex polamoy,dn11c,dn12c,dn21c,dn22c,Gn1n1,Gn1n2,Gn2n1
     $     ,Gn2n2
      double complex chanxxvect(nxm*nym*nzm)

      double complex sdetnn(nzm,nzm ,nxm*nym)

      integer n1,n2,n3
      double precision dnxy,normefrobenius

      integer job,lda,info,lwork,NRHS
      double complex  deterz(2)
      
      integer nmaxcompo
      double complex vectxx(nx*ny)
      double complex , dimension (:,:), allocatable :: sdettmp
      double complex , dimension (:), allocatable :: chanxx,worktmp
     $     ,work
      integer , dimension (:),allocatable ::ipvttmp
      integer jx,jy,ipvtnn(nz,nx*ny)
      double complex , dimension (:,:), allocatable :: spola

      integer FFTW_FORWARD,FFTW_ESTIMATE,FFTW_BACKWARD
      integer*8 plan2b,plan2f
      integer valuesf(8),valuesi(8)
      double precision tf,ti
      character(64) message
      character(8)  :: date
      character(10) :: time
      character(5)  :: zone
      
      FFTW_FORWARD=-1
      FFTW_BACKWARD=+1
      FFTW_ESTIMATE=64

      if (ntest.eq.0) then
         icomp=(0.d0,1.d0)
c     ****************************************
c     calcul polarisabilité moyenne
c     ****************************************
c         message='Chan'
c         call cpu_time(ti)
c         call date_and_time(date,time,zone,valuesi)
         polamoy=0.d0
         k=0

         do i=1,nbsphere
            polamoy=polamoy+polarisa(i,2,2)
            if (cdabs(polarisa(i,2,2)).ne.0) k=k+1
         enddo

         polamoy=polamoy/dble(k)

         normefrobenius=0.d0
         tmp=0.d0
         tmp1=0.d0
         tmp2=0.d0


         dnxy=dble(nx*ny)

c     Il faut effectuer une TF de longueur nx sur ny*nz, cad ny*nz FFT
c     de longueur nx
         x0=0.d0
         y0=0.d0
         z0=0.d0
c     boucle sur ny et nz
         nmaxcompo=nx*ny
       
         allocate(chanxx(1:nmaxcompo))       
         allocate(spola(nx,ny))

         do i=1,nz
c     calcul de spola
c$$$            kk=nx*ny*(i-1)
c$$$            do ix=1,nx
c$$$               do iy=1,ny
c$$$                  spola(ix,iy)=0.d0
c$$$                  do jx=1,nx-ix+1
c$$$                     do jy=1,ny-iy+1
c$$$                        spola(ix,iy)=spola(ix,iy)+(polarisa(jx+nx *(jy-1
c$$$     $                       )+kk,2,2))
c$$$                     enddo
c$$$                  enddo
c$$$                  spola(ix,iy)=spola(ix,iy)/dnxy
c$$$               enddo
c$$$            enddo

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,k1,j1,x,y,z,propaesplibre)
!$OMP& PRIVATE(dn11c,dn21c,dn12c,dn22c,dn1x,dn2x,dn1y,dn2y)
!$OMP& PRIVATE(Gn2n2,Gn1n2,Gn2n1,Gn1n1,r)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
            do j=1,ny
               do k=1,nx    
                  dn1x=dble(k-1)
                  dn2x=dble(nx-k+1)
                  dn1y=dble(j-1) 
                  dn2y=dble(ny-j+1)
c                  dn11c=spola(1,1)+spola(k,j)-spola(1,j)-spola(k,1)
c                  dn12c=spola(1,j)-spola(k,j)
c                  dn21c=spola(k,1)-spola(k,j)
c                  dn22c=spola(k,j)
c                  write(*,*) '***********',k,j
c                  write(*,*) 'dn2x*dn2y',dn2x*dn2y/dnxy,spola(k,j)
c     $                 /polamoy
c                  write(*,*) 'dn1x*dn1y',dn1x*dn1y/dnxy,(spola(1,1)
c     $                 +spola(k,j)-spola(1,j)-spola(k,1))/polamoy
c                  write(*,*) 'dn1x*dn2y',dn1x*dn2y/dnxy,(spola(1,j)
c     $                 -spola(k,j))/polamoy
c                  write(*,*) 'dn2x*dn1y',dn2x*dn1y/dnxy,(spola(k,1)
c     $                 -spola(k,j))/polamoy

                  k1=nx-k+2
                  j1=ny-j+2
                  if (k.eq.1) then
                     k1=1
                  endif
                  if (j.eq.1) then
                     j1=1
                  endif

                  x=aretecube*dble(k-1)
                  y=aretecube*dble(j-1)
                  z=aretecube*dble(i-1)

                  if (nrig.eq.8) then                  
                     call propa_espace_libre(x,y,z,x0,y0,z0,k0
     $                    ,propaesplibre)
                     Gn2n2=dconjg(Uincx)*(propaesplibre(1,1) *Uincx
     $                    +propaesplibre(1,2)*Uincy+propaesplibre(1,3)
     $                    *Uincz)+dconjg(Uincy)*(propaesplibre(2,1)
     $                    *Uincx +propaesplibre(2,2)*Uincy
     $                    +propaesplibre(2,3) *Uincz)+dconjg(Uincz)
     $                    *(propaesplibre(3,1)*Uincx +propaesplibre(3,2)
     $                    *Uincy+propaesplibre(3,3) *Uincz)
                  elseif (nrig.eq.9) then
                     r=dsqrt(x*x+y*y+z*z)
                     Gn2n2=k0*k0*cdexp(icomp*k0*r)/r
                  endif
                  
                  x=aretecube*dble(k-1)
                  y=aretecube*dble(j1-1)
                  z=aretecube*dble(i-1)

                  if (nrig.eq.8) then 
                     call propa_espace_libre(x,y,z,x0,y0,z0,k0
     $                    ,propaesplibre)
                     Gn2n1=dconjg(Uincx)*(propaesplibre(1,1) *Uincx
     $                    +propaesplibre(1,2)*Uincy+propaesplibre(1,3)
     $                    *Uincz)+dconjg(Uincy)*(propaesplibre(2,1)
     $                    *Uincx +propaesplibre(2,2)*Uincy
     $                    +propaesplibre(2,3) *Uincz)+dconjg(Uincz)
     $                    *(propaesplibre(3,1)*Uincx +propaesplibre(3,2)
     $                    *Uincy+propaesplibre(3,3) *Uincz)
                  elseif (nrig.eq.9) then
                     r=dsqrt(x*x+y*y+z*z)
                     Gn2n1=k0*k0*cdexp(icomp*k0*r)/r
                  endif
                  
                  x=aretecube*dble(k1-1)
                  y=aretecube*dble(j-1)
                  z=aretecube*dble(i-1)
                  if (nrig.eq.8) then
                     call propa_espace_libre(x,y,z,x0,y0,z0,k0
     $                    ,propaesplibre)
                     Gn1n2=dconjg(Uincx)*(propaesplibre(1,1) *Uincx
     $                    +propaesplibre(1,2)*Uincy+propaesplibre(1,3)
     $                    *Uincz)+dconjg(Uincy)*(propaesplibre(2,1)
     $                    *Uincx +propaesplibre(2,2)*Uincy
     $                    +propaesplibre(2,3) *Uincz)+dconjg(Uincz)
     $                    *(propaesplibre(3,1)*Uincx +propaesplibre(3,2)
     $                    *Uincy+propaesplibre(3,3) *Uincz)
                  elseif (nrig.eq.9) then
                     r=dsqrt(x*x+y*y+z*z)
                     Gn1n2=k0*k0*cdexp(icomp*k0*r)/r
                  endif

                  x=aretecube*dble(k1-1)
                  y=aretecube*dble(j1-1)
                  z=aretecube*dble(i-1)
                  if (nrig.eq.8) then
                     call propa_espace_libre(x,y,z,x0,y0,z0,k0
     $                    ,propaesplibre)
                     Gn1n1=dconjg(Uincx)*(propaesplibre(1,1) *Uincx
     $                    +propaesplibre(1,2)*Uincy+propaesplibre(1,3)
     $                    *Uincz)+dconjg(Uincy)*(propaesplibre(2,1)
     $                    *Uincx +propaesplibre(2,2)*Uincy
     $                    +propaesplibre(2,3) *Uincz)+dconjg(Uincz)
     $                    *(propaesplibre(3,1)*Uincx +propaesplibre(3,2)
     $                    *Uincy+propaesplibre(3,3) *Uincz)
                  elseif (nrig.eq.9) then
                     r=dsqrt(x*x+y*y+z*z)
                     Gn1n1=k0*k0*cdexp(icomp*k0*r)/r
                  endif
                  
c                  chanxx(k1+nx*(j1-1))=-(dn11c*Gn1n1+dn12c*Gn1n2+dn21c
c     $                 *Gn2n1+dn22c*Gn2n2)
                  
                  chanxx(k1+nx*(j1-1))=-(dn1x*dn1y*Gn1n1+dn1x*dn2y*Gn1n2
     $                 +dn2x*dn1y*Gn2n1+dn2x*dn2y*Gn2n2)/dnxy*polamoy
c                  write(*,*) 'kj',k,j
c                  write(*,*) 'chan',chanxx(k1+nx*(j1-1)),k1+nx*(j1-1)
c                  write(*,*) 'G',Gn1n1,Gn1n2 ,Gn2n1,Gn2n2
c                  write(*,*) 'd',dn1x*dn1y,dn1x*dn2y,dn2x*dn1y,dn2x*dn2y
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL


            if (i.eq.1) then
               chanxx(1)=chanxx(1)+1.d0             
            endif
c            write(*,*)  chanxx(1),chanyy(1),chanzz(1),i

c               do k=1,nx
c                  do j=1,ny
c                     write(*,*) 'chanxx',chanxx(k+nx*(j-1)),k,j
c                  enddo
c               enddo
#ifdef USE_FFTW
            call dfftw_execute_dft(plan2b, chanxx, chanxx)             
#else 
            call fftsingletonz2d(chanxx,nx,ny,FFTW_BACKWARD)           
#endif
            kk=nx*ny*(i-1)
c     réécrit ce vecteur dans sa partie
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(j,k)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
            do j=1,ny
               do k=1,nx              
                  chanxxvect(j+ny*(k-1)+kk)=chanxx(k+nx*(j-1))                
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         enddo
   
         deallocate(chanxx)      
         deallocate(spola)

         k=1
         job=11
         lda=nz
         lwork=4*nz
         allocate(Sdettmp(nz,nz))
         allocate(worktmp(nz))
         allocate(work(lwork))
         allocate(ipvttmp(nz))

!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(l,ll,kk,kkk,ix,iy,i,j)
!$OMP& PRIVATE(test,Sdettmp,ipvttmp,info,deterz,worktmp,work)
!$OMP DO SCHEDULE(STATIC)           
         do l=1,nx*ny

            do kk=1,nz
               do kkk=1,nz
                  ll=l+nx*ny*abs(kk-kkk)                
                  Sdettmp(kk,kkk)=chanxxvect(ll)
c                  write(*,*) 'sdet',Sdettmp(kk,kkk),kk,kkk
               enddo
            enddo     

            call ZGETRF( nz, nz, sdettmp, nz, IPVttmp, INFO )
            if (INFO.ne.0) then
               write(*,*) 'probleme3 in zgefa.f',info,i
               stop
            endif
            do i=1,nz
               do j=1,nz
                  Sdetnn(i,j,l)=Sdettmp(i,j)
               enddo
               IPVtnn(i,l)=IPVttmp(i)
            enddo
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL

         deallocate(Sdettmp)
         deallocate(worktmp)
         deallocate(work)
         deallocate(ipvttmp)
       

c         call cpu_time(tf)
c         call date_and_time(date,time,zone,valuesf)
c         
c         call calculatedate(valuesf,valuesi,tf,ti,message)
      
      elseif (ntest.eq.1) then
c         message='produit'
c         call cpu_time(ti)
c         call date_and_time(date,time,zone,valuesi)


 
         do i=1,nz
c     sauvegarde dans un petit vecteur

!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(j,k,kk)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
            do j=1,ny
               do k=1,nx
                  vectxx(k+nx*(j-1))=FF(k+nx*(j-1)+nx*ny*(i-1))                 
c                  write(*,*) 'FF',vectxx(k+nx*(j-1)),j,k
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL

#ifdef USE_FFTW
            call dfftw_execute_dft(plan2b,vectxx,vectxx)      
#else 
            call fftsingletonz2d(vectxx,nx,ny,FFTW_BACKWARD)                 
#endif
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(j,k)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
            do j=1,ny
               do k=1,nx
                  xi(i+nz*(j-1)+nz*ny*(k-1))=vectxx(k+nx*(j-1))
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL

         enddo

         lda=nz
         NRHS=1
c     ***********************************************
c     fin calcul vecteur 
c     ***********************************************
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(l,i,j,k,kk,ii,jj)
!$OMP DO SCHEDULE(STATIC) 
         do l=1,nx*ny
            call  zgetrs( 'N',nz, NRHS, Sdetnn(1:nz,1:nz,l), LDA,
     $           IPVtnn(1:nz,l),xi((l-1)*nz+1:l*nz),LDA,INFO)
         enddo
!$OMP ENDDO 
!$OMP END PARALLEL
         

c     ****************************************
c     refait les opérations inverses
c     ****************************************
         dnxy=dble(nx*ny)
         do i=1,nz
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(j,k)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)
            do j=1,ny
               do k=1,nx                
                  vectxx(k+nx*(j-1))=xi(i+nz*(j-1)+nz*ny*(k-1))              
               enddo
            enddo
!$OMP ENDDO 
!$OMP END PARALLEL

#ifdef USE_FFTW
            call dfftw_execute_dft(plan2f,vectxx,vectxx)     
#else 
            call fftsingletonz2d(vectxx,nx,ny,FFTW_FORWARD)                   
#endif
            
!$OMP PARALLEL DEFAULT(SHARED)  PRIVATE(j,k)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(2)      
            do j=1,ny
               do k=1,nx
                  FF(k+nx*(j-1)+nx*ny*(i-1))=vectxx(k+nx*(j-1))/dnxy
c                  write(*,*) 'FF2',FF(k+nx*(j-1)+nx*ny*(i-1)),k+nx*(j-1)
c     $                 +nx*ny*(i-1)
               enddo
            enddo   
!$OMP ENDDO 
!$OMP END PARALLEL
                     
         enddo



c         call cpu_time(tf)
c         call date_and_time(date,time,zone,valuesf)      
c         call calculatedate(valuesf,valuesi,tf,ti,message)
         
      endif
   
      
      end
