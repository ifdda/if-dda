c**********************************************************
c**********************************************************
c**********************************************************
      subroutine integer_string4(i,fil)
      implicit none
      integer i,j
      character*4 fil
C     
      write(fil,100) i
      do j=1,4
         if (fil(j:j).eq.' ') then
            fil(j:j)='0'
         endif
      enddo
 100  format(i4)
      return
      end
c**********************************************************
c**********************************************************
c**********************************************************
      subroutine integer_string2(i,fil)
      implicit none
      integer i,j
      character*2 fil
C     
      write(fil,100) i
      do j=1,2
         if (fil(j:j).eq.' ') then
            fil(j:j)='0'
         endif
      enddo
 100  format(i2)
      return
      end
c**********************************************************
c**********************************************************
c**********************************************************
      subroutine integer_string3(i,fil)
      implicit none
      integer i,j
      character*3 fil
C     
      write(fil,100) i
      do j=1,3
         if (fil(j:j).eq.' ') then
            fil(j:j)='0'
         endif
      enddo
 100  format(i3)
      return
      end
c**********************************************************
c**********************************************************
c**********************************************************
      subroutine integer_string1(i,fil)
      implicit none
      integer i,j
      character*1 fil
C      
      write(fil,100) i

 100  format(i1)
      return
      end
