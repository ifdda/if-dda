      subroutine dipoleincp(xdip,ydip,zdip,p,x,y,z,aretecube,k0
     $     ,Ex,Ey,Ez,nstop,infostr)
      implicit none
      integer nstop
      double precision x,y,z,k0,pi,xdip,ydip,zdip,aretecube,dist
      double complex Ex,Ey,Ez,icomp,uncomp,propaesplibre(3,3),p(3),const
      character(64) infostr
      pi=dacos(-1.d0)
      icomp=(0.d0,1.d0)
      uncomp=(1.d0,0.d0)
      
c     calcul distance au dipôle
      dist=dsqrt((xdip-x)*(xdip-x)+(ydip-y)*(ydip-y) +(zdip-z)*(zdip-z))

      if (dist.le.aretecube/100.d0) then
         const=-(4.d0*pi)/3.d0/aretecube/aretecube/aretecube+icomp*2.d0
     $        /3.d0*k0*k0*k0
         Ex=const*p(1)
         Ey=const*p(2)
         Ez=const*p(3)
      else
         call propa_espace_libre(x,y,z,xdip,ydip,zdip,k0,propaesplibre)
         Ex=propaesplibre(1,1)*p(1)+propaesplibre(1,2)*p(2)
     $        +propaesplibre(1,3)*p(3)
         Ey=propaesplibre(2,1)*p(1)+propaesplibre(2,2)*p(2)
     $        +propaesplibre(2,3)*p(3)
         Ez=propaesplibre(3,1)*p(1)+propaesplibre(3,2)*p(2)
     $        +propaesplibre(3,3)*p(3)
      endif
      
      end
c****************************************************
c****************************************************
c****************************************************
      subroutine dipoleinsidep(xdip,ydip,zdip,xs,ys,zs,aretecube,nmax
     $     ,nbsphere,test)
      implicit none
      integer i,nmax,nbsphere,ic,jc,kc,test

      double precision xdip,ydip,zdip,aretecube
      double precision xs(nmax),ys(nmax),zs(nmax)


c     test si le dipole eclairant est au moins a une demi maille de tous
c     les dipoles (dans ce cas exterieur a l'objet), si proche alors mis
c     a la position du dipole le plus proche.

      test=0
      do i=1,nbsphere
         ic=nint((xdip-xs(i))/aretecube*0.9999d0)
         jc=nint((ydip-ys(i))/aretecube*0.9999d0)
         kc=nint((zdip-zs(i))/aretecube*0.9999d0)
         write(*,*) (xdip-xs(i))/aretecube,(ydip-ys(i))/aretecube,(zdip
     $        -zs(i))/aretecube,ic,jc,kc
         if (ic.eq.0.and.jc.eq.0.and.kc.eq.0) then
            xdip=xs(i)
            ydip=ys(i)
            zdip=zs(i)
            return
         endif
      enddo
      test=1
      
      end
