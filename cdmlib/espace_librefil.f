c     propagateur d espace libre
      subroutine propa_espace_librefil(x,y,z,x0,y0,z0,k0,aretecube,
     $     propaesplibre)
      implicit none
      integer i,j
      double precision x,y,z,x0,y0,z0,k0,aretecube,Id,Rab,Rtenseur,Rvect
     $     ,pi,Rab2,kf,alpha,alphap,alphapp,kr,kp,km,cip,cim,sip,sim,hh
      double complex propaesplibre,const1,gf,gfp,gfpp,icomp
      dimension propaesplibre(3,3),Id(3,3),Rtenseur(3,3),Rvect(3)

      pi=dacos(-1.d0)
      Rab2=0.d0
      Rvect(1)=(x-x0)
      Rvect(2)=(y-y0)
      Rvect(3)=(z-z0)
      kf=pi/aretecube
      icomp=(0.d0,1.d0)
      do i=1,3
         do j=1,3
            Id(i,j)=0.d0
            if (i.eq.j) Id(i,i)=1.d0
            Rtenseur(i,j)=Rvect(i)*Rvect(j)
         enddo
         Rab2=Rab2+Rvect(i)*Rvect(i)
      enddo
      Rab=dsqrt(Rab2)

      if (Rab.eq.0.d0) then
         do i=1,3
            do j=1,3
               propaesplibre(i,j)=(0.d0,0.d0) 
	   enddo
         enddo
      else
         kr=k0*Rab
         kp=(kf+k0)*Rab
         call cisi ( kp, cip, sip )
         km=(kf-k0)*Rab
         call cisi ( km, cim, sim )
         
         

         alpha=dsin(kr)*(cip-cim)+dcos(kr)*(pi-sip-sim)
         alphap=k0*dsin(kr)*(-pi+sim+sip)+k0*dcos(kr)*(cip-cim)-2.d0
     $        *dsin(kf*rab)/rab
         alphapp=k0*k0*dsin(kr)*(cim-cip)+k0*k0*dcos(kr)*(sim+sip-pi)+2
     $        .d0*dsin(kf*rab)/rab2-2.d0*kf*dcos(kf*rab)/rab
         const1=cdexp(icomp*k0*Rab)/Rab
         
         hh=(sin(kf*rab)-kf*rab*dcos(kf*rab))/rab2/rab/2.d0/pi/pi
         gf=(const1-alpha/pi/rab)*k0*k0
         gfp=icomp*k0*const1-const1/rab+alpha/pi/rab2-alphap/pi/Rab
         gfpp=-k0*k0*const1-2.d0*icomp*k0*const1/rab+2.d0*const1/rab2 -2
     $        .d0*alpha/rab2/rab/pi+2.d0*alphap/rab2/pi-alphapp/rab/pi


       
         do i=1,3
            do j=1,3
               propaesplibre(i,j)=Id(i,j)*(gf+gfp/rab+4.d0*pi/3.d0*hh)+(
     $              -gfp/rab2/rab+gfpp/rab2)*Rtenseur(i,j)
            enddo
         enddo
      endif
      end

c     ************************************************************
c     ************************************************************
c     ************************************************************
      subroutine cisi ( x, ci, si )

      implicit none

      double precision bj(101)
      double precision ci
      double precision el
      double precision eps
      integer k,m
      double precision p2
      double precision si
      double precision x
      double precision x2
      double precision xa
      double precision xa0
      double precision xa1
      double precision xcs
      double precision xf
      double precision xg
      double precision xg1
      double precision xg2
      double precision xr
      double precision xs
      double precision xss
      
      p2 = 1.570796326794897D+00
      el = 0.5772156649015329D+00
      eps = 1.0D-15
      x2 = x * x
      if ( x.eq. 0.0D+00 ) then

         ci = -1.0D+300
         si = 0.0D+00

      else if ( x .le. 16.0D+00 ) then

         xr = -0.25d0*x2
         ci = el + dlog ( x ) + xr
         do k = 2, 40
            xr = -0.5d0*xr*dble(k-1)/dble(k*k*(2*k-1))*x2
            ci = ci + xr
            if ( dabs ( xr ) .lt. dabs ( ci ) * eps ) then
               goto 10
            end if
         end do

 10      xr = x
         si = x
         do k = 1, 40
            xr = -0.5d0*xr*dble(2*k-1)/dble(k)/dble(4*k*k+4*k+1)*x2
            si = si + xr
            if ( dabs ( xr ) .lt. dabs ( si ) * eps ) then
               return
            end if
         end do

      else if ( x.le.32.d0 ) then

         m = int(47.2d0+0.82d0*x)
         xa1 = 0.d0
         xa0 = 1.d-100
         do k = m, 1, -1
            xa = 4.d0*dble(k)*xa0/x-xa1
            bj(k) = xa
            xa1 = xa0
            xa0 = xa
         end do
         xs = bj(1)
         do k = 3, m, 2
            xs = xs+2.d0*bj(k)
         end do
         bj(1) = bj(1)/xs
         do k = 2, m
            bj(k) = bj(k)/xs
         end do
         xr = 1.0D+00
         xg1 = bj(1)
         do k = 2, m
            xr=0.25d0*xr*dble(2*k-3)**2/(dble(k-1)*dble(2*k-1)**2)*x
            xg1 = xg1 + bj(k) * xr
         end do
         
         xr = 1.d0
         xg2 = bj(1)
         do k = 2, m
            xr =0.25d0*xr*dble(2*k-5)**2/(dble(k-1)*dble(2*k-3)**2)*x
            xg2 = xg2 + bj(k) * xr
         end do

         xcs = dcos(x/2.d0)
         xss = dsin(x/2.d0)
         ci = el+dlog(x)-x*xss*xg1+2.d0*xcs*xg2-2.d0*xcs*xcs
         si = x*xcs*xg1+2.d0*xss*xg2-dsin(x)

      else
         
         xr = 1.d0
         xf = 1.d0
         do k = 1, 9
            xr = -2.d0*xr*dble(k)*dble(2*k-1)/x2
            xf = xf + xr
         end do
         xr = 1.d0/x
         xg = xr
         do k = 1, 8
            xr = -2.d0*xr*dble(2*k+1)*dble(k)/x2
            xg = xg+xr
         end do
         ci = xf * dsin ( x ) / x - xg * dcos ( x ) / x
         si = p2 - xf * dcos ( x ) / x - xg * dsin ( x ) / x
      end if
      return
      end
