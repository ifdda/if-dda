c$$$      implicit none
c$$$      integer LDA,NDIM,NLAR,NOU,STATUS,STEPERR,NLOOP,MAXIT,i,j,IM
c$$$      parameter(lda=200,nlar=13)
c$$$      double precision NORM,TOL,TOLE
c$$$      double complex XI(lda),XR(lda),B(lda),WRK(lda,nlar) ,mat(lda,lda)
c$$$     $     ,diago(lda),ALPHA,BETA,ETA,DZETA,R0RN,icomp,xsol(lda)
c$$$      
c$$$  
c$$$      ndim=20
c$$$      icomp=(0.d0,1.d0)
c$$$      do i=1,ndim
c$$$         do j=1,ndim
c$$$            mat(i,j)=0.d0
c$$$         enddo
c$$$         mat(i,i)=(1.d0,0.d0)*dble(i)*icomp**dble(i)
c$$$         b(i)=(1.d0,0.d0)*icomp
c$$$         diago(i)=1.d0
c$$$         xi(i)=(0.d0,0.d0)
c$$$      enddo
c$$$      
c$$$      tol=1.d-4
c$$$      tole=tol
c$$$      nloop=0
c$$$      nou=0
c$$$      MAXIT=1000
c$$$      
c$$$      do i=1,ndim
c$$$         b(i)=b(i)*diago(i)
c$$$         xi(i)=xi(i)/diago(i)
c$$$      enddo
c$$$      
c$$$ 10   call GCORS2(Xi,XR,xsol,B,lda,ndim,nlar,nou,WRK,NLOOP,MAXIT ,TOLE
c$$$     $     ,NORM,ALPHA,BETA,ETA,DZETA,R0RN,STATUS,STEPERR)
c$$$      
c$$$c      write(*,*) 'donnees',nloop,nou,ALPHA,BETA,ETA,DZETA,R0RN
c$$$      
c$$$c      write(*,*) 'TOL',tol,STATUS
c$$$      if (STATUS.lt.0) then
c$$$         write(*,*) 'stop nstat',STATUS,STEPERR
c$$$         stop
c$$$      endif
c$$$      
c$$$      do i=1,ndim
c$$$         xi(i)=xi(i)*diago(i)
c$$$      enddo
c$$$      
c$$$      do i=1,ndim
c$$$         xr(i)=0.d0
c$$$         do j=1,ndim
c$$$            xr(i)=xr(i)+mat(i,j)*xi(j)
c$$$         enddo
c$$$c         write(*,*) 'xr xi',xr(i),xi(i)
c$$$      enddo
c$$$      
c$$$      do i=1,ndim
c$$$         xr(i)=xr(i)*diago(i)
c$$$      enddo
c$$$      
c$$$      
c$$$      if (STATUS.ne.1) goto 10
c$$$  
c$$$      
c$$$      write(*,*) '********************',NLOOP,tole
c$$$      DO I=1,NDIM
c$$$         WRITE(*,*) 'SOL',Xi(I)
c$$$      ENDDO
c$$$
c$$$c     calcul erreur relative
c$$$      tole=0.d0
c$$$      do i=1,ndim
c$$$         tole=tole+cdabs(xr(i)-b(i))
c$$$      enddo
c$$$      tole=tole/norm
c$$$      write(*,*) 'tole',tole
c$$$      stop
c$$$      if (tole.ge.tol) goto 10
c$$$
c$$$  
c$$$      end
c*************************************************
      SUBROUTINE GCORS2(Xi,XR,xsol,B,lda,ndim,nlar,nou,WRK,ITNO,MAXIT
     $     ,TOL,NORM,ALPHA,ALPHAt,rho,rhot,STATUS,STEPERR)
      IMPLICIT NONE

      INTEGER ii,nou,ndim,ITNO,LDA,MAXIT,STATUS,STEPERR,nlar,nseed
      DOUBLE COMPLEX B(lda),WRK(lda,nlar),xi(lda),xr(lda),xsol(lda)

*     .. Local Scalars ..
      DOUBLE COMPLEX alpha,alphat,rho,rhot,beta,betat,sigma,sigmat
      double complex  ctmp,icomp,rhoa,rhota
      DOUBLE PRECISION TOL,NORM,RESIDU,sunif,ran
c      write(*,*) 'NORM',NORM
      IF (NOU.EQ.1) GOTO 10
      IF (NOU.EQ.2) GOTO 20
      IF (NOU.EQ.3) GOTO 30
      IF (NOU.EQ.4) GOTO 40
      IF (NOU.EQ.5) GOTO 50
      IF (NOU.EQ.6) GOTO 60

      STATUS = 0
      STEPERR = -1

c     1:r
c     2:rt
c     3:q
c     4:qt
c     5:t
c     6:tt
c     7:s
c     8:st
c     9:u
c     10:ut
c     11:h
c     12:ht
c     13:s0*
c     14:r0*
c     calcul norme et Ax0
      NORM=0.d0
      do ii=1,ndim
         xsol(ii)=xi(ii)
         NORM=NORM+cdabs(B(ii))**2.d0
      enddo
      NORM=dsqrt(NORM)

      nou=1
      return

c     initialise r0=b-Ax0 et calcul residu initial
 10   ctmp=0.d0
      do ii=1,ndim
         WRK(ii,1)=B(ii)-xr(ii)
c         write(*,*) 'r',WRK(ii,1)
         xi(ii)=WRK(ii,1)
         ctmp=ctmp+xi(ii)*dconjg(xi(ii))
      enddo

      write(*,*) 'residu initial',cdabs(cdsqrt(ctmp))/norm,norm

c     calcul Ar0
      nou=2
      return

 20   do ii=1,ndim
         WRK(ii,14)=WRk(ii,1)   !r0*=r0     
         WRK(ii,9)=WRk(ii,1)    !u0=r0
         WRK(ii,5)=WRk(ii,1)    !t0=r0
         WRK(ii,2)=xr(ii)       !r0t=Ar0
         WRK(ii,3)=WRk(ii,2)    !q0=r0t
         WRK(ii,10)=WRk(ii,2)   !u0t=r0t
         WRK(ii,6)=WRk(ii,2)    !t0t=r0t      
         xi(ii)=WRK(ii,3)
      enddo

c     creation de s0
      nseed=0
      ran=SUNIF(4*nseed+1)
      icomp=(0.d0,1.d0)
      do ii=1,ndim
         WRK(ii,13)=SUNIF(-1)*dreal(WRK(ii,14))+
     $        icomp*SUNIF(-1)*dimag(WRK(ii,14)) !s0
c         WRK(ii,13)=1.d0 !s0
      enddo
      
      nou=3
      return

 30   rho=0.d0
      rhot=0.d0
      do ii=1,ndim
         WRK(ii,4)=xr(ii) !qt=Aq           
         rho=rho+WRK(ii,14)*dconjg(WRK(ii,2)) !rho=<r0*,r0t>
         rhot=rhot+WRK(ii,13)*dconjg(WRK(ii,2)) !rhot=<s0*,r0t>
      enddo

c      write(*,*) 'rho',rho,rhot
c     commence la boucle
      ITNO=-1
 100  ITNO=ITNO+1
c      write(*,*) 'ITNO',ITNO
      sigma=0.d0
      sigmat=0.d0
      do ii=1,ndim
         sigma=sigma+WRK(ii,14)*dconjg(WRK(ii,4)) !sigma=<r0*,qt>
         sigmat=sigmat+WRK(ii,13)*dconjg(WRK(ii,4)) !sigma=<s0*,qt>
c         write(*,*) 'ttt',WRK(ii,14),WRK(ii,13),WRK(ii,4)
      enddo
c      write(*,*) 'sigma',sigma,sigmat
      
      if (sigma.eq.0.d0) then
         STATUS=-1
         STEPERR=1
         return 
      endif
      if (sigmat.eq.0.d0) then
         STATUS=-1
         STEPERR=2
         return 
      endif

      alpha=rho/sigma
      alphat=rhot/sigmat
c      write(*,*) 'alpha',alpha,alphat
      
      if (alpha.eq.0.d0) then
         STATUS=-1
         STEPERR=3
         return 
      endif
      if (alphat.eq.0.d0) then
         STATUS=-1
         STEPERR=4
         return 
      endif

      
      do ii=1,ndim
         WRK(ii,7)= WRK(ii,5)-alpha*WRK(ii,3) !s=t-alpha*q
         WRK(ii,8)= WRK(ii,6)-alpha*WRK(ii,4) !st=tt-alpha*qt
         WRK(ii,11)= WRK(ii,9)-alphat*WRK(ii,3) !h=u-alphat*q
         WRK(ii,12)= WRK(ii,10)-alphat*WRK(ii,4) !ht=ut-alphat*qt
c         write(*,*) 's',WRK(ii,7)
c         write(*,*) 'st',WRK(ii,8)
c         write(*,*) 'h',WRK(ii,11)
c         write(*,*) 'ht',WRK(ii,12)
      enddo

      do ii=1,ndim
         xsol(ii)=xsol(ii)+alpha*WRK(ii,9)+alphat*WRK(ii,7) !x=x+alpha*u+alphat*s
         WRK(ii,1)=WRK(ii,1)-alpha*WRK(ii,10)-alphat*WRK(ii,8) !r=r-alpha*ut-alphat*st
c         write(*,*) 'x',xsol(ii)
c         write(*,*) 'r',WRK(ii,1)
         xi(ii)=WRK(ii,1)
      enddo
      RESIDU=0.d0
      do ii=1,ndim
         RESIDU=RESIDU+cdabs(WRK(ii,1))**2.d0
      enddo
      RESIDU=dsqrt(RESIDU)/NORM
      write(*,*) 'RESIDU',RESIDU
c      stop
      if (RESIDU.le.TOL) then
         STATUS=1
         do ii=1,ndim
            xi(ii)=xsol(ii)
         enddo
         nou=6
         return
 60   endif
      nou=4
      return
      
c     calcul de rt=Ar
 40   do ii=1,ndim
         WRK(ii,2)=xr(ii)
      enddo

      rhoa=0.d0
      rhota=0.d0
      do ii=1,ndim
         rhoa=rhoa+WRK(ii,14)*dconjg(WRK(ii,2)) !rho=<r0*,rt>
         rhota=rhota+WRK(ii,13)*dconjg(WRK(ii,2))!rhot=<s0*,rt>
      enddo

      beta=rhoa/rho*alpha/alphat
      betat=rhota/rhot*alphat/alpha
      rho=rhoa
      rhoa=rhota

      do ii=1,ndim
         WRK(ii,5)=WRK(ii,1)+betat*WRK(ii,7) !t=r+betat*s
         WRK(ii,6)=WRK(ii,2)+betat*WRK(ii,8) !tt=rt+betat*st
         WRK(ii,9)=WRK(ii,1)+beta*WRK(ii,11) !u=r+beta*h
         WRK(ii,10)=WRK(ii,2)+beta*WRK(ii,12) !ut=rt+beta*ht
      enddo
      do ii=1,ndim
         WRK(ii,3)=WRK(ii,6)+beta*(WRK(ii,12)+betat*WRK(ii,3)) !q=tt+beta*(ht+betat*q)
         xi(ii)=WRK(ii,3)
      enddo

      nou=5
      return

 50   do ii=1,ndim
         WRK(ii,4)=xr(ii) !qt=Aq
      enddo

      

      if (ITNO.le.MAXIT) goto 100

      STATUS=1
      do ii=1,ndim
         xi(ii)=xsol(ii)
      enddo

      END
