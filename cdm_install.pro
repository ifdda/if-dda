    TEMPLATE = subdirs
    SUBDIRS += qwt cdmlib cdm tests/test0 tests/test1 tests/test2 tests/test3 tests/test4 
    CONFIG += ordered

    qwt.file = qwt-6.2.0/qwt.pro
    cdmlib.file = cdmlib/cdmlib.pro
    cdm.file = cdm/cdm.pro
    tests.file = tests/test0/test0.pro    
    tests.file = tests/test1/test1.pro
    tests.file = tests/test2/test2.pro
    tests.file = tests/test3/test3.pro
    tests.file = tests/test4/test4.pro



