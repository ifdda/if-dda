#include <iostream>
#include <string>
#include <fstream>
using namespace std;
#ifndef PLOT_H
#define PLOT_H
#include <qpainterpath.h>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_symbol.h>
#include <qwt_scale_div.h>
#include <qwt_plot_spectrogram.h>
#include <qprinter.h>
#include <qprintdialog.h>
#include <qwt_color_map.h>
#include <qwt_scale_widget.h>
#include <qwt_scale_draw.h>
#include <qwt_plot_zoomer.h>
#include <qwt_plot_panner.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_renderer.h>
#include <qwt_text_engine.h>
#include <qwt_matrix_raster_data.h>
#include <qwt_point_mapper.h>
#include <qwt_painter.h>
#include <qwt_series_data.h>
#include <qwt_point_data.h>
#include <qwt_point_polar.h>
#include <QBuffer>
#include <QGridLayout>
#include <Q3DScatter>
#include <Q3DSurface>
#include <QLinearGradient>
#include <QRandomGenerator>
#include <QAbstract3DGraph>
#include <QScreen>
#include "QsLog.h"

//using namespace QtDataVisualization;

class PlotRaster: public QwtPlot
{
    Q_OBJECT

public:
    PlotRaster( QWidget * = NULL, QVector<QwtPoint3D> *_data = NULL, 
                const int &_num_col = 0, QString _title = 0, QString _xtitle = 0,
		QString _ytitle = 0, QVector<QColor> *_colors = NULL);
    ~PlotRaster();
    
private:
    QwtPlotSpectrogram *d_spectrogram;
    QVector<QwtPoint3D> *data;
    QVector<QColor> *colors;
    int num_col;
    QString title;
    QString xtitle;
    QString ytitle;
};

class Plot : public QWidget
{
  Q_OBJECT

  public:
      Plot( QWidget * = NULL, QVector<QwtPoint3D> *_data = NULL, QVector<double> *colormap = NULL, QString _title = 0, int _param = 0);
      ~Plot();
      QString getTitle() { return title;};
      QAbstract3DGraph *graph;
      //Q3DSurface *surface;
   private:
      QVector<double> *colormap;
      QVector<QwtPoint3D> *data;
      QString title;
      int param;
      static bool MysortzFunc(const QwtPoint3D& i, const QwtPoint3D& j);
      static bool MysortyFunc(const QwtPoint3D& i, const QwtPoint3D& j);
      static bool MysortxFunc(const QwtPoint3D& i, const QwtPoint3D& j);
      
};

class PlotVector: public QwtPlot
{
    Q_OBJECT

public:
    PlotVector( QWidget * = NULL, QVector<QwtPoint3D> *_data = NULL, 
                QVector<QwtPointPolar> *_datap = NULL,
                int _num_col = 0, QString _title = 0, QString _xtitle = 0,
		QString _ytitle = 0);
    ~PlotVector();

private:
    QwtPlotCurve *d_curve;
    QVector<QwtPoint3D> *data;
    QVector<QwtPointPolar> *datap;
    int num_col;
    QString title;
    QString xtitle;
    QString ytitle;
};
#endif
