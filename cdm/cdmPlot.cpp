#include "cdmPlot.h"

class MyQwtSymbol: public QwtSymbol
{
public:
    MyQwtSymbol( const QBrush &brush , QwtPointPolar datap)
    {
        QPen pen( Qt::black, 1.5);
        pen.setJoinStyle( Qt::RoundJoin );
        pen.setCosmetic( true );
        QPainterPath path = createArrow( datap );
        //setPinPoint( QPointF( 0.0, 0.0 ) );
        setPen( pen );
        setBrush( brush );
        setPath( path );    
    }

private:
    QPainterPath createArrow( QwtPointPolar datap ) const
    {
        QPainterPath path; 
        const double h = datap.radius();
        const double a = datap.radius() / 10;
        QLOG_DEBUG() << "MyQwtSymbol::createArrow:radius:" << 
				QString::number(datap.radius());
        QLOG_DEBUG() << "MyQwtSymbol::createArrow:azimuth:" << 
				QString::number(datap.azimuth());
        
        path.lineTo( h - 3 * a , 0 );
        path.moveTo( h, 0 );
        path.lineTo( h - 3 * a, a );
        path.lineTo( h - 3 * a, -a );
        path.lineTo( h, 0 );
      
        QTransform transform;
        transform.rotate( datap.azimuth() * (-180.) / M_PI );
        path = transform.map( path );

        return path;
    }
};
class MyQwtPlotCurve: public QwtPlotCurve
{
public:
    MyQwtPlotCurve( QVector<QwtPoint3D> *_data, QVector<QwtPointPolar> *_datap )
    {
     QLOG_DEBUG() << "MyQwtPlotCurve:MyQwtPlotCurve" ;
          
     data = _data;
     datap = _datap;
     // Init symbols
     setSymbol(new MyQwtSymbol(Qt::magenta,datap->at(0)));
    }
    void drawSymbols (QPainter *painter,
		      const QwtSymbol &symbol,
		      const QwtScaleMap &xMap,
		      const QwtScaleMap &yMap,
		      const QRectF &canvasRect,
		      int   from,
		      int   to)	const 
    {
    //QLOG_DEBUG() << "MyQwtPlotCurve::drawSymbols";
    //QLOG_DEBUG() << "MyQwtPlotCurve::drawSymbols:from" << from << " to:" << to;
    QBrush brush = Qt::magenta;
    QwtPointMapper mapper;
    mapper.setFlag( QwtPointMapper::RoundPoints, 
       QwtPainter::roundingAlignment( painter ) );
    mapper.setFlag( QwtPointMapper::WeedOutPoints, 
       testPaintAttribute( QwtPlotCurve::FilterPoints ) );
    mapper.setBoundingRect( canvasRect );
  
    
    //QLOG_DEBUG() << "MyQwtPlotCurve::drawSymbols:data->size()=" << data->size();
    // Reconstruct symbols
    // Calculate average distance
    double distance = 0;
     for ( int j = 0 ; j < data->size() ; j++)
        distance = distance + fabs(data->at(j).x());
     distance/=data->size();
     //QLOG_DEBUG() << "MyQwtPlotCurve::drawSymbols:Average distance =" 
      //            << distance;
     
    for (int i = 0 ; i < data->size(); i++) {
     QVector<double> xvect, yvect,fxvect, fyvect;
     xvect.push_back( data->at(i).x());
     yvect.push_back( data->at(i).y());     
     QwtPointArrayData<double> *dataarr = new QwtPointArrayData<double>(xvect,yvect); // version 6.2.0 Qwt

     //QwtPointArrayData *dataarr = new QwtPointArrayData(xvect,yvect); // version 6.1.2 Qwt
     const QPolygonF points = mapper.toPointsF( xMap, yMap, dataarr, 0, 0);
     
     QLOG_DEBUG() << "distance* radius " << QString::number(datap->at(i).radius() * 
              distance);
     if (datap->at(i).radius() != 0)
       fxvect.push_back( data->at(i).x() + datap->at(i).radius() * distance / 5 + distance / 10);
     else
       fxvect.push_back( data->at(i).x() );
     fyvect.push_back( data->at(i).y());
      QwtPointArrayData<double> *dataarrp = new QwtPointArrayData<double>(fxvect,fyvect); // version 6.2.0 Qwt
    // QwtPointArrayData *dataarrp = new QwtPointArrayData(fxvect,fyvect);  // version 6.1.2 Qwt
     const QPolygonF fpoints = mapper.toPointsF( xMap, yMap, dataarrp, 0, 0);
     if ( fpoints.size() > 0 ) {
	QwtPointPolar dataptrans(datap->at(i).azimuth(),
				fpoints.at(0).x() - points.at(0).x());
	QLOG_DEBUG() << "MyQwtPlotCurve::dataptrans radius()=" 
		     << dataptrans.radius();
	QLOG_DEBUG() << "MyQwtPlotCurve::dataptrans::azimuth()=" 
                     << dataptrans.azimuth();
	MyQwtSymbol mysymbol(brush,dataptrans);
	if ( points.size() > 0 )
	  mysymbol.drawSymbols( painter, points );
       }
       
       QLOG_DEBUG() << "MyQwtPlotCurve::drawSymbols:data->at(" << i << ").x=" 
                   << data->at(i).x();
       QLOG_DEBUG() << "MyQwtPlotCurve::drawSymbols:data->at(" << i << ").y=" 
                   << data->at(i).y();
       QLOG_DEBUG() << "MyQwtPlotCurve::drawSymbols:datap->at(" << i << ").radius=" 
                   << datap->at(i).radius();
       QLOG_DEBUG() << "MyQwtPlotCurve::drawSymbols:datap->at(" << i << ").azimuth=" 
                   << datap->at(i).azimuth();
     
       QLOG_DEBUG() << "MyQwtPlotCurve::xvect=" << xvect.at(0);
       QLOG_DEBUG() << "MyQwtPlotCurve::yvect=" << yvect.at(0);
       QLOG_DEBUG() << "MyQwtPlotCurve::QPolygonF::points.x()=" << points.at(0).x();
       QLOG_DEBUG() << "MyQwtPlotCurve::QPolygonF::points.y()=" << points.at(0).y();
       QLOG_DEBUG() << "MyQwtPlotCurve::QPolygonF::points.size()=" << points.size();
       QLOG_DEBUG() << "MyQwtPlotCurve::fxvect=" << fxvect.at(0);
       QLOG_DEBUG() << "MyQwtPlotCurve::fyvect=" << fyvect.at(0);       
       QLOG_DEBUG() << "MyQwtPlotCurve::QPolygonF::fpoints.x()=" << fpoints.at(0).x();
       QLOG_DEBUG() << "MyQwtPlotCurve::QPolygonF::fpoints.y()=" << fpoints.at(0).y();
       QLOG_DEBUG() << "MyQwtPlotCurve::QPolygonF::fpoints.size()=" << fpoints.size(); 
    }
   }
   ~MyQwtPlotCurve() {
      delete data;
      delete datap;
   }
private:
    QVector<QwtPoint3D>    *data;
    QVector<QwtPointPolar> *datap;
    
};

class MyQwtPlotZoomer: public QwtPlotZoomer
{
public:

    MyQwtPlotZoomer( QWidget *canvas):QwtPlotZoomer( canvas )
    {
        setTrackerMode( AlwaysOff );
    }

    MyQwtPlotZoomer( QWidget *canvas, QVector<QwtPoint3D> *_data , double _maxx, double _miny, double _maxy):QwtPlotZoomer( canvas )
    {
        data = _data;
        maxx = _maxx;
        miny = _miny;
        maxy = _maxy;
        setTrackerMode( AlwaysOn );
    }

    virtual QwtText trackerTextF( const QPointF &pos ) const
    {
        QColor bg( Qt::white );
        bg.setAlpha( 160 );
        QwtText text("");
	for (int i = 0 ; i < data->size() - 1; i++) {
          QwtPoint3D infpoint = data->at(i);
          QwtPoint3D suppoint = data->at(i+1);
	  double stepx=(data->at(1).x()-data->at(0).x())/2;
	  double stepy = stepx; 
          double infx = infpoint.x();
          double infy = infpoint.y();
          double supx = suppoint.x();
          double supy = suppoint.y();
          double infz = infpoint.z();
          double supz = suppoint.z();
	  QLOG_DEBUG() << "COUCOU :" << QString::number(stepx) << QString::number(stepy) ;
	  QLOG_DEBUG() << "POINT ORDER LIST SUP X :" << QString::number(supx) << " SUP Y :" << QString::number(supy) << " SUP Z :" << QString::number(supz);
          QLOG_DEBUG() << "POINT ORDER LIST INF X :" << QString::number(infx) << " INF Y :" << QString::number(infy) << " INF Z :" << QString::number(infz);
           //              << " POS X :" << QString::number(pos.x()) << " POS Y :" << QString::number(pos.y());
          if ( pos.x() > infx && pos.x() < supx ) {
             
             if (  pos.x() < 0 &&  pos.y() > infy ) {
                //QLOG_INFO() << "1:POS X :" << QString::number(pos.x()) << " POS Y :" << QString::number(pos.y()) << " INF Y :" << QString::number(infy);
                text.setText("(" + QString::number(infx+stepx,'E',2) + "," + QString::number(infy+stepy,'E',2) + ") " + "," + QString::number(infz,'E',5) + ") ");
                text.setBackgroundBrush( QBrush( bg ) );
             }
             else if ( pos.x() > 0 && pos.y() > supy ) {
                //QLOG_INFO() << "2:POS X :" << QString::number(pos.x()) << " POS Y :" << QString::number(pos.y()) << " SUP Y :" << QString::number(supy);
                text.setText("(" + QString::number(infx+stepx,'E',2) + "," + QString::number(infy+stepy,'E',2) + ") " + "," + QString::number(infz,'E',5) + ") ");
                text.setBackgroundBrush( QBrush( bg ) );
             }
          }
          else if ( pos.x() >= maxx && qRound(infx) == qRound(maxx) ) {
            
             if ( pos.y() >= infy  &&  pos.y() <= supy ) {
                //QLOG_INFO() << "3: SUP X :" << QString::number(supx) << " SUP Y :" << QString::number(supy) << " SUP Z :" << QString::number(supz);
                text.setText("(" + QString::number(infx+stepx,'E',2) + "," + QString::number(infy+stepy,'E',2) + ") " + "," + QString::number(infz,'E',5) + ") ");
                text.setBackgroundBrush( QBrush( bg ) );
             }
             else if ( pos.y() <= miny ) {
                //QLOG_INFO() << "4: SUP X :" << QString::number(supx) << " SUP Y :" << QString::number(supy) << " SUP Z :" << QString::number(supz);
                text.setText("(" + QString::number(supx+stepx,'E',2) + "," + QString::number(supy+stepy,'E',2) + ") " + "," + QString::number(supz,'E',5) + ") ");
                text.setBackgroundBrush( QBrush( bg ) );
             }
             else if ( pos.y() >= maxy ) {
                //QLOG_INFO() << "5: SUP X :" << QString::number(supx) << " SUP Y :" << QString::number(supy) << " SUP Z :" << QString::number(supz);
                text.setText("(" + QString::number(supx+stepx,'E',2) + "," + QString::number(supy+stepy,'E',2) + ") " + "," + QString::number(supz,'E',5) + ") ");
                text.setBackgroundBrush( QBrush( bg ) );
             }
          }
        }
        return text;
    }
private:
   QVector<QwtPoint3D>    *data;
   double                  maxx;
   double                  miny;
   double                  maxy;
};

class MyQwtScaleDraw: public QwtScaleDraw
{
public:
    MyQwtScaleDraw():QwtScaleDraw()
    {
    }
    ~MyQwtScaleDraw() {
       QLOG_DEBUG() << "delete MyQwtScaleDraw::~MyQwtScaleDraw";
    }
    virtual QwtText label( double value ) const
    {
        QwtText text(QString::number(value,'E',5));
        return text;
    }
};

class MyQwtMatrixRasterData: public QwtMatrixRasterData
{
public:
    MyQwtMatrixRasterData():QwtMatrixRasterData()
    {      
    }

};

class MyQwtLinearColorMap: public QwtLinearColorMap
{
public:
    MyQwtLinearColorMap(QVector<QColor> *colors):QwtLinearColorMap()
    {
       setColorInterval(colors->at(0), colors->at(1));
       setMode( QwtLinearColorMap::ScaledColors);
    }
    ~MyQwtLinearColorMap() {
       QLOG_DEBUG() << "delete MyQwtLinearColorMap::MyQwtLinearColorMap";
   }
};

PlotRaster::PlotRaster( QWidget *parent , QVector<QwtPoint3D> *_data, 
			const int &_num_col, QString _title, QString _xtitle, QString _ytitle,
			QVector<QColor> *_colors):
    QwtPlot( parent )
{

    QLOG_DEBUG() << "PlotRaster::PlotRaster" ;
   
    data = _data;
    title = _title;
    xtitle = _xtitle;
    ytitle = _ytitle;
    num_col = _num_col;
    colors = _colors;
    this->setTitle( title );
    d_spectrogram = new QwtPlotSpectrogram();
    d_spectrogram->setRenderThreadCount( 0 ); // use system specific thread count
    MyQwtLinearColorMap *colormap =  new MyQwtLinearColorMap(colors);
    d_spectrogram->setColorMap( colormap );
   
    d_spectrogram->setCachePolicy( QwtPlotRasterItem::PaintCache );
    MyQwtMatrixRasterData *rasterdata = new MyQwtMatrixRasterData();
    QVector<double> zvalues;
    for (int i = 0 ; i < data->size(); i++)
       zvalues.push_back(data->at(i).z());
    
    rasterdata->setValueMatrix(zvalues,num_col);
    double minx=1e308,maxx=-1e308;
    double miny=1e308,maxy=-1e308;
    double minz=1e308,maxz=-1e308;
    for (int i = 0 ; i < data->size(); i++) {
       QwtPoint3D point = data->at(i);
       double tmpx = point.x();
       double tmpy = point.y();
       double tmpz = point.z();
       //QLOG_INFO() << " X :" << QString::number(tmpx) << " Y :" << QString::number(tmpy);
       if (tmpx >= maxx) maxx = tmpx;
       if (tmpx <= minx) minx = tmpx;
       if (tmpy >= maxy) maxy = tmpy;
       if (tmpy <= miny) miny = tmpy;
       if (tmpz == -100) continue;
       if (tmpz >= maxz) maxz = tmpz;
       if (tmpz <= minz) minz = tmpz;
    }
    if (data->size() == 0) {
       minx = maxx = miny = maxy = minz = maxz = 0;
    }
    
    QLOG_INFO() << "PlotRaster: Number of columns:" << num_col;
    QLOG_INFO() << "PlotRaster: Data size:" << data->size();
    QLOG_INFO() << "PlotRaster: Num columns:" << rasterdata->numColumns();
    QLOG_INFO() << "PlotRaster: Num rows:" << rasterdata->numRows();
    QLOG_INFO() << "Min x:" << QString::number(minx);
    QLOG_INFO() << "Max x:" << QString::number(maxx);
    QLOG_INFO() << "Min y:" << QString::number(miny);
    QLOG_INFO() << "Max y:" << QString::number(maxy);
    QLOG_INFO() << "Min z:" << QString::number(minz);
    QLOG_INFO() << "Max z:" << QString::number(maxz);
    double intervalx = (maxx-minx) / (num_col - 1);
    double intervaly = (maxy-miny) / (num_col - 1);
    QwtInterval rangex = QwtInterval(minx,maxx+intervalx);
    QwtInterval rangey = QwtInterval(miny,maxy+intervaly);
    QwtInterval rangez;
    if (minz == maxz  ) {
      minz=minz*0.5;
      maxz=maxz*1.5;
      if (minz == maxz) {
	minz=0;
	maxz=0.1;
      }
    }
      
    rangez = QwtInterval(minz,maxz);
    rasterdata->setInterval( Qt::XAxis, rangex );
    rasterdata->setInterval( Qt::YAxis, rangey );
    rasterdata->setInterval( Qt::ZAxis, rangez );
    d_spectrogram->setData( rasterdata );
    d_spectrogram->attach( this );

    // Axis range
    
    QList< double > ticksx[QwtScaleDiv::NTickTypes] ;
    for (int i = 0 ; i < num_col; i++ ) {
       ticksx[0].append(minx + i * intervalx);
    }
    
    QList< double > ticksy[QwtScaleDiv::NTickTypes] ;
    for (int i = 0 ; i < num_col; i++ ) {
       ticksy[0].append(miny + i * intervaly);
    }
    
    this->setAxisScale(QwtPlot::xBottom,minx,maxx+intervalx,0);
    this->setAxisScale(QwtPlot::yLeft,miny,maxy+intervaly,0);
    
    this->setAxisTitle(QwtPlot::xBottom, xtitle);
    this->setAxisTitle(QwtPlot::yLeft, ytitle);
    // A color bar on the right axis
    QwtScaleWidget *rightAxis = this->axisWidget( QwtPlot::yRight );
    rightAxis->setColorBarEnabled( true );
    MyQwtLinearColorMap *colormap2 =  new MyQwtLinearColorMap(colors);
    rightAxis->setColorMap( rangez, colormap2 );
    rightAxis->setSpacing(2);    
    rightAxis->setScaleDraw(new MyQwtScaleDraw());
    this->setAxisScale( QwtPlot::yRight, minz, maxz, 0 );
    
    this->enableAxis( QwtPlot::yRight );
    this->plotLayout()->setAlignCanvasToScales( true );
    this->replot();

    // LeftButton for the zooming
    // MidButton for the panning
    // RightButton: zoom out by 1
    // Ctrl+RighButton: zoom out to full size
    //QLOG_INFO() << " QwtPlotZoomer() --> " << miny << "," << maxy;
    QwtPlotZoomer* zoomer = new MyQwtPlotZoomer( canvas() , data, maxx, miny, maxy);
    zoomer->setMousePattern( QwtEventPattern::MouseSelect2,
        Qt::RightButton, Qt::ControlModifier );
    zoomer->setMousePattern( QwtEventPattern::MouseSelect3,
        Qt::RightButton );

    QwtPlotPanner *panner = new QwtPlotPanner( canvas() );
    panner->setAxisEnabled( QwtPlot::yRight, false );
    panner->setMouseButton( Qt::MiddleButton );

    // Avoid jumping when labels with more/less digits
    // appear/disappear when scrolling vertically

    const QFontMetrics fm( axisWidget( QwtPlot::yLeft )->font() );
    QwtScaleDraw *sd = axisScaleDraw( QwtPlot::yLeft );
    sd->setMinimumExtent( fm.boundingRect( "100.00").width( ) );

    const QColor c( Qt::darkBlue );
    zoomer->setRubberBandPen( c );
    zoomer->setTrackerPen( c );

    //this->enableAxis(QwtPlot::xBottom, false);
    //this->enableAxis(QwtPlot::yLeft, false);
}

PlotRaster::~PlotRaster() {
    QLOG_INFO() << "PlotRaster::~PlotRaster:: Deleting Plot";
    delete d_spectrogram;
}

PlotVector::PlotVector( QWidget *parent , QVector<QwtPoint3D> *_data, 
			QVector<QwtPointPolar> *_datap, int _num_col, QString _title,
			QString _xtitle, QString _ytitle):
    QwtPlot( parent )
{
    QLOG_DEBUG() << "PlotVector::PlotVector" ;

    data = _data;
    datap = _datap;
    title = _title;
    xtitle = _xtitle;
    ytitle = _ytitle;
    num_col = _num_col;
    d_curve = new MyQwtPlotCurve(data, datap);
    d_curve->setRenderThreadCount( 0 ); // use system specific thread count
    d_curve->setStyle(QwtPlotCurve::NoCurve);
    QPolygonF points;
    for (int i = 0 ; i < data->size(); i++) {
        points << QPointF(data->at(i).x(),data->at(i).y());
    }
    d_curve->setSamples( points );
    this->setTitle( title );
    d_curve->attach( this );
    double minx=1e308,maxx=-1e308;
    double miny=1e308,maxy=-1e308;
    double minz=1e308,maxz=-1e308;
    for (int i = 0 ; i < data->size(); i++) {
       QwtPoint3D point = data->at(i);
       double tmpx = point.x();
       double tmpy = point.y();
       double tmpz = point.z();
       if (tmpx > maxx) maxx = tmpx;
       if (tmpx < minx) minx = tmpx;
       if (tmpy > maxy) maxy = tmpy;
       if (tmpy < miny) miny = tmpy;
       if (tmpz == -100) continue;
       if (tmpz > maxz) maxz = tmpz;
       if (tmpz < minz) minz = tmpz;
    }
    if (data->size() == 0) {
       minx = maxx = miny = maxy = minz = maxz = 0;
    }
     double xabsmax;
    if (fabs(minx) > fabs(maxx))
       xabsmax = fabs(maxx);
    else
       xabsmax = fabs(minx);
    double yabsmax;
    if (fabs(miny) > fabs(maxy))
       yabsmax = fabs(maxy);
    else
       yabsmax = fabs(miny);
    QLOG_DEBUG() << " xabsmax = " << QString::number(xabsmax,'g',8);
    QLOG_DEBUG() << " yabsmax = " << QString::number(yabsmax,'g',8);
    
    double xmin = minx - xabsmax/1.5;
    double xmax = maxx + xabsmax/1.5;
    double ymin = miny - yabsmax/1.5;
    double ymax = maxy + yabsmax/1.5;
   
    this->setAxisScale( QwtPlot::xBottom, xmin, xmax, 0 );
    this->setAxisScale( QwtPlot::yLeft, ymin, ymax, 0 );
    this->setAxisTitle(QwtPlot::xBottom, xtitle);
    this->setAxisTitle(QwtPlot::yLeft, ytitle);
    plotLayout()->setAlignCanvasToScales( true );
    replot();
    // LeftButton for the zooming
    // MidButton for the panning
    // RightButton: zoom out by 1
    // Ctrl+RighButton: zoom out to full size

    QwtPlotZoomer* zoomer = new MyQwtPlotZoomer( this->canvas() );
    zoomer->setZoomBase(QRectF(QPointF(xmin,ymax),QPointF(xmax,ymin))); 
    zoomer->setMousePattern( QwtEventPattern::MouseSelect2,
        Qt::RightButton, Qt::ControlModifier );
    zoomer->setMousePattern( QwtEventPattern::MouseSelect3,
        Qt::RightButton );

    QwtPlotPanner *panner = new QwtPlotPanner( canvas() );
    panner->setAxisEnabled( QwtPlot::yRight, false );
    panner->setMouseButton( Qt::MiddleButton );

    // Avoid jumping when labels with more/less digits
    // appear/disappear when scrolling vertically

    const QFontMetrics fm( this->axisWidget( QwtPlot::yLeft )->font() );
    QwtScaleDraw *sd = axisScaleDraw( QwtPlot::yLeft );
    sd->setMinimumExtent( fm.boundingRect("100.00").width() );

    const QColor c( Qt::darkBlue );
    zoomer->setRubberBandPen( c );
    zoomer->setTrackerPen( c );
}

PlotVector::~PlotVector() {
    QLOG_DEBUG() << "PlotVector::~PlotVector:: Deleting Plot";
    delete d_curve;
}

Plot::Plot( QWidget *parent, QVector<QwtPoint3D> *_data, QVector<double> *_colormap, QString _title, int _param ):
    QWidget( parent ) 
{

    data = _data;  
    colormap = _colormap;
    title = _title;
    param = _param;
    //
    // Calculate mins, maxs
    //
    double minx=1e308,maxx=-1e308;
    double miny=1e308,maxy=-1e308;
    double minz=1e308,maxz=-1e308;
    double minr=1e308,maxr=-1e308;
    for (int i = 0 ; i < data->size(); i++) {
       QwtPoint3D point = data->at(i);
       double tmpx = point.x();
       double tmpy = point.y();
       double tmpz = point.z();
       double tmpr = colormap->at(i);
       if (tmpx > maxx) maxx = tmpx;
       if (tmpx < minx) minx = tmpx;
       if (tmpy > maxy) maxy = tmpy;
       if (tmpy < miny) miny = tmpy;
       if (tmpz > maxz) maxz = tmpz;
       if (tmpz < minz) minz = tmpz;
       if (tmpr > maxr) maxr = tmpr;
       if (tmpr < minr) minr = tmpr;
    }
    QLOG_INFO() << "COLORMAP MIN = " << minr;
    QLOG_INFO() << "COLORMAP MAX = " << maxr;
    //
    // Fill data
    //
    if ( title == "Dipoles" ) {
       graph = new Q3DScatter();
       graph->setAspectRatio(1.0);
       graph->setHeight(this->height());
       graph->setWidth(this->width());
       graph->setShadowQuality(QAbstract3DGraph::ShadowQualityNone);
       graph->activeTheme()->setType(Q3DTheme::ThemeStoneMoss);
       graph->setReflectivity(0.0f);
       graph->scene()->activeCamera()->setCameraPreset(Q3DCamera::CameraPresetFront);
       graph->activeTheme()->setAmbientLightStrength(1.0f);
       ((Q3DScatter*)(graph))->axisX()->setTitle("X");
       ((Q3DScatter*)(graph))->axisY()->setTitle("Y");
       ((Q3DScatter*)(graph))->axisZ()->setTitle("Z");
       ((Q3DScatter*)(graph))->axisX()->setLabelFormat("%.2E");
       ((Q3DScatter*)(graph))->axisY()->setLabelFormat("%.2E");
       ((Q3DScatter*)(graph))->axisZ()->setLabelFormat("%.2E");
       ((Q3DScatter*)(graph))->axisX()->setRange(minx, maxx);
       ((Q3DScatter*)(graph))->axisY()->setRange(miny, maxy);
       ((Q3DScatter*)(graph))->axisZ()->setRange(minz, maxz);
       
       // Create series respect to epsilon values
       double epsilonRef = -999999;
       QVector<double> copyColormap(*colormap);
       std::sort(copyColormap.begin(), copyColormap.end()); // Order colormap in this copy
       QVector<double> *epsilonValues = new QVector<double>();
       for (int j = 0; j < copyColormap.size(); j++) {
          double epsilon = copyColormap.at(j);
          if ( epsilon != epsilonRef) {
             epsilonRef = epsilon;
             QScatter3DSeries *series = new QScatter3DSeries();
             series->setMeshSmooth(true);
             series->setMesh(QAbstract3DSeries::MeshUserDefined);
             series->setUserDefinedMesh("./sphere.obj");
             QString eps = QString("%1").arg(epsilonRef);
             series->setName("Epsilon="+eps);
             series->setItemLabelFormat(QString("(@seriesName) @xTitle: @xLabel @yTitle: @yLabel @zTitle: @zLabel"));
             ((Q3DScatter*)(graph))->addSeries(series);
             epsilonValues->push_back(epsilonRef);
             QLOG_INFO() << " created series for epsilonRef = " << epsilonRef;
          }
       }
       for (int j = 0; j < epsilonValues->size(); j++) {
          // Create dataset for each epsilon values
          QScatterDataArray *dataArray = new QScatterDataArray;
          for (int k = 0; k < data->size(); k++) {
             if (colormap->at(k) == epsilonValues->at(j)) {
                *dataArray << QVector3D(data->at(k).x(),data->at(k).y(),data->at(k).z());
             }
          }
          // set data in series
           ((Q3DScatter*)(graph))->seriesList().at(j)->dataProxy()->resetArray(dataArray);
           ((Q3DScatter*)(graph))->seriesList().at(j)->setBaseColor(QColor( QRandomGenerator::global()->generate()%255, 
           						     QRandomGenerator::global()->generate()%255, 
           						     QRandomGenerator::global()->generate()%255));
       }
       
       QGridLayout *layout = new QGridLayout(this);
       this->setLayout(layout);
       layout->addWidget(QWidget::createWindowContainer(graph,this),0,0);
       QLOG_INFO() << "Plot::Plot scatter size = " << graph->size();
    }
    else if ( title == "Poynting" ) {
       graph = new Q3DScatter();
       graph->setAspectRatio(1.0);
       graph->setHeight(this->height());
       graph->setWidth(this->width());
       graph->setShadowQuality(QAbstract3DGraph::ShadowQualityNone);
       graph->scene()->activeCamera()->setCameraPreset(Q3DCamera::CameraPresetFront);
       graph->activeTheme()->setType(Q3DTheme::ThemeStoneMoss);
       graph->setReflectivity(0.0f);
       graph->activeTheme()->setAmbientLightStrength(1.0f);
       ((Q3DScatter*)(graph))->axisX()->setTitle("X");
       ((Q3DScatter*)(graph))->axisY()->setTitle("Y");
       ((Q3DScatter*)(graph))->axisZ()->setTitle("Z");
       ((Q3DScatter*)(graph))->axisX()->setLabelFormat("%.2E");
       ((Q3DScatter*)(graph))->axisY()->setLabelFormat("%.2E");
       ((Q3DScatter*)(graph))->axisZ()->setLabelFormat("%.2E");
       ((Q3DScatter*)(graph))->axisX()->setRange(minx, maxx);
       ((Q3DScatter*)(graph))->axisY()->setRange(miny, maxy);
       ((Q3DScatter*)(graph))->axisZ()->setRange(minz, maxz);
       
       // Create series respect to modulus values
       double modulusRef = -999999;
       QVector<double> copyColormap(*colormap);
       std::sort(copyColormap.begin(), copyColormap.end()); // Order colormap in this copy
       QVector<double> *modulusValues = new QVector<double>();
       for (int j = 0; j < copyColormap.size(); j++) {
          double modulus = copyColormap.at(j);
          if ( modulus != modulusRef) {
             modulusRef = modulus;
             QScatter3DSeries *series = new QScatter3DSeries();
             series->setMeshSmooth(true);
             series->setMesh(QAbstract3DSeries::MeshUserDefined);
             series->setUserDefinedMesh("./largesphere.obj");
             QString mod = QString("%1").arg(modulusRef);
             series->setName("Modulus="+mod);
             series->setItemLabelFormat(QString("(@seriesName) @xTitle: @xLabel @yTitle: @yLabel @zTitle: @zLabel"));
             ((Q3DScatter*)(graph))->addSeries(series);
             modulusValues->push_back(modulusRef);
            // QLOG_INFO() << " created series for modulusRef = " << modulusRef;
          }
       }
       for (int j = 0; j < modulusValues->size(); j++) {
          // Create dataset for each modulus values
          QScatterDataArray *dataArray = new QScatterDataArray;
          for (int k = 0; k < data->size(); k++) {
             if (colormap->at(k) == modulusValues->at(j)) {
                *dataArray << QVector3D(data->at(k).x(),data->at(k).y(),data->at(k).z());
             }
          }
          // set data in series
           ((Q3DScatter*)(graph))->seriesList().at(j)->dataProxy()->resetArray(dataArray);
           ((Q3DScatter*)(graph))->seriesList().at(j)->setBaseColor(QColor( 255,
                                                                            255-(int)((255/maxr)*modulusValues->at(j)),
                                                                            0));
            QLOG_DEBUG() << " created series for modulusRef = " << (int)((255/maxr)*modulusValues->at(j));
       }
       
       QGridLayout *layout = new QGridLayout(this);
       this->setLayout(layout);
       layout->addWidget(QWidget::createWindowContainer(graph,this),0,0);
       QLOG_INFO() << "Plot::Plot scatter size = " << graph->size();
    }
    /*else if ( title == "Poynting" ) {
       graph = new Q3DSurface();
       graph->setAspectRatio(1.0);
       graph->setHeight(this->height());
       graph->setWidth(this->width());
       graph->setShadowQuality(QAbstract3DGraph::ShadowQualityNone);
       graph->scene()->activeCamera()->setCameraPreset(Q3DCamera::CameraPresetFront);
       ((Q3DSurface*)(graph))->axisX()->setTitle("X");
       ((Q3DSurface*)(graph))->axisY()->setTitle("Y");
       ((Q3DSurface*)(graph))->axisZ()->setTitle("Z");
       ((Q3DSurface*)(graph))->axisX()->setLabelFormat("%.2E");
       ((Q3DSurface*)(graph))->axisY()->setLabelFormat("%.2E");
       ((Q3DSurface*)(graph))->axisZ()->setLabelFormat("%.2E");
       ((Q3DSurface*)(graph))->axisX()->setRange(minx, maxx);
       ((Q3DSurface*)(graph))->axisY()->setRange(miny, maxy);
       ((Q3DSurface*)(graph))->axisZ()->setRange(minz, maxz);
       
       QSurface3DSeries *series = new QSurface3DSeries();
       series->setItemLabelFormat(QStringLiteral("@xTitle: @xLabel @yTitle: @yLabel @zTitle: @zLabel"));
       series->setDrawMode(QSurface3DSeries::DrawSurfaceAndWireframe);
       series->setFlatShadingEnabled(false);
       
       ((Q3DSurface*)(graph))->addSeries(series);
       
       QSurfaceDataArray *dataArray = new QSurfaceDataArray;
            
       for (int j = 0; j < data->size(); j+=param) {
          QSurfaceDataRow *dataRow = new QSurfaceDataRow(param+1);
           for (int k = j; k < j+param; k++) {
              QLOG_DEBUG()  << k <<" x = " << data->at(k).x() 
		  << " y = " << data->at(k).y()
		  << " z = " << data->at(k).z();
              *dataRow << QSurfaceDataItem(QVector3D(data->at(k).x(),data->at(k).y(),data->at(k).z()));
              }
          *dataRow << QSurfaceDataItem(QVector3D(data->at(j).x(),data->at(j).y(),data->at(j).z()));
          *dataArray << dataRow; 
       }
       ((Q3DSurface*)(graph))->seriesList().at(0)->dataProxy()->resetArray(dataArray);
 
       QGridLayout *layout = new QGridLayout(this);
       this->setLayout(layout);
       layout->addWidget(QWidget::createWindowContainer(graph,this),0,0);
       QLOG_INFO() << "Plot::Plot surface size = " << graph->size();
    }*/
}
Plot::~Plot() {
    QLOG_DEBUG() << "Plot::~Plot:: Deleting Plot";
    delete data;
    delete colormap;
}
bool
Plot::MysortzFunc(const QwtPoint3D& i, const QwtPoint3D& j) { 
      return (i.z() < j.z());
}
bool
Plot::MysortyFunc(const QwtPoint3D& i, const QwtPoint3D& j) { 
      return (i.y() < j.y());
}
bool
Plot::MysortxFunc(const QwtPoint3D& i, const QwtPoint3D& j) { 
      return (i.x() < j.x());
}
